from email_templates.email_template import EmailTemplateBase


class EmailTemplateAutoSuspension(EmailTemplateBase):

    _sender = 'icms-support@cern.ch'
    _recipients = '{recipient_email}'
    _subject = 'Your authorship application progress'
    _bcc = 'icms-support@cern.ch'
    _source_app = 'common'
    _body = '''Dear {recipient_name},

    This message was generated automatically to notify you about your authorship application progress.

    According to our database, your application started {app_duration} days ago and its deadline has already passed.
    Over that period you have provided {work_done} months of EPR work.
    As a result, your service work requirements have been suspended and you are no longer an authorship applicant.

    Should you intend to become a CMS Author, your Team Leader can unsuspend you at any time and your
    authorship application will restart.

    Best regards,
    iCMS mailing system'''

    def __init__(self, recipient_name, recipient_email, app_duration, work_done, sender_email):
        super(EmailTemplateAutoSuspension, self).__init__(recipient_name=recipient_name,
                                                          recipient_email=recipient_email, app_duration=app_duration,
                                                          work_done=isinstance(work_done, float) and ('%.2f' % work_done) or work_done)
        if sender_email:
            self._sender = sender_email