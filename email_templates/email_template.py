#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

from icms_orm.common import EmailMessage


class EmailTemplateBase(object):
    _sender = ''
    _recipients = ''
    _subject = ''
    _body = ''
    _cc = ''
    _bcc = ''
    _source_app = ''
    _reply_to = ''
    _remarks = ''

    def __init__(self, *args, **kwargs):
        self._kwargs = kwargs

    def generate_message(self, store_in_db=True):
        return EmailMessage.compose_message(
            sender=self.sender,
            to=self.recipients,
            subject=self.subject,
            body=self.body,
            cc=self.cc,
            bcc=self.bcc,
            source_app=self.source_app,
            reply_to=self.reply_to,
            remarks=self.remarks,
            db_session=store_in_db and EmailMessage.session() or None
        )

    @property
    def sender(self):
        return self._sender.format(**self._kwargs)

    @property
    def recipients(self):
        return self._recipients.format(**self._kwargs)

    @property
    def subject(self):
        return self._subject.format(**self._kwargs)

    @property
    def body(self):
        return self._body.format(**self._kwargs)

    @property
    def cc(self):
        return self._cc.format(**self._kwargs)

    @property
    def bcc(self):
        return self._bcc.format(**self._kwargs)

    @property
    def source_app(self):
        return self._source_app.format(**self._kwargs)

    @property
    def reply_to(self):
        return self._reply_to.format(**self._kwargs)

    @property
    def remarks(self):
        return self._remarks.format(**self._kwargs)


