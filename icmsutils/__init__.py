#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

from icmsutils.datatypes import ActorData

__all__ = ['ActorData']