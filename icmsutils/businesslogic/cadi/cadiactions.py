from icmsutils.exceptions import CadiException
from icmsutils.businesslogic import flags
from icms_orm.cmspeople import Person
from icms_orm.cmsanalysis import CadiAnalysis as Analysis, CadiAnalysisAnalysts as Analyst, Note as NoteProxy, \
    AnalysisNoteRel as NotesRel, History
import traceback, logging
from icmsutils.businesslogic.cadi import cadibasics
import sqlalchemy as sa
import datetime, pytz
from icmsutils import timedate


class CadiAction(object):

    def __init__(self, db_session, cadi_line, actor,
                 msg_forbidden='Current user is not allowed to perform selected action!', **act_kwargs):
        try:
            self.ssn = db_session
            self.line = isinstance(cadi_line, Analysis) and cadi_line or self.ssn.query(Analysis).filter(
                Analysis.code.ilike('%%%s%%' % cadi_line)).one()
            self.actor = isinstance(actor, Person) and actor or Person.query.get(actor)
            self.msg_forbidden = msg_forbidden
            self.action_args = act_kwargs
        except Exception as e:
            logging.error(traceback.format_exc())
            raise CadiException('Action creation failed!')

    def is_allowed(self):
        """
        This method is not likely a candidate for overriding, you probably want to look at _check_implementation.
        :return:
        """
        CACHE_KEY = '__cached_is_allowed'
        if not hasattr(self, CACHE_KEY):
            setattr(self, CACHE_KEY, self._check_implementation(**self.action_args))
        return getattr(self, CACHE_KEY)

    def _check_implementation(self, **kwargs):
        """
        This method checks some likely defaults (AWG, contact person, iCMS admins, physics coordination) that may be
        enough for subclassing action. Otherwise please override it.
        :return:
        """
        if flags.has_any_flag({flags.Flag.PUBC_MEMBER}, self.actor):
            return True
        if self.actor.cmsId in {int(getattr(a, Analyst.cmsid.key)) for a in self.line.analysts}:
            return True
        if self.actor.cmsId in cadibasics.get_awg_members(self.ssn, cadibasics.get_awg_from_analysis_code(self.line.code)):
            return True
        return False

    def perform(self):
        """
        This method is meant to be called by client code and probably not to be overridden by subclasses.
        :return:
        """
        if not self.is_allowed():
            raise CadiException(self.msg_forbidden)
        self._action_implementation(**self.action_args)

    def _action_implementation(self, **kwargs):
        """
        This method delivers the action's definition, should be overridden in subclasses and should not be
        directly called by the client code.
        :return:
        """
        pass

    def _make_history_entry(self, field, old_value, new_value, master_id, timestamp_override=None):
        """
        A helper method creating the history entry and setting corresponding update fields in the Analysis table
        :return:
        """
        timestamp = timestamp_override or datetime.datetime.utcnow()
        setattr(self.line, Analysis.updaterDate.key, timestamp)
        setattr(self.line, Analysis.updaterName.key, '%s %s (%s)' % (self.actor.firstName, self.actor.lastName, self.actor.instCode))
        setattr(self.line, Analysis.updater.key, self.actor.cmsId)

        return History.from_ia_dict({
            History.table: field.parent.tables[0].key.split('.')[-1],
            History.field: field.key,
            History.masterId: master_id,
            History.updaterId: self.actor.cmsId,
            History.updaterName: getattr(self.line, Analysis.updaterName.key),
            History.updateDate: timedate.datetime_to_str_verbose(
                pytz.utc.localize(timestamp)),
            History.description: '',
            History.remarks: 'created with CadiAction',
            History.oldValue: old_value,
            History.newValue: new_value,
        })


class AttachNoteAction(CadiAction):
    def _action_implementation(self, note_id):
        if note_id in cadibasics.get_related_cms_note_ids(self.ssn, self.line.code):
            raise CadiException('Note %s already linked to CADI line %s' % (note_id, self.line.code))
        note = self.ssn.query(NoteProxy).filter(NoteProxy.cmsNoteId == note_id).first()
        line = self.ssn.query(Analysis).filter(sa.or_(Analysis.code == self.line.code, Analysis.code == 'd%s' % self.line.code)).one()
        if not note:
            logging.debug('Creating a new "proxy" entry for %s in CMSAnalysis DB' % note_id)
            note = NoteProxy.from_ia_dict({NoteProxy.cmsNoteId: note_id})
        else:
            logging.debug('Proxy entry for %s found in CMSAnalysis DB' % note_id)
        rel = NotesRel.from_ia_dict({NotesRel.id: line.id})
        rel.note = note
        self.ssn.add(note)
        self.ssn.add(rel)
        self.ssn.commit()


class DetachNoteAction(CadiAction):
    def _action_implementation(self, note_id):
        entry = self.ssn.query(NotesRel).join(NoteProxy, NoteProxy.id == NotesRel.noteId).\
            join(Analysis, NotesRel.id == Analysis.id).filter(Analysis.code.in_([self.line.code, 'd%s' % self.line.code])).\
            filter(NoteProxy.cmsNoteId == note_id).one_or_none()
        if entry:
            self.ssn.delete(entry)
            self.ssn.commit()
        else:
            raise CadiException('Note %s is not associated with CADI line %s.' % (note_id, self.line.code))


class RenameLineAction(CadiAction):
    def _action_implementation(self, new_title):
        old_title = self.line.name
        self.ssn.add(self._make_history_entry(field=Analysis.name, new_value=new_title, old_value=old_title, master_id=self.line.id))
        self.line.name = new_title
        Analysis.updaterDate
        self.ssn.add(self.line)
        self.ssn.commit()


class ChangeLineStateAction(CadiAction):
    def _action_implementation(self, new_state):
        if new_state not in cadibasics.CadiState.get_ordered_state_names():
            raise CadiException('%s does not seem like a legit CADI state!' % new_state)
        old_state = self.line.status
        setattr(self.line, Analysis.status.key, new_state)
        self.ssn.add(self._make_history_entry(field=Analysis.status, new_value=new_state, old_value=old_state, master_id=self.line.id))
        self.ssn.add(self.line)
        self.ssn.commit()
