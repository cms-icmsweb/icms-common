from datetime import date

class DateSensitive():
    """
    To be mixed into utility classes that require the notion of what date it is (or what date they should think it is)
    """
    def __init__(self):
        self._today = None

    def set_fixed_date(self, fixed_date):
        self._today = fixed_date

    def today(self):
        return self._today or date.today()
