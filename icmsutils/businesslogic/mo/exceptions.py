from icmsutils.exceptions import IcmsException

class MoException(IcmsException):
    pass

class IllegalMoStateTransitionException(IcmsException):
    pass

class MoEditionDisabledException(IcmsException):
    pass