from icms_orm.cmspeople import MoData, Person, Institute
from sqlalchemy import func
from datetime import datetime as dt
import sqlalchemy as sa


class PhdCountModel(object):
    def __init__(self, db_session, year=None, include_free=False):
        self._year = year or dt.now().year
        (inst_col, flag_col) = (getattr(MoData, x.key.replace('2017', str(self._year))) for x in [MoData.phdInstCode2017, MoData.phdMo2017])

        rows = db_session.query(func.count(flag_col), inst_col, Institute.country).\
            join(Institute, inst_col == Institute.code).filter(flag_col.ilike('yes')).group_by(inst_col).all()

        self._by_inst = {}
        self._by_country = {}

        corrections = {}
        if include_free:
            corrections.update(get_free_mo_phds_per_inst(db_session, self._year))

        for number, inst_code, country in rows:
            number += corrections.get(inst_code, 0)
            self._by_inst[inst_code] = self._by_inst.get(inst_code, 0) + number
            self._by_country[country] = self._by_country.get(inst_code, 0) + number

    def for_inst(self, code):
        return self._by_inst.get(code, 0)

    def for_country(self, name):
        return self._by_country.get(name, 0)

    def __getitem__(self, item):
        return self._by_inst.get(item, self._by_country.get(item, 0))


def get_phds_per_inst(db_session, year=None):
    """
    Checks how many PPL enetered the PHD list under the institute's banner.
    """
    if not year:
        year = dt.now().year
    (inst_col, flag_col) = (getattr(MoData, x.key.replace('2017', str(year))) for x in [MoData.phdInstCode2017, MoData.phdMo2017])
    return {x[0]: x[1] for x in db_session.query(inst_col, func.count(flag_col)).filter(flag_col.ilike('yes')).group_by(inst_col).all()}


def get_phds_per_inst_by_present_affiliation(db_session):
    """
    Method for cross-checking the PRESENT state. Checks how many PPL affiliated with the inst are PHDs.
    """
    year = dt.now().year
    flag_col = getattr(MoData, MoData.phdMo2017.key.replace('2017', str(year)))
    return {x[0]: x[1] for x in db_session.query(Person.instCode, func.count(MoData.cmsId)).
            join(MoData, Person.cmsId == MoData.cmsId).filter(flag_col.ilike('yes')).group_by(Person.instCode).all()}


def get_free_mo_phds_per_inst(db_session, year=None):
    """
    Counts the free MO phds per inst and year
    """
    year = year or dt.now().year
    mo_col, free_mo_col, phd_mo_col, phd_ic_col = (getattr(MoData, ref_col.key.replace('2017', str(year)))
        for ref_col in [MoData.mo2017, MoData.freeMo2017, MoData.phdMo2017, MoData.phdInstCode2017])

    q = db_session.query(Person.instCode, func.count(Person.cmsId)).join(MoData, Person.cmsId == MoData.cmsId).\
        filter(mo_col.ilike('YES')).filter(free_mo_col.ilike('YES')).filter(phd_mo_col.ilike('NO')).\
        filter(sa.or_(phd_ic_col == '', phd_ic_col == None)).group_by(Person.instCode)

     #
    return {r[0]: r[1] for r in q.all()}

