from icms_orm.common import MoStatusValues, FundingAgency as FA, MO
from icms_orm.common import Person, Institute, PersonStatus
from icms_orm import PseudoEnum
import sqlalchemy as sa


class MoRecord(object):
    def __init__(self, mo, person, fa, person_status):
        self._mo = mo
        self._person = person
        self._fa = fa
        self._person_status = person_status

    @property
    def mo(self):
        return self._mo

    @property
    def person(self):
        return self._person

    @property
    def fa(self):
        return self._fa

    @property
    def person_status(self):
        return self._person_status

    def __str__(self):
        return 'MO record for CMSid {cms_id} in year {year}'.format(cms_id=self._person.get(Person.cms_id), year=self._mo.get(MO.year))


class MoModel(object):

    class Mode(PseudoEnum):
        # restricts the set of states to proposed and approved, shows the latest
        PRE_APPROVAL = 'PRE_APPROVAL'
        # only considers the approved state
        OFFICIAL = 'OFFICIAL'
        # restricts the set of states to anything that allows signing, shows the latest
        POST_APPROVAL = 'POST_APPROVAL'
        # only shows those, who got rejected before the approval
        REJECTED = 'REJECTED'
        # shows those who have been taken off the list following its approval - swap outs, cancellations (if possible)
        REMOVED = 'REMOVED'

    @classmethod
    def get_instance(cls, year, mode):
        _allowed_statuses = set()
        _eliminating_statuses = set()
        if mode == MoModel.Mode.OFFICIAL:
            _allowed_statuses.add(MoStatusValues.APPROVED)
        elif mode == MoModel.Mode.PRE_APPROVAL:
            _allowed_statuses.update({MoStatusValues.APPROVED, MoStatusValues.PROPOSED})
            _eliminating_statuses.add(MoStatusValues.REJECTED)
        elif mode == MoModel.Mode.POST_APPROVAL:
            _eliminating_statuses = {MoStatusValues.SWAPPED_OUT, MoStatusValues.CANCELLED}
            for _v in MoStatusValues.values():
                if MoStatusValues.APPROVED in _v:
                    _allowed_statuses.add(_v)
        elif mode == MoModel.Mode.REJECTED:
            _allowed_statuses.add(MoStatusValues.REJECTED)
        elif mode == MoModel.Mode.REMOVED:
            _allowed_statuses.update({MoStatusValues.SWAPPED_OUT, MoStatusValues.CANCELLED})
        # the underlying query needs to accept the eliminating statuses (so that an allowed one does not pass)
        _allowed_statuses.update(_eliminating_statuses)
        return cls(year=year, status_pool=_allowed_statuses, eliminating_statuses=_eliminating_statuses)

    def __init__(self, year, status_pool=None, eliminating_statuses=None):
        """
        :param year:
        :param mo_status: pass an enum value for only these to be displayed. None means the most recent will show up.
        :param status_pool: pass a set of values that the results will be limited to. see how the modes work.
        :param eliminating_statuses: these will be removed from the result set, but allowed in the query
        (allows eg. not showing at all someone swapped out rather than picking them up with an earlier status)
        """
        _ssn = MO.session()
        self._year = year
        self._status_pool = status_pool or {}
        self._eliminating_statuses = eliminating_statuses or {}
        self._fas = {}
        self._fa_names_by_id = {}
        self._fa_names_by_cms_id = {}
        self._insts = {}
        self._people = {}
        self._people_statuses = {}
        self._totals = {}
        # here we will store by ID:
        self._mo = {}
        self._mo_id_by_cms_id = {}
        self._mo_id_by_fa = {}
        self._mo_id_by_ic = {}
        self._cms_ids_by_fa_and_ic = {}

        _q = _ssn.query(MO, FA, Institute, Person, PersonStatus).filter(MO.year == year).\
            join(FA, MO.fa_id == FA.id, isouter=True).join(Institute, MO.inst_code == Institute.code, isouter=True).\
            join(Person, MO.cms_id == Person.cms_id).\
            join(PersonStatus, sa.and_(Person.cms_id == PersonStatus.cms_id, PersonStatus.end_date == None))

        _q = MO.query_for_latest(year=self._year, status_pool=self._status_pool, initial_query=_q)

        _rows = _q.all()

        for _mo, _fa, _inst, _person, _person_status in _rows:
            if _mo.get(MO.status) in self._eliminating_statuses:
                continue
            _fa_name = _fa and _fa.get(FA.name) or None
            _fa_id = _fa and _fa.get(FA.id) or None
            _ic = _inst and _inst.get(Institute.code) or None
            _cms_id = _person.get(Person.cms_id)
            _mo_id = _mo.get(MO.id)

            self._fas[_fa_name] = _fa
            self._fa_names_by_id[_fa_id] = _fa_name
            self._fa_names_by_cms_id[_cms_id] = _fa_name
            self._insts[_fa_name] = self._insts.get(_fa_name) or {}
            self._insts[_fa_name][_ic] = _inst
            self._people[_cms_id] = _person
            self._people_statuses[_cms_id] = _person_status

            self._cms_ids_by_fa_and_ic[(_fa_name, _ic)] = self._cms_ids_by_fa_and_ic.get((_fa_name, _ic)) or set()
            self._cms_ids_by_fa_and_ic[(_fa_name, _ic)].add(_cms_id)
            self._totals[_fa_name] = self._totals.get(_fa_name, 0) + 1
            self._mo[_mo_id] = _mo
            self._mo_id_by_cms_id[_cms_id] = self._mo_id_by_cms_id.get(_cms_id, [])
            self._mo_id_by_cms_id[_cms_id].append(_mo_id)
            self._mo_id_by_fa[_fa_name] = self._mo_id_by_fa.get(_fa_name, [])
            self._mo_id_by_fa[_fa_name].append(_mo_id)
            self._mo_id_by_ic[_ic] = self._mo_id_by_ic.get(_ic, [])
            self._mo_id_by_ic[_ic].append(_mo_id)

    def _mo_by_cms_id(self, cms_id):
        return self._mo.get((self._mo_id_by_cms_id.get(cms_id) or [None])[-1])

    def _get_person_by_cms_id(self, cms_id):
        return self._people.get(cms_id, None)

    def _get_fa_by_id(self, _fa_id):
        return self._fas.get(self._fa_names_by_id.get(_fa_id))

    def _get_fa_for_cms_id(self, cms_id):
        return self._fas.get(self._fa_names_by_cms_id.get(cms_id))

    def get_total(self, fa_name):
        return fa_name and self._totals.get(fa_name) or sum(self._totals.values())

    def get_fa_names(self):
        return self._fas.keys()

    def get_inst_codes(self, fa_name=None):
        return self._insts.get(fa_name).keys()

    def get_records(self, fa_name=None, inst_code=None):
        """
        :param fa_name: 
        :param inst_code: 
        :return: MoRecord instances
        """
        _people = []
        _modata = []
        if fa_name and inst_code:
            _people = [self._people.get(_id) for _id in self._cms_ids_by_fa_and_ic.get((fa_name, inst_code), {})]
        elif fa_name:
            _modata = [self._mo.get(_mo_id) for _mo_id in self._mo_id_by_fa.get(fa_name, [])]
        elif inst_code:
            _modata = [self._mo.get(_mo_id) for _mo_id in self._mo_id_by_ic.get(inst_code, [])]
        else:
            _people = self._people.values()
        if _people:
            return [MoRecord(person=_p, mo=self._mo_by_cms_id(_p.get(Person.cms_id)),
                             fa=self._get_fa_for_cms_id(_p.get(Person.cms_id)),
                             person_status=self._people_statuses.get(_p.get(Person.cms_id))) for _p in _people]
        elif _modata:
            return [MoRecord(person=self._get_person_by_cms_id(_mo.get(MO.cms_id)), mo=_mo,
                             fa=self._get_fa_for_cms_id(_mo.get(MO.cms_id)),
                             person_status=self._people_statuses.get(_mo.get(MO.cms_id))) for _mo in _modata]
        return []
    
    def count_by_inst_code(self, inst_code):
        return len(self._mo_id_by_ic.get(inst_code, []))
    
    def count_by_fa_name(self, fa_name):
        return len(self._mo_id_by_fa.get(fa_name, []))
