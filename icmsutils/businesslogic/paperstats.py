"""
Methods counting how many author lists have been generated in a given year.
If a CMS ID is provided, they will only count the lists including specified author.

Author list statuses:
 in - means the person is in the paper
 in New - means the person has been manually added
"""

import icms_orm
from icms_orm.cmsanalysis import PaperAuthor, PaperAuthorHistory, Analysis
import sqlalchemy as sa

LIST_GEN_ACTION = 'ListGen'
AUTHOR_STATUS_IN = 'in'
AUTHOR_STATUS_IN_NEW = 'in NEW'


def get_generated_al_sq(author_cms_id=None, year=None, force_join=False):
    ssn = PaperAuthorHistory.session()
    queried_cols = [
        sa.distinct(PaperAuthorHistory.code).label('code'),
        sa.case([(PaperAuthorHistory.date.like('%%/%d' % _year), _year) for _year in range(2000, 2031)]).label('year'),
    ]
    if force_join:
        queried_cols.append(sa.cast(PaperAuthor.cmsid, sa.Integer).label('cms_id'))
    s = ssn.query(*queried_cols).filter(PaperAuthorHistory.action.like(LIST_GEN_ACTION))
    if year:
        s = s.filter(PaperAuthorHistory.date.like('%%/%d' % year))

    if author_cms_id or force_join:
        # joining with PaperAuthor in case individual statistics have been requested
        s = s.join(PaperAuthor,
                   sa.and_(PaperAuthor.code == PaperAuthorHistory.code))
        s = s.filter(sa.or_(PaperAuthor.status.like(AUTHOR_STATUS_IN), PaperAuthor.status.like(AUTHOR_STATUS_IN_NEW)))
    if author_cms_id:
        s = s.filter(PaperAuthor.cmsid == str(author_cms_id))

    # join against the actual analyses - just to filter out potential stale data
    s = s.join(Analysis,
               sa.or_(PaperAuthorHistory.code == Analysis.code, Analysis.code == sa.func.concat('d', PaperAuthorHistory.code)))
    return s.subquery()


def count_signed_papers_by_year(cms_id=None, year=None):
    ssn = Analysis.session()
    sq = get_generated_al_sq(author_cms_id=cms_id, year=year)
    rows = ssn.query(sq.c.year, sa.func.count(sq.c.code)).group_by(sq.c.year).all()
    return {r[0]: r[1] for r in rows}


def count_generated_al_per_year(year=None):
    return count_signed_papers_by_year(cms_id=None, year=year)


def signed_papers_count_map_by_cms_id_and_year(year=None):
    ssn = Analysis.session()
    sq = get_generated_al_sq(author_cms_id=None, year=year, force_join=True)
    rows = ssn.query(sq.c.year, sq.c.cms_id, sa.func.count(sq.c.code)).group_by(sq.c.year, sq.c.cms_id).all()
    result = dict()
    for year, cms_id, count in rows:
        result[cms_id] = result.get(cms_id, dict())
        result[cms_id][year] = count
    return result
