from typing import Iterable, Optional, Type
from icmsutils.datatypes import ActorData
from icms_orm import PseudoEnum
from icms_orm.common import RequestStepStatusValues, RequestStatusValues, RequestStep, Request
from abc import ABC, ABCMeta, abstractclassmethod, abstractmethod
from .exceptions import RequestProcessingException, InsufficientPriviligesException
import logging


class BaseRequestProcessor(ABC):
    """
    This class is to know how to toggle the states by processing  / creating the steps
    """
    class Action(PseudoEnum):
        CREATE = 'CREATE_REQUEST'
        APPROVE = 'APPROVE_REQUEST_STEP'
        REJECT = 'REJECT_REQUEST_STEP'

    def __init__(self, request):
        self._request = request

    @abstractmethod
    def can_create(self, actor_data: ActorData) -> bool:
        """
        :param actor_data:
        :return: True or False
        """

    def can_act(self, actor_data: ActorData, action: Optional[Action] = None, step=None) -> bool:
        return actor_data.cms_id in self.get_cms_ids_allowed_to_act(action=action, step=None)

    @abstractmethod
    def get_cms_ids_allowed_to_act(self, action: Optional[Action] = None, step=None) -> Iterable[int]:
        """
        Supposed to return the CMS IDs of people allowed to act upon the request
        :param actor_data:
        :param action: a specific action or None (meaning ANY available)
        :param step: an instance of step or None (meaning ANY available)
        :return: True or False
        """

    @abstractmethod
    def create_next_steps(self, actor_data: ActorData, action: Optional[Action] = None):
        """
        Supposed to create the steps and return them - no DB handling necessary in implementing code
        :param actor_data:
        :param action:
        :return: an iterable collection of created steps
        """

    def _get_pending_steps(self):
        return {_s.get(RequestStep.id): _s for _s in self.request.get(Request.steps) if _s.get(RequestStep.status) == RequestStepStatusValues.PENDING}

    def _pick_pending_step(self, step_id=None):
        _pending_steps = self._get_pending_steps()
        assert isinstance(_pending_steps, dict)
        if step_id is not None:
            if step_id not in _pending_steps:
                raise RequestProcessingException(
                    'Provided step ID %d is not among the pending steps!' % step_id)
            return _pending_steps.get(step_id)
        else:
            if len(_pending_steps) > 1:
                raise RequestProcessingException('No step ID provied and there are %d steps available to act upon.'
                                                 % (len(_pending_steps)))
            else:
                return list(_pending_steps.values())[0]

    def handle_step(self, action: Action, actor_data: ActorData, step_id=None, remarks=None):
        assert action in BaseRequestProcessor.Action.values()
        if action == BaseRequestProcessor.Action.CREATE:
            if not self.can_create(actor_data):
                raise InsufficientPriviligesException(
                    actor_data, self.request, action)
        else:
            _step = self._pick_pending_step(step_id)
            _step.set(RequestStep.remarks, remarks)
            if not self.can_act(actor_data=actor_data, action=action, step=_step):
                raise InsufficientPriviligesException(
                    actor_data=actor_data, request=self.request, action=action)
            if action == BaseRequestProcessor.Action.APPROVE:
                _step.set(RequestStep.status, RequestStepStatusValues.APPROVED)
            elif action == BaseRequestProcessor.Action.REJECT:
                _step.set(RequestStep.status, RequestStepStatusValues.REJECTED)
                # rejecting a single step marks the whole request as rejected - no overturning mechanism planned
                self.request.set(Request.status, RequestStatusValues.REJECTED)

        # invoke subclass-provided approval steps generator
        _next_steps = self.create_next_steps(actor_data, action)
        if _next_steps is not None:
            for _step in _next_steps:
                _step.set(RequestStep.request, self.request)

        # mark as ready/scheduled (depending on scheduled execution date) for execution if not yet REJECTED
        if self.request.get(Request.status) == RequestStatusValues.PENDING_APPROVAL and not self._get_pending_steps():
            if self._request.get(Request.scheduled_execution_date) is None:
                self.request.set(
                    Request.status, RequestStatusValues.READY_FOR_EXECUTION)
            else:
                self.request.set(
                    Request.status, RequestStatusValues.SCHEDULED_FOR_EXECUTION)

    @property
    def request(self):
        return self._request


class BaseRequestExecutor(object):
    """
    This class is only to know how to implement the request (perform the actions requiring approval)
    In this trivial example, it merely logs the details of the request (not enough)
    """

    def __init__(self, **kwargs):
        self._args = kwargs

    def execute(self):
        logging.info('Executing by printing the following request data: %s' % ', '.
                     join(['%s=%s' % (_k, _v) for _k, _v in self._args.items()]))
