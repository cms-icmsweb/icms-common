from abc import ABC, abstractclassmethod
from typing import Type
from icmsutils.businesslogic.requests.base_classes import BaseRequestExecutor, BaseRequestProcessor
from icmsutils.businesslogic.requests.requests_service import RequestsService


class BaseRequestDefinition(ABC):
    """
    A simple wrapper providing all the necessary information to register a new request type with the service
    """

    @classmethod
    @abstractclassmethod
    def get_name(cls) -> str:
        return ''

    @classmethod
    @abstractclassmethod
    def get_processor_class(cls) -> Type[BaseRequestProcessor]:
        return BaseRequestProcessor

    @classmethod
    @abstractclassmethod
    def get_executor_class(cls) -> Type[BaseRequestExecutor]:
        return BaseRequestExecutor

    @classmethod
    def register(cls, service_class: Type[RequestsService] = RequestsService):
        service_class.register_request_type_name_with_processor_and_executor(
            request_type_name=cls.get_name(),
            processor_class=cls.get_processor_class(),
            executor_class=cls.get_executor_class()
        )
