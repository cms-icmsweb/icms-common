from icms_orm.toolkit import RoomRequest, Status
from icms_orm.cmspeople import Person
from .flags import Flag, has_any_flag
from datetime import datetime, timedelta


def can_edit_rr(rr, person):
    if datetime.now() < getattr(rr, RoomRequest.time_start.key):
        if getattr(rr, RoomRequest.status.key) == Status.PENDING and getattr(person, Person.cmsId.key) in [getattr(rr, RoomRequest.cms_id_by.key), getattr(rr, RoomRequest.cms_id_for.key)]\
                or has_any_flag([Flag.ICMS_ADMIN, Flag.ICMS_SECR], person):
            return True
    return False


def can_approve_rr(rr, person):
    # rr can be None in case it's only being created - in such a case the flags decide
    if rr is None or getattr(rr, RoomRequest.status.key) == Status.PENDING:
        return has_any_flag([Flag.ICMS_SECR, Flag.ICMS_ADMIN], person)
    return False


def are_colliding(booking_a: RoomRequest, booking_b: RoomRequest):
    a_start = booking_a.time_start
    b_start = booking_b.time_start
    if a_start.date() != b_start.date():
        return False
    if (booking_a.room_id or booking_a.room_id_preferred) != (booking_b.room_id or booking_b.room_id_preferred):
        return False
    a_end = a_start + timedelta(minutes=getattr(booking_a, RoomRequest.duration.key))
    b_end = b_start + timedelta(minutes=getattr(booking_b, RoomRequest.duration.key))
    summed_durations = sum([getattr(x, RoomRequest.duration.key) for x in [booking_a, booking_b]])
    time_points = sorted([a_start, a_end, b_start, b_end])
    if ((time_points[-1] - time_points[0]).seconds / 60) < summed_durations:
        return True
    return False


def get_colliding_requests(db_session, cms_week_id, day=None, room_id=None, output='set', requests=None):
    """
    :param db_session:
    :param cms_week_id:
    :param day:
    :param room_id:
    :param output:
        - set: returns a single set where all the requests that collide with at least one other request are added
    :param requests: a list of RoomRequest objects to perform the checks on without requerying the DB
    :return:
    """
    irrelevant_states = [Status.DELETED, Status.SUSPENDED]
    if not requests:
        q = db_session.query(RoomRequest).filter(RoomRequest.cms_week_id == cms_week_id).filter(RoomRequest.status.notin_(irrelevant_states))
        q = day and q.filter(datetime(day.year, day.month, day.day, 0, 0) <= RoomRequest.time_start).\
            filter(datetime(day.year, day.month, day.day, 23, 59) > RoomRequest.time_start) or q
        q = room_id and q.filter(RoomRequest.room_id == room_id) or q
        requests = q.all()
    else:
        requests = [r for r in requests if r.status not in irrelevant_states]
    all_colliding = set()
    for i in range(0, len(requests)):
        for j in range(i + 1, len(requests)):
            a, b = (requests[i], requests[j])
            if are_colliding(a, b):
                # colliding_pairs.append((a, b))
                all_colliding.add(a)
                all_colliding.add(b)
    return all_colliding
