from __future__ import division

import sqlalchemy
from datetime import date, timedelta as td
from icms_orm import c2k
from icms_orm.epr import TimeLineUser, EprUser, Shift, EprInstitute as Institute, Pledge, Task, ShiftTaskMap as STM, TimeLineInst, CMSActivity, Project
from icms_orm.cmspeople import Institute as MysqlInstitute
from sqlalchemy import and_, func, case, cast, or_
from sqlalchemy.types import Date, Integer
import collections, logging
from icms_orm.cmspeople.projects import Pledge as OldPledge
from icms_orm.cmspeople import MemberStatusValues
import collections
from icms_orm.common import ApplicationAsset as AppAsset
from email_templates.service_work_emails import EmailTemplateEprDueIncrease, EmailTemplateEprDueReduction
from icms_orm.common import Person, InstituteLeader, Affiliation, EprAggregate, EprAggregateUnitValues as AggUnit
from icms_orm.epr import TimeLineUser as TLU, EprUser, Project as EprProject
from icmsutils.businesslogic.superseders import SupersedersResolver


class AssetNames(object):
    EPR_AUTHOR_REDUCTIONS = 'epr_author_due_reductions'
    EPR_AUTHOR_REDUCTION_REASONS = 'epr_due_reduction_reasons'


class EprAggregatesFactory(object):

    @classmethod
    def find_project_code(cls, input_name):
        """
        For now a passthrough but consolidating project names will likely entail performing some fixes on that stage
        """
        return input_name

    @classmethod
    def get_shifts_done(cls, year, cms_id=None, inst_code=None):
        ssn = STM.session()
        grouping_columns = \
            year >= 2017 and (TLU.instCode, EprUser.cmsId, EprProject.name) \
            or year >= 2015 and (TLU.instCode, EprUser.cmsId, Shift.subSystem) \
            or (OldPledge.instCode, OldPledge.cmsId, OldPledge.projectId)
        args = dict(db_session=ssn, year=year, grouping_column=grouping_columns, inst_code=inst_code, cms_id=cms_id)
        return cls.__delegate_to_query_method_and_flip_results_inside_out(_get_shifts_done, args, AggUnit.SHIFTS_DONE)

    @classmethod
    def get_work_done(cls, year, cms_id=None, inst_code=None):
        ssn = STM.session()
        grouping_columns = \
            year >= 2015 and (Institute.code, EprUser.cmsId, EprProject.name) \
            or (OldPledge.instCode, OldPledge.cmsId, OldPledge.projectId)
        args = dict(db_session=ssn, year=year, grouping_column=grouping_columns, inst_code=inst_code,
                    cms_id=cms_id)
        return cls.__delegate_to_query_method_and_flip_results_inside_out(_get_worked, args, AggUnit.PLEDGES_DONE)

    @classmethod
    def __delegate_to_query_method_and_flip_results_inside_out(cls, method, args, agg_unit):
        data = method(**args)
        # We need to store it as a dictionary as inst-code remapping sometimes results in the same coordinates showing
        # up twice. We'll sum up the values in such cases.
        _ans = dict()
        for _ic in data:
            for _cms_id in data[_ic]:
                for _prj in data[_ic][_cms_id]:
                    _e = EprAggregate.from_ia_dict({
                        EprAggregate.cms_id: _cms_id,
                        EprAggregate.inst_code: SupersedersResolver.get_present_value(MysqlInstitute.code, _ic),
                        EprAggregate.year: args['year'],
                        EprAggregate.project_code: cls.find_project_code(_prj),
                        EprAggregate.aggregate_value: data[_ic][_cms_id][_prj],
                        EprAggregate.aggregate_unit: agg_unit
                    })
                    assert isinstance(_e, EprAggregate)
                    if _e.coordinates in _ans:
                        _ans[_e.coordinates].aggregate_value += _e.aggregate_value
                    else:
                        _ans[_e.coordinates] = _e
        return _ans.values()


def _get_dues(db_session, year, grouping_column, inst_code=None, cms_id=None):
    years = isinstance(year, collections.Iterable) and year or [year]

    grouping_columns = (isinstance(grouping_column, collections.Iterable) and [c for c in grouping_column] or [
        grouping_column]) + (isinstance(year, collections.Iterable) and [TimeLineUser.year] or [])
    q = db_session.query(func.sum(TimeLineUser.dueApplicant + TimeLineUser.dueAuthor), *grouping_columns).\
        filter(TimeLineUser.year.in_(years))
    q = q.filter(TimeLineUser.status == MemberStatusValues.PERSON_STATUS_CMS)
    if inst_code:
        q = q.filter(TimeLineUser.instCode == inst_code)
    if cms_id:
        q = q.filter(TimeLineUser.cmsId == cms_id)
    q = q.group_by(*grouping_columns)
    return __merge_query_results([q])


def _get_worked(db_session, year, grouping_column, inst_code=None, cms_id=None):
    """
    If year is any collection of years, a map will be returned per each requested grouping entity (inst or person)
    with years as keys.
    :param db_session:
    :param year:
    :param grouping_column:
    :param inst_code:
    :param cms_id:
    :return:
    """
    years = sorted(isinstance(year, collections.Iterable) and year or [year])
    old_years = [_year for _year in years if _year < 2015]
    new_years = [_year for _year in years if _year >= 2015]
    queries = []
    group_by_cols = (isinstance(grouping_column, collections.Iterable) and [c for c in grouping_column] or [
        grouping_column]) + (len(years) > 1 and [Pledge.year] or [])
    if new_years:
        q = db_session.query(func.sum(Pledge.workTimeDone * case(
            [(Task.kind.ilike('core'), 1), (Task.kind.ilike('%noncore%'), 0.25), (Task.kind.ilike('%non-core%'), 0.25)]
        ).label('work_done')), *group_by_cols)

        q = q.filter(or_(*[Pledge.year == year for year in new_years]))
        q = q.join(Task, Pledge.taskId == Task.id)
        q = q.join(CMSActivity, Task.activityId == CMSActivity.id)
        q = q.join(Project, CMSActivity.projectId == Project.id)
        q = q.filter(and_(Pledge.status.notin_({'rejected'}), Task.tType.notilike('%shift%')))
        q = q.join(Institute, Institute.id == Pledge.instId)
        q = q.join(EprUser, Pledge.userId == EprUser.id)
        if inst_code:
            q = q.filter(Institute.code == inst_code)
        if cms_id:
            q = q.filter(EprUser.cmsId == cms_id)
        q = q.group_by(*group_by_cols)
        queries.append(q)
    if old_years:
        group_by_cols = [_map_to_old_grouping_column(c) for c in group_by_cols]
        q = db_session.query(
            func.sum((OldPledge.totalAccepted - OldPledge.totalShift) * OldPledge.percentage / 100.0),
            *group_by_cols
        )
        q = q.filter(or_(*[OldPledge.year == year for year in old_years]))
        # for aesthetics: eliminates zeros from the output
        q = q.filter(OldPledge.totalAccepted - OldPledge.totalShift > 0)
        if inst_code:
            q = q.filter(OldPledge.instCode == inst_code)
        elif cms_id:
            q = q.filter(OldPledge.cmsId == cms_id)
        q = q.group_by(*group_by_cols)
        queries.append(q)

    return __merge_query_results(queries)


def _get_shifts_done(db_session, year, grouping_column, inst_code=None, cms_id=None):
    # A little problem: shifts are not associated with any institute...
    # So we need to find the corresponding institute based on TimeLines
    # And there's another issue: TimeLines have no end dates...

    q = None

    years = sorted(isinstance(year, collections.Iterable) and year or [year])
    old_years = [_year for _year in years if _year < 2015]
    new_years = [_year for _year in years if 2017 > _year >= 2015]
    newest_years = [_year for _year in years if _year >= 2017]
    queries = []
    # handling grouping column being actually a collection of columns
    group_by_cols = (isinstance(grouping_column, collections.Iterable) and [c for c in grouping_column] or [grouping_column]) + (len(years) > 1 and [Shift.year] or [])
    for year_range in (new_years, newest_years):
        if year_range:
            q = None
            if year_range[0] >= 2017:
                if year_range[0] >= 2020:
                    q = db_session.query(func.sum(Shift.weight * (STM.weight + (STM.addedValueCredits / 8.)) / 21.7),
                                        *group_by_cols)
                else:
                    # 21.7 = average number of working days in a month
                    q = db_session.query(func.sum(Shift.weight * STM.weight * (1. + (STM.addedValueCredits / 7.)) / 21.7),
                                        *group_by_cols)
                q = q.join(STM, and_(Shift.subSystem == STM.subSystem, and_(
                    Shift.shiftType == STM.shiftType, Shift.flavourName == STM.flavourName, Shift.year == STM.year))). \
                    join(Task, STM.taskCode == Task.code).join(CMSActivity, Task.activityId == CMSActivity.id). \
                    join(Project, Project.id == CMSActivity.projectId). \
                    filter(STM.status.ilike('ACTIVE'))                    
            else:
                q = db_session.query(func.sum(Shift.weight / 21.7), *group_by_cols)

            q = q.join(EprUser, EprUser.hrId == Shift.userId).join(TimeLineUser, and_(
                EprUser.cmsId == TimeLineUser.cmsId,
                Shift.year == TimeLineUser.year,    
                Shift.shiftStart >= TimeLineUser.timestamp,
                cast(Shift.shiftStart, Date) < cast(TimeLineUser.timestamp, Date) + cast(TimeLineUser.yearFraction * 365,
                                                                                         Integer)
            ))
            q = q.filter(Shift.year.in_(year_range))
            if inst_code:
                q = q.filter(TimeLineUser.instCode == inst_code)
            if cms_id:
                q = q.filter(TimeLineUser.cmsId == cms_id)
            if year == date.today().year:
                q = q.filter(Shift.shiftEnd < date.today())
            q = q.group_by(*group_by_cols)
            queries.append(q)
    if old_years:
        group_by_cols = [_map_to_old_grouping_column(c) for c in group_by_cols]
        q = db_session.query(
            func.sum(OldPledge.totalShift * OldPledge.percentage / 100.0),
            *group_by_cols
        ).filter(OldPledge.year.in_(old_years))
        # for aesthetics: eliminates zeros from the output
        q = q.filter(OldPledge.totalShift > 0)
        if inst_code:
            q = q.filter(OldPledge.instCode == inst_code)
        elif cms_id:
            q = q.filter(OldPledge.cmsId == cms_id)
        q = q.group_by(*group_by_cols)
        queries.append(q)

    return __merge_query_results(queries)


def __merge_query_results(queries):
    ret_map = {}
    for q in queries:
        rows = q.all()
        for row in rows:
            submap = ret_map
            for key in row[1:-1]:
                submap[key] = submap.get(key, {})
                submap = submap[key]
            key = row[-1]
            submap[key] = submap.get(key, 0)
            submap[key] += row[0]
    return ret_map


def _map_to_old_grouping_column(passed_column):
    """
    Queries for old DB cannot group using old columns. A quick re-map is added so that the interface remains the same
    but the lookup underneath can access either the new or the old DB.
    :param passed_column:
    :return:
    """
    if passed_column in (TimeLineUser.cmsId, EprUser.cmsId):
        return OldPledge.cmsId
    elif passed_column in (TimeLineUser.instCode, EprUser.mainInst, Institute.code):
        return OldPledge.instCode
    elif passed_column in (Pledge.year, Shift.year):
        return OldPledge.year
    return passed_column


def get_institutes_dues(year, db_session, inst_code=None):
    return _get_dues(db_session=db_session, year=year, grouping_column=TimeLineUser.instCode, inst_code=inst_code)


def get_institute_due(inst_code, year, db_session):
    return next(iter(get_institutes_dues(year=year, db_session=db_session, inst_code=inst_code).values()), 0.0)


def get_people_dues(inst_code, year, db_session):
    return _get_dues(db_session=db_session, year=year, grouping_column=TimeLineUser.cmsId, inst_code=inst_code)


def get_person_due(cms_id, year, db_session):
    return next(
        iter(_get_dues(db_session=db_session, year=year, grouping_column=TimeLineUser.cmsId, cms_id=cms_id).values()),
        0.0)


def get_institutes_worked(year, db_session):
    return _get_worked(db_session=db_session, year=year, grouping_column=Institute.code)


def get_institute_worked(inst_code, year, db_session):
    return _get_worked(db_session=db_session, year=year, grouping_column=Institute.code, inst_code=inst_code).get(
        inst_code, 0.0)


def get_people_worked(inst_code, year, db_session):
    return _get_worked(db_session=db_session, year=year, grouping_column=EprUser.cmsId, inst_code=inst_code)


def get_person_worked(cms_id, year, db_session):
    return _get_worked(db_session=db_session, year=year, grouping_column=EprUser.cmsId, cms_id=cms_id).get(cms_id, 0.0)


def get_institutes_shifts_done(year, db_session):
    _get_shifts_done(db_session=db_session, year=year, grouping_column=TimeLineUser.instCode)


def get_institute_shifts_done(inst_code, year, db_session):
    return _get_shifts_done(db_session=db_session, year=year, inst_code=inst_code,
                            grouping_column=TimeLineUser.instCode).get(inst_code, 0.0)


def get_people_shifts_done(inst_code, year, db_session):
    return _get_shifts_done(db_session=db_session, year=year, inst_code=inst_code, grouping_column=TimeLineUser.cmsId)


def get_person_shifts_done(cms_id, year, db_session):
    return _get_shifts_done(db_session=db_session, year=year, cms_id=cms_id, grouping_column=TimeLineUser.cmsId).get(
        cms_id, 0.0)


def get_annual_author_due(year=None):
    year = year or date.today().year
    if year <= 2015:
        return 4.5
    elif year <= 2017:
        return 3
    return 4


def get_annual_applicant_due(year=None):
    """
    :param year: mostly to be used as starting year of one's application
    :return:
    """
    if year < 2015:
        return 0
    return 6


def correct_applicant_due(cms_id, year, correction, db_session, inst_code=None):
    if correction == 0:
        return correction
    q = db_session.query(TimeLineUser).filter(TimeLineUser.year == year).filter(TimeLineUser.cmsId == cms_id)
    if inst_code:
        q = q.filter(TimeLineUser.instCode == inst_code)
    tlus = q.all()

    total_due = 0
    for tlu in tlus:
        total_due += getattr(tlu, TimeLineUser.dueApplicant.key)
    # in case requested correction was higher than total amassed due for that year
    if correction < 0:
        correction = max(correction, -1 * total_due)
    corrected = 0
    # consider either only the TLs with some due or all in case the total due is zero
    for tlu in [t for t in tlus if (getattr(t, TimeLineUser.dueApplicant.key) > 0 or total_due == 0)]:
        # step is either proportional to TL's due / sum(TL dues) or it's just the correction divided by number of TLs
        correction_step = correction * (total_due and (tlu.dueApplicant / total_due) or (1 / len(tlus)))
        setattr(tlu, TimeLineUser.comment.key, '%s%s: correcting applicant due by %.2f [%.2f -> %.2f]; ' %
                (tlu.comment or '', str(date.today()), correction_step, tlu.dueApplicant, tlu.dueApplicant + correction_step))
        setattr(tlu, TimeLineUser.dueApplicant.key, tlu.dueApplicant + correction_step)
        corrected += correction_step
        db_session.add(tlu)
    db_session.commit()
    return corrected


def correct_author_due(cms_id, year, correction, inst_code=None, reason=None, remarks=None, actor_cms_id=None):
    s = TimeLineUser.session()
    _q = s.query(TimeLineUser).filter(and_(TimeLineUser.cmsId == cms_id, TimeLineUser.year == year))
    if inst_code:
        _q = _q.filter(TimeLineUser.instCode == inst_code)
    _tlus = _q.all()
    _matched_due = sum([_tlu.get(TimeLineUser.dueAuthor) for _tlu in _tlus])

    for _tlu in _tlus:
        _distribution_factor = _tlu.get(TimeLineUser.yearFraction)
        if _matched_due > 0:
            _distribution_factor = (_tlu.get(TimeLineUser.dueAuthor) / _matched_due)
        _d = correction * _distribution_factor
        _tlu.set(TimeLineUser.dueAuthor, max(0, _tlu.get(TimeLineUser.dueAuthor) + _d))
        _tlu.set(TimeLineUser.comment, '{day}: Correctling authorship due by {d}; {previous}'.format(
            previous=_tlu.get(TimeLineUser.comment), d=_d, day=date.today().strftime('%Y-%m-%d')
        ))
        s.add(_tlu)
    asset_data = AppAsset.retrieve(AssetNames.EPR_AUTHOR_REDUCTIONS) or []
    asset_data.append({
        'author_cms_id': cms_id,
        'year': year,
        'correction': correction,
        'inst_code': inst_code,
        'reason': reason,
        'remarks': remarks,
        'actor_cms_id': actor_cms_id,
        'date': str(date.today())
    })
    AppAsset.store(name=AssetNames.EPR_AUTHOR_REDUCTIONS, value=asset_data)

    reasons_map = {_e.get('value'): _e.get('label') for _e in AppAsset.retrieve(AssetNames.EPR_AUTHOR_REDUCTION_REASONS)}
    _q_ppl = Person.session().query(Person). join(InstituteLeader,
        sqlalchemy.and_(
            InstituteLeader.end_date == None,
            InstituteLeader.inst_code == inst_code
        ), isouter=True).filter(sqlalchemy.or_(
            Person.cms_id == cms_id,
            Person.cms_id == InstituteLeader.cms_id
        )
    )
    _ppl = {_r.cms_id: _r for _r in _q_ppl.all()}

    template_class = EmailTemplateEprDueReduction if correction < 0 else EmailTemplateEprDueIncrease
    _email = template_class(recipient=' '.join([_ppl[cms_id].get(_c) for _c in (Person.first_name, Person.last_name)]),
                                           recipient_email=_ppl[cms_id].get(Person.email),
                                           team_leader_emails=', '.join([_p.get(Person.email) for _p in _ppl.values() if _p.get(Person.cms_id) != cms_id]),
                                           year=year, amount=abs(correction), reason=reasons_map.get(reason, reason), remarks=remarks)
    _email.generate_message(store_in_db=True)

    s.commit()
    return sum([_tlu.get(TimeLineUser.dueAuthor) for _tlu in _tlus]) - _matched_due


def correct_future_time_lines(db_session, write_changes=False):

    _this_year = date.today().year

    sq_latest = db_session.query(TimeLineUser.cmsId, func.max(TimeLineUser.id).label('the_id')).filter(
        TimeLineUser.year == _this_year).group_by(TimeLineUser.cmsId).subquery()

    future_tls = db_session.query(TimeLineUser).filter(TimeLineUser.year > _this_year).all()
    latest_tls = db_session.query(TimeLineUser).join(sq_latest, TimeLineUser.id == sq_latest.c.the_id).all()
    latest_tls = {r.cmsId: r for r in latest_tls}

    # make sure not to count the future time line in...
    app_stats = db_session.query(TimeLineUser.cmsId, func.min(TimeLineUser.year).label('start_year'),
        func.sum(TimeLineUser.dueApplicant).label('total')).filter(TimeLineUser.dueApplicant > 0).\
        filter(TimeLineUser.year <= _this_year).group_by(TimeLineUser.cmsId).all()

    app_stats = {r.cmsId: {'start': r.start_year, 'total': r.total} for r in app_stats}

    diff_stats = {}

    logging.debug('Found %d future time-lines' % len(future_tls))

    for tl in future_tls:
        ref_tl = latest_tls.get(tl.cmsId)
        # here we describe the values that we expect to see in the time-lines for the future
        ref_map = {
            TimeLineUser.yearFraction: 1,
            TimeLineUser.year: _this_year + 1,
            # setting due applicant as 0 will require some subsequent checks in the future, but for now be it so... (we could sum up the past dues above though)
            TimeLineUser.dueApplicant: ref_tl.get(TimeLineUser.dueApplicant) > 0 and
                                       app_stats[tl.cmsId]['start'] == _this_year and
                                       get_annual_applicant_due(_this_year + 1) - app_stats[tl.cmsId]['total'] or 0,
            # author due is easier to project
            TimeLineUser.dueAuthor: get_annual_author_due(_this_year + 1)
                    * ((ref_tl.get(TimeLineUser.isAuthor) and tl.get(TimeLineUser.status) == 'CMS') and 1 or 0)
        }
        for k in [TimeLineUser.instCode, TimeLineUser.mainProj, TimeLineUser.status, TimeLineUser.isAuthor,
                  TimeLineUser.isSuspended, TimeLineUser.category]:
            # these values should be propagated directly into the future from the reference time line
            ref_map[k] = ref_tl.get(k)
        # now that we have specified our expectations, let's pitch them against reality
        for k, ref_val in ref_map.items():
            if tl.get(k) != ref_val:
                diff_stats[c2k(k)] = diff_stats.get(c2k(k), 0) + 1
                if write_changes:
                    tl.set(k, ref_val)
                    db_session.add(tl)

    # a quick check for institutes: only the status can change here (as of 11.2017)
    trouble = db_session.query(TimeLineInst, Institute).join(Institute, TimeLineInst.code == Institute.code).\
        filter(TimeLineInst.year == _this_year + 1).filter(Institute.cmsStatus != TimeLineInst.cmsStatus).all()
    for tli, inst in trouble:
        diff_stats[c2k(TimeLineInst.cmsStatus)] = diff_stats.get(c2k(TimeLineInst.cmsStatus), 0) + 1
        if write_changes:
            tli.set(TimeLineInst.cmsStatus, inst.get(Institute.cmsStatus))
            db_session.add(tli)

    if write_changes:
        db_session.commit()
    return diff_stats


class AppDueStats(object):
    def __init__(self, db_session, current_year=None):
        self.s = db_session
        self.year = current_year or date.today().year

        # looks a little redundant but sometimes the TLs are created in advance at the end of the PRECEEDING year
        self.start_years = {}
        self.start_dates = {}
        for row in self.s.query(
                TimeLineUser.cmsId,
                func.min(TimeLineUser.year).label('year'),
                func.min(TimeLineUser.timestamp).label('date')
        ).filter(TimeLineUser.dueApplicant > 0).filter(TimeLineUser.year <= self.year).group_by(
            TimeLineUser.cmsId).all():
            self.start_years[row.cmsId] = getattr(row, 'year')
            self.start_dates[row.cmsId] = getattr(row, 'date')
        self.stats = {year: {} for year in range(2015, self.year + 1)}

        for row in self.s.query(TimeLineUser.cmsId, TimeLineUser.year, func.sum(TimeLineUser.dueApplicant)). \
                filter(and_(TimeLineUser.dueApplicant > 0, TimeLineUser.year <= self.year)) \
                .group_by(TimeLineUser.cmsId, TimeLineUser.year):
            self.stats[row.year][row.cmsId] = row[-1]

    def get_total(self, cms_id):
        return sum([self.stats.get(x, {}).get(cms_id, 0) for x in self.stats.keys()] or [0])

    def get_total_before_this_year(self, cms_id):
        return sum([self.stats.get(x, {}).get(cms_id, 0) for x in self.stats.keys() if x < self.year] or [0])

    def get_first_due_date(self, cms_id):
        start_date = self.start_dates.get(cms_id, None)
        # handle correctly the case when a timeline has been forward-created so the year and date straddle accross 01.01
        if start_date and start_date.year + 1 == self.start_years.get(cms_id) and start_date.day == 31 \
                and start_date.month == 12:
            return start_date + td(days=1)
        return start_date

    def get_first_due_year(self, cms_id):
        """
        :param cms_id:
        :return: the first year when EPR applicant due was assigned or None
        """
        return self.start_years.get(cms_id, None)

    def is_epr_burden_expired(self, cms_id):
        """
        :param cms_id:
        :return: True if the first year of assigned EPR dues is less than current year -1
        """
        first_due_year = self.get_first_due_year(cms_id)
        return first_due_year and first_due_year < self.year - 1

    def __str__(self):
        return 'AppDueStats for %d' % self.year
