from datetime import datetime as dt
from icms_orm.toolkit import VoteDelegation, Voting, VotingMergerMember, VotingMerger, Status
from icms_orm.cmspeople import Institute, Person
from sqlalchemy import desc, or_, and_


class VotingException(Exception):
    pass


def get_upcoming_votings(db_session, now=None, votings=None):
    if now is None:
        now = dt.now()
    if votings is None:
        votings = db_session.query(Voting).filter(Voting.start_time > now).all()
    else:
        votings = [x for x in votings if getattr(x, Voting.start_time.key) > now]
    return votings


def get_vote_delegations(db_session, cms_id=None):
    q = db_session.query(Voting, VoteDelegation).join(VoteDelegation, Voting.code == VoteDelegation.voting_code)
    if cms_id:
        q = q.filter(VoteDelegation.cms_id_from == cms_id).filter(VoteDelegation.status == Status.ACTIVE)
    votings = []
    delegations = dict()
    for v, d in q.all():
        votings.append(v)
        if d:
            delegations[getattr(v, Voting.code.key)] = d
    return votings, delegations


def get_permanent_delegations(db_session, cms_id=None):
    q = db_session.query(VoteDelegation).filter(VoteDelegation.is_long_term == True)
    if cms_id:
        q = q.filter(VoteDelegation.cms_id_from == cms_id)
    return q.all()


def establish_long_term_proxy(db_session, cms_id_from, cms_id_to, cms_id_by=None, specific_inst_code=None):
    """
    :param db_session:
    :param cms_id_from:
    :param cms_id_to:
    :param cms_id_by:
    :return: a tuple containing created/re-activated delegation and a list of de-activated ones (should they exist)
    """
    (ssn, VD) = (db_session, VoteDelegation)
    cms_id_by = cms_id_by or cms_id_from
    cbi_cms_ids = ssn.query(Institute.cbiCmsId).filter(and_(Institute.cmsStatus.ilike('yes'), Institute.cbiCmsId.in_({cms_id_from, cms_id_to}))).all()
    cbi_cms_ids = {x[0] for x in cbi_cms_ids}
    # check if the delegating is a CBI and the delegate isn't
    if cms_id_from not in cbi_cms_ids:
        raise VotingException('User with CMS id %d is not a CBI of a CMS Member Institute and no vote delegation is therefore possible' % cms_id_from)
    elif cms_id_to in cbi_cms_ids:
        raise VotingException('User with CMS id %d is a CBI who cannot act as a permanent proxy' % cms_id_to)

    # ensure the delegate isn't already someone's proxy
    proxy_already = ssn.query(VD).filter(and_(VD.cms_id_to == cms_id_to, VD.status == Status.ACTIVE)).filter(VD.is_long_term == True).count() > 0
    if proxy_already:
        raise VotingException('User with CMS id %d is already acting as a proxy.' % cms_id_to)
    active = ssn.query(VD).filter(VD.cms_id_from == cms_id_from).filter(
        VD.status == Status.ACTIVE).filter(VD.is_long_term == True).all()
    if active:
        for a in active:
            a.status = Status.CANCELLED
            ssn.add(a)
    # try re-activating an existing delegation
    existing = ssn.query(VD).filter(VD.cms_id_to == cms_id_to).filter(
        VD.cms_id_from == cms_id_from).filter(VD.is_long_term == True).\
        filter(VD.specific_inst_code == specific_inst_code).order_by(desc(VD.time_updated)).first()
    if existing:
        existing.status = Status.ACTIVE
        setattr(existing, VD.cms_id_updater.key, cms_id_by)
    else:
        existing = VoteDelegation.new(voting_code=None, cms_id_from=cms_id_from, cms_id_to=cms_id_to,
                                      status=Status.ACTIVE, cms_id_creator=cms_id_by, time_created=None,
                                      cms_id_updater=None, time_updated=None, is_long_term=True,
                                      specific_inst_code=specific_inst_code)
    ssn.add(existing)
    ssn.commit()
    return existing, active


def delegate_vote_to(db_session, cms_id_from, cms_id_to, voting_code, cms_id_by=None, specific_inst_code=None):
    # there's a lot to be checked here, for now we will just let delegate though
    # returns: all created or fetched DB objects: voting delegation, voting
    ssn, VD = (db_session, VoteDelegation)
    cms_id_by = cms_id_by or cms_id_from

    # check delegation deadline
    voting = ssn.query(Voting).filter(Voting.code == voting_code).one_or_none()
    if not voting:
        raise VotingException('Voting %s not found!' % voting_code)
    elif getattr(voting, Voting.delegation_deadline.key) < dt.now():
        raise VotingException('Delegation deadline for voting %s passed on %s at %s' %
                              tuple([voting_code] + getattr(voting, Voting.delegation_deadline.key).strftime('%Y-%m-%d %H:%M').split(' ')))

    # check if cms_id_to can be delegated to
    if is_already_voting(db_session, voting_code, cms_id_to):
        raise VotingException('Specified delegate (CMSid %d) is already busy during the %s voting.' % (cms_id_to, voting_code))

    # check if there is already a Vote Delegation for this voting and from this cms_id_from and for this inst_code
    vd = ssn.query(VD).filter(and_(VD.voting_code == voting_code, VD.cms_id_from == cms_id_from, VD.status == Status.ACTIVE, VD.specific_inst_code == specific_inst_code)).all()
    # check also if desired delegate isn't already a delegate for this voting
    if len(vd) > 1:
        raise VotingException('Multiple active delegations exist for CMS id %d%s and voting code %s. Please contact administartors.' %
            (cms_id_from, specific_inst_code and ', inst code %s' % specific_inst_code or '', voting_code))
    elif vd:
        # if so, update the delegate
        vd = vd[0]
        setattr(vd, VD.cms_id_to.key, cms_id_to)
        setattr(vd, VD.cms_id_updater.key, cms_id_by)
        ssn.add(vd)
    else:
        # otherwise create from scratch
        vd = VD.new(voting_code=voting_code, cms_id_from=cms_id_from, cms_id_to=cms_id_to, status=Status.ACTIVE,
                    cms_id_creator=cms_id_from, specific_inst_code=specific_inst_code)
        ssn.add(vd)
    ssn.commit()
    return vd, voting



def is_already_voting(db_session, voting_code, cms_id):
    """
    Ideally, the method should return True if any of the below is true:
     - cms_id is already delegated to for this voting (and delegation is active)
     - cms_id is a long term proxy and voting is nothing extraordinary
     - cms_id is a CBI of an already voting inst (todo: inst is not checked that well yet)
     - cms_id is a representative of a merger (todo: not done at all)
    We can assume that someone who delegates their vote is not available and therefore does not return to the pool of potential delegates
    todo: if there was a method finding everybody actually voting, then this one would be straightforward!
    """
    (ssn, VD) = (db_session, VoteDelegation)
    # maybe is already a delegate for that voting...
    how_many = ssn.query(VD).filter(VD.voting_code == voting_code).filter(VD.status == Status.ACTIVE).filter(VD.cms_id_to == cms_id).count()
    if how_many > 0:
        return True
    # maybe is already a LTP and voting is a regular one (not an election)
    v = ssn.query(Voting).filter(Voting.code == voting_code).filter(Voting.type.ilike('vote')).count()
    if v > 0:
        how_many = ssn.query(VD).filter(VD.is_long_term == True).filter(VD.cms_id_to == cms_id).filter(VD.voting_code == voting_code).count()
        if how_many > 0:
            return True

    # last but not least, maybe the person is a CBI (of an already voting institute...)
    how_many = ssn.query(Institute).filter(Institute.cbiCmsId == cms_id).filter(Institute.cmsStatus.ilike('yes')).count()
    if how_many > 0:
        return True

    # or maybe he is a representative of a merger...??
    return False


def cancel_delegation(db_session, delegation_id, cms_id_by):
    delegation, voting = db_session.query(VoteDelegation, Voting).\
        outerjoin(Voting, VoteDelegation.voting_code == Voting.code).filter(VoteDelegation.id == delegation_id).one_or_none()
    if not delegation:
        raise VotingException('No such delegation exists!')
    elif not getattr(delegation, VoteDelegation.cms_id_creator.key) == cms_id_by:
        raise VotingException('You cannot cancel what you did not delegate.')
    elif voting and getattr(voting, Voting.delegation_deadline.key) < dt.now():
        raise VotingException('Delegation deadline for voting %s passed on %s at %s. Modifications are no longer possible'
                              % (voting.code, voting.delegation_deadline.strftime('%Y-%m-%d'), voting.delegation_deadline.strftime('%H:%M')))
    else:
        delegation.status = Status.CANCELLED
        setattr(delegation, VoteDelegation.cms_id_updater.key, cms_id_by)
        db_session.add(delegation)
        db_session.commit()
    return delegation, voting

