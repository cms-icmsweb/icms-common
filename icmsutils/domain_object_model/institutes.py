from icms_orm.common import Institute as NewInst
from icms_orm.cmspeople import Institute as OldInst

from icmsutils.exceptions import IcmsException


class Institute(object):
    def __init__(self, code, name, country):
        self._code = code
        self._name = code
        self._country = country if code != 'CERN' else 'CERN'

    @property
    def code(self):
        return self._code

    @property
    def name(self):
        return self._name

    @property
    def country(self):
        return self._country

    @classmethod
    def from_data_object(cls, data_object, overrides=None):
        overrides = overrides or {}
        params = {}
        if isinstance(data_object, OldInst):
            params = dict(code=data_object.code, name=data_object.code, country=data_object.country)
        elif isinstance(data_object, NewInst):
            params = dict(code=data_object.code, name=data_object.code, country=data_object.country_code)
        else:
            raise IcmsException('Cannot translate {0} into a Person object'.format(data_object))
        params.update(overrides)
        return Institute(**params)

    def __hash__(self) -> int:
        return self.code.__hash__()

    def __eq__(self, o: object) -> bool:
        return isinstance(o, Institute) and self.code == o.code
    
    
    
    