#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0


class IcmsException(Exception):
    """
    Something to further sub-class or use as-is whenever an action cannot be resumed because of some erroneous state.
    """
    @property
    def message(self):
        return str(self)


class CadiException(IcmsException):
    """
    A generic class to represent a problem arising somewhere in the context of CADI
    """
    pass


class IcmsInsufficientRightsException(IcmsException):

    def __init__(self, *args: object, **kwargs: object) -> None:
        args = args or ['User does not have the privileges necessary to perform requested action.']
        super().__init__(*args)


class AuthorListException(CadiException):
    pass


class FwdSyncException(IcmsException):
    pass
