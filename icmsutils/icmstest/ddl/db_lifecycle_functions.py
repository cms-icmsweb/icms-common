import traceback
import icms_orm
import pytest
import subprocess
import logging

from sqlalchemy import MetaData
from sqlalchemy import create_engine
from icmscommon import ConfigGovernor as ConGov, DbGovernor
from icmsutils.dbutils import pg_parse_login_pass_host_db as _extract_db_login_pass_host_db

#-ap
# the following DBs need to exist in the mysql service:
#
# create database test_cmspeople;
# create database test_cmsanalysis;
# create database test_metadata;
#
# and the user icms_test needs to be able to login on 'localhost' (not just 127.0.0.1 or ::1)
#
#-ap end

"""
For bootstrapping dev/test DBs, check icms-orm's db_bootstrap_script.py (under icms_orm.configuration)
"""

def recreate_db_by_bind(bind, *args, **kwargs):
    manager = DbGovernor.get_db_manager()
    manager.drop_all(bind=bind)
    manager.create_all(bind=bind)

def recreate_people_db():
    return recreate_db_by_bind(bind=icms_orm.cms_people_bind_key())

def recreate_old_cadi_db():
    recreate_db_by_bind(bind=icms_orm.cadi_bind_key())

def recreate_old_notes_dbs():
    recreate_db_by_bind(bind=icms_orm.old_notes_bind_key())
    recreate_db_by_bind(bind=icms_orm.old_notes_wf_bind_key())

def recreate_icms_databases(*args, **kwargs):
    manager = DbGovernor.get_db_manager()
    manager.drop_all(bind=icms_orm.epr_bind_key())
    manager.drop_all(bind=icms_orm.toolkit_bind_key())
    manager.drop_all(bind=icms_orm.cms_common_bind_key())
    manager.create_all(bind=icms_orm.cms_common_bind_key())
    manager.create_all(bind=icms_orm.toolkit_bind_key())
    manager.create_all(bind=icms_orm.epr_bind_key())

# Those 3 better be recreated all at once to avoid nasty surprises (cross-schema foreign keys and such)
recreate_common_db = recreate_icms_databases
recreate_toolkit_db = recreate_icms_databases
recreate_epr_db = recreate_icms_databases