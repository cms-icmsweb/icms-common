"""
LDAP querying utilities
"""
#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

import ldap
import re
import os
import logging
from functools import wraps

from ldap.controls import SimplePagedResultsControl


class LdapPerson(object):
    def __init__(self, ldap_entry):
        self.__set_blank()
        for key, val in ldap_entry.items():
            if key == 'memberOf':
                for item in val:
                    m = re.search(r'CN=([^,]+)', str(item))
                    e_group = m.group(1)
                    if 'cms' in e_group.lower():
                        self.cmsEgroups.append(e_group)
                    else:
                        self.cernEgroups.append(e_group)
            elif key == 'department':
                self.department = val[0].decode('utf-8')
            elif key == 'displayName':
                self.fullName = val[0].decode('utf-8')
            elif key == 'division':
                self.division = val[0].decode('utf-8')
            elif key == 'employeeID':
                self.hrId = int(val[0])
            elif key == 'givenName':
                self.given_name = val[0].decode('utf-8')
                self.firstName = val[0].decode('utf-8')
            elif key == 'unixHomeDirectory':
                self.unixHome = val[0].decode('utf-8')
            elif key == 'telephoneNumber':
                self.officePhone = val[0].decode('utf-8')
            elif key == 'sn':
                self.familyName = val[0].decode('utf-8')
            elif key == 'cernSection':
                self.section = val[0].decode('utf-8')
            elif key == 'gidNumber':
                self.gid = int(val[0])
            elif key == 'physicalDeliveryOfficeName':
                self.office = val[0].decode('utf-8')
            elif key == 'mail':
                self.mail = val[0].decode('utf-8')
            elif key == 'postOfficeBox':
                self.postOfficeBox = val[0].decode('utf-8')
            elif key == 'userAccountControl':
                self.accountStatus = LdapPerson._account_status_from_code(
                    val[0])
            elif key == 'mobile':
                self.mobilePhone = val[0].decode('utf-8')
            elif key == 'name':
                self.login = val[0].decode('utf-8')

    def __set_blank(self):
        self.gid = None
        self.hrId = None
        self.fullName = None
        self.firstName = None
        self.familyName = None
        self.institute = None
        self.department = None
        self.division = None
        self.section = None
        self.group = None
        self.cmsEgroups = []
        self.cernEgroups = []
        self.unixHome = None
        self.officePhone = None
        self.mobilePhone = None
        self.office = None
        self.mail = None
        self.postOfficeBox = None
        self.accountStatus = None
        self.login = None

    def is_in_zh(self):
        return self.is_in_egroup('zh')

    def is_in_egroup(self, egroup_name):
        return egroup_name in self.cmsEgroups or egroup_name in self.cernEgroups

    @staticmethod
    def _account_status_from_code(code):
        _code = int(code) if code else None
        if _code == 0x200:
            return 'Active'
        elif _code == 0x800000:
            return 'Password expired'
        else:
            return 'Inactive (code: %s)' % str(code)

    def __str__(self):
        return self.__repr__()

    def __repr__(self):
        return '<%s object for %s [#%d], a member of %d CMS e-groups and %d CERN e-groups>' % \
               (self.__class__.__name__, self.fullName, self.hrId,
                len(self.cmsEgroups), len(self.cernEgroups))


def _extract_ldap_dict_decorator(f):
    """
    It will take a function returning a list of full LDAP results and return a wrapper that calls it first and
    only leaves the dictionary part of the result
    """
    def inner(*args, **kwargs):
        res = f(*args, **kwargs)
        return [x[1] for x in res]
    return inner


class LdapProxy(object):

    ATTRLIST_LITE = ['department', 'displayName', 'division', 'employeeID', 'givenName',
                     'unixHomeDirectory', 'telephoneNumber', 'sn', 'cernSection', 'gidNumber',
                     'physicalDeliveryOfficeName', 'mail', 'postOfficeBox', 'userAccountControl', 'mobile', 'name']

    ATTRLIST_FULL = ATTRLIST_LITE + ['memberOf']

    class AccountType(object):
        PRIMARY = 1
        SECONDARY = 2
        SERVICE = 4

        ANY = PRIMARY | SECONDARY | SERVICE
        ALL_BUT_SERVICE = PRIMARY | SECONDARY

    __instance = None

    @classmethod
    def get_instance(cls, **kwargs):
        if not cls.__instance:
            cls.__instance = cls(**kwargs)
        return cls.__instance

    def __init__(self, ldap_uri_override=None, retry_max_override=None, retry_delay_override=None, wrapper_reconnect_attempts=1):
        ldap.set_option(ldap.OPT_X_TLS_REQUIRE_CERT, ldap.OPT_X_TLS_NEVER)
        self._ldap_uri = ldap_uri_override or os.environ.get(
            'LDAP_URI', 'ldaps://xldap.cern.ch')
        self._ldap_retry_max = retry_max_override or 3
        self._ldap_retry_delay = retry_delay_override or 5.0
        self._wrapper_reconnect_attempts = wrapper_reconnect_attempts
        logging.info(
            'ldapAuth::search> using ldap server at "{0}"'.format(self._ldap_uri))

        self.l = ldap.ldapobject.ReconnectLDAPObject(
            self._ldap_uri, retry_max=self._ldap_retry_max, retry_delay=self._ldap_retry_delay)

    def __process_result_set(self, input_result_set):
        return [self.__process_result(x) for x in input_result_set]

    def __process_result(self, result):
        return repr(result)

    def __basic_query(self, filter, attrlist=None):
        _exception = None
        _reconnect_attempts_left = max(self._wrapper_reconnect_attempts, 0)
        while True:
            try:
                page_control = SimplePagedResultsControl(
                    True, size=1000, cookie='')
                def _search(): return self.l.search_ext('OU=Users,OU=Organic Units,DC=cern,DC=ch',
                                                        ldap.SCOPE_SUBTREE, filter, attrlist=attrlist, serverctrls=[page_control])
                response = _search()
                result = []
                pages = 0
                while True:
                    pages += 1
                    rtype, rdata, rmsgid, serverctrls = self.l.result3(
                        response)
                    result.extend(rdata)
                    controls = [control for control in serverctrls if control.controlType ==
                                SimplePagedResultsControl.controlType]
                    if not controls or not controls[0].cookie:
                        break
                    page_control.cookie = controls[0].cookie
                    response = _search()
                return result
            except Exception as e:
                logging.info('LDAP query failed because of {0}'.format(e))
                if _reconnect_attempts_left > 0:
                    logging.info('About to try reconnecting (attempts left: {0})'.format(
                        _reconnect_attempts_left))
                    _reconnect_attempts_left -= 1
                    self.l.reconnect(
                        uri=self._ldap_uri, retry_max=self._ldap_retry_max, retry_delay=self._ldap_retry_delay)
                else:
                    raise e

    @staticmethod
    def __emp_type_cond(account_types=AccountType.ANY):
        primary = account_types & LdapProxy.AccountType.PRIMARY
        secondary = account_types & LdapProxy.AccountType.SECONDARY
        service = account_types & LdapProxy.AccountType.SERVICE

        allowed = []
        for name, allow in [('Primary', primary), ('Secondary', secondary), ('Service', service)]:
            if allow:
                allowed.append('(employeeType=%s)' % name)
        return '(|%s)' % ''.join(allowed)

    @_extract_ldap_dict_decorator
    def _find_people_by_filter(self, filter, attrlist=None):
        return self.__basic_query(filter, attrlist=attrlist)

    def get_person_by_hrid(self, hrid, account_types=AccountType.PRIMARY):
        people = self.find_people_by_filter('(&(employeeID=%d)%s)' % (
            hrid, LdapProxy.__emp_type_cond(account_types)))
        return people[0] if people else None

    def find_people_by_family_name(self, family_name, attrlist=None):
        return self.find_people_by_filter('(&(sn=*%s*)%s)' % (family_name, LdapProxy.__emp_type_cond()), attrlist=attrlist)

    def find_people_by_gid(self, gid, attrlist=None):
        _filter = '(&(gidNumber={gid}){cond})'.format(
            gid=gid, cond=LdapProxy.__emp_type_cond())
        return self.find_people_by_filter(filter=_filter, attrlist=attrlist)

    def get_person_by_mail(self, email, account_types=AccountType.PRIMARY):
        people = self.find_people_by_filter('(&(mail=%s)%s)' % (
            email, LdapProxy.__emp_type_cond(account_types)))
        return people[0] if people else None

    def get_person_by_login(self, login, account_types=AccountType.PRIMARY):
        people = self.find_people_by_filter('(&(cn=%s)%s)' % (
            login, LdapProxy.__emp_type_cond(account_types)))
        return people[0] if people else None

    def find_people_by_hrids(self, hrids, account_types=AccountType.PRIMARY):
        hrid_subquery = ''.join(['(employeeID=%d)' % hrid for hrid in hrids])
        people = self.find_people_by_filter('(&%s(|%s))' % (
            LdapProxy.__emp_type_cond(account_types), hrid_subquery))
        return people

    def find_people_by_filter(self, filter, attrlist=None):
        """
        :param filter: a custom LDAP filter, like '(&(employeeID=12321)(employeeType=Primary))'
        :return:
        """
        data = self._find_people_by_filter(filter=filter, attrlist=attrlist)
        return [LdapPerson(x) for x in data]

    @staticmethod
    def authenticate(username, password):
        l = ldap.initialize('ldaps://ldap.cern.ch')
        ldap.set_option(ldap.OPT_X_TLS_REQUIRE_CERT, ldap.OPT_X_TLS_NEVER)

        username = 'cn=%s,ou=users,o=cern,c=ch' % username.strip()

        try:
            l.simple_bind_s(username, password)
        except ldap.INVALID_CREDENTIALS:
            return False
        except Exception as error:
            raise error
        l.unbind()
        return True


if __name__ == '__main__':
    p = LdapProxy.get_instance()
    print(p.get_person_by_hrid(744616))
    print(p.find_people_by_family_name('Smith'))
