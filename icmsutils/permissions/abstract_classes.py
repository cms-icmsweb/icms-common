import abc
from typing import List, Optional, Type
from icmsutils.permissions.authorization_context import AuthorizationContext
from icmsutils.permissions.wrappers import AccessRuleWrapper
from icmsutils.permissions.rule_checkers import RuleSetChecker
from icms_orm.toolkit import RestrictedActionTypeValues
import logging


class AbstractRestrictedResource(metaclass=abc.ABCMeta):
    @property
    def id(self):
        return self._get_id()

    @abc.abstractmethod
    def _get_id(self):
        pass


class AbstractAccessRulesProvider(metaclass=abc.ABCMeta):
    @classmethod
    @abc.abstractclassmethod
    def get_rules_for_resource_and_action(cls, resource: AbstractRestrictedResource, action: str) -> List[AccessRuleWrapper]:
        """
        Rules at the moment can be dictionaries or lists thereof. Crude...
        """
        return []


class AbstractPermissionsManager(metaclass=abc.ABCMeta):
    @classmethod
    @abc.abstractclassmethod
    def get_rules_provider_class(cls) -> Type[AbstractAccessRulesProvider]:
        return AbstractAccessRulesProvider

    @classmethod
    @abc.abstractclassmethod
    def get_resources_manager_class(cls) -> Type['AbstractRestrictedResourcesManager']:
        return AbstractRestrictedResourcesManager

    @classmethod
    def can_perform(cls: Type['AbstractPermissionsManager'], action: str, resource_key: str, context: AuthorizationContext):
        """
        Resolves the action and resource before handing the work over. 
        """
        assert action in RestrictedActionTypeValues.values(
        ), f'Unknown action {action}'

        resources_manager_class: Type[AbstractRestrictedResourcesManager] = cls.get_resources_manager_class(
        )
        assert issubclass(resources_manager_class,
                          AbstractRestrictedResourcesManager)

        resource: Optional[AbstractRestrictedResource] = resources_manager_class.\
            get_resource_by_key_and_context(key=resource_key, context=context)

        if resource is None:
            logging.debug(
                f'Key {resource_key} does not match any restricted resources - falling back to default rules')
        else:
            logging.debug(
                f'Key {resource_key} matches restricted resource {resource.id}')

        rule_set_checker = RuleSetChecker(context)
        return cls.can_perform_action_on_resource(action=action, resource=resource, rule_set_checker=rule_set_checker)

    @classmethod
    def can_perform_action_on_resource(cls, action: str, resource: AbstractRestrictedResource, rule_set_checker: RuleSetChecker):
        assert action in RestrictedActionTypeValues.values(
        ), f'Unknown action {action}'
        rules_provider_class = cls.get_rules_provider_class()

        assert issubclass(
            rules_provider_class, AbstractAccessRulesProvider), f'{rules_provider_class.__name__} seems not to subclass {AbstractAccessRulesProvider.__name__}'
        rule_wrappers = rules_provider_class.get_rules_for_resource_and_action(
            resource, action)
        rules = []
        _wrapper: AccessRuleWrapper
        for _wrapper in rule_wrappers:
            assert isinstance(_wrapper, AccessRuleWrapper)
            rules.append(_wrapper.rule)
        result = rule_set_checker.check_rule_set(rules)
        return result


class AbstractRestrictedResourcesManager(metaclass=abc.ABCMeta):
    @classmethod
    @abc.abstractclassmethod
    def get_resource_by_key_and_context(cls, key: str, context: AuthorizationContext) -> Optional[AbstractRestrictedResource]:
        return None
