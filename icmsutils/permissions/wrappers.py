class AccessRuleWrapper():
    """
    A thin wrapper around a dict, list or some primitive type originally meant to represent a rule.
    Leaves some headroom to implement aspects like rule validation.
    """
    def __init__(self, rule_object):
        self._rule_object = rule_object

    @property
    def rule(self):
        return self._rule_object