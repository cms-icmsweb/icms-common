from icms_orm.common import PrehistoryTimeline
import logging
from icmsutils.prehistory import MultipleRegistrationLineException, RegistrationIncongruenceException, \
    PrehistoryException
from icmsutils.prehistory.prehistory_entries import PrehistoryRegistration, PrehistoryChange, PrehistoryFromPh, \
    PrehistoryEntry

UNKNOWN = 'INFO_MISSING'


class PrehistoryParser(object):

    ptn_reg = PrehistoryRegistration.pattern
    ptn_change = PrehistoryChange.pattern

    ptn_change_det_a = PrehistoryChange.pattern_details_a
    ptn_change_det_b = PrehistoryChange.pattern_details_b

    ptn_from_ph = PrehistoryFromPh.pattern

    def __init__(self, history_string, person_obj):
        self.person = person_obj
        self.history_lines = PrehistoryParser.split_raw_history_into_lines(history_string)
        self.events = []

        prevLine = None
        for line in self.history_lines:
            if prevLine and line == prevLine: 
                # logging.warning( f'skipping duplicate line when creating history {line} ' )
                continue # ignore duplicate lines
            prevLine = line
            last_event = PrehistoryEntry.from_line(line)            
            if last_event:
                self.events.append(last_event)
                continue

        self.events = tuple(self.events)

    @classmethod
    def split_raw_history_into_lines(cls, raw_history):
        nl = '|*:*|'
        raw_history = raw_history

        raw_history = raw_history.replace('\n\r', nl)
        raw_history = raw_history.replace('\r\n', nl)
        raw_history = raw_history.replace('\n', nl)
        raw_history = raw_history.replace('\r', nl)

        return [x.strip() for x in raw_history.split(nl) if x.strip()]

    @classmethod
    def merge_lines_into_raw_history(cls, lines):
        return '\r\n'.join(lines)

    def get_events(self) -> [PrehistoryEntry]:
        return self.events

    def get_raw_lines(self):
        return self.history_lines

    def generate_timelines(self, suppress_prehistory_exceptions=False):
        lines = []
        try:
            for line in self.yield_timelines():
                lines.append(line)
        # At some point it already seemed that majority of proper registration lines was captured correctly
        # if self.history_lines and registration_event is None:
        #     raise NoRegistrationLineException('No registration event found for #%d' % self.person.cmsId)
        except PrehistoryException as pe:
            if suppress_prehistory_exceptions is True:
                logging.warning(pe.message)
            else:
                raise pe
        return lines

    @staticmethod
    def _nullify_unknown_fields(timeline):
        """
        Internal method that removes the UNKNOWN value and replaces it with None.
        Objects treated like this can later be insterted into a DB without violating the integrity constraints
        :param timeline:
        :return: PrehistoryTimeline
        """
        for field in [PrehistoryTimeline.activity_cms, PrehistoryTimeline.inst_code, PrehistoryTimeline.status_cms]:
            if timeline.get(field) == UNKNOWN:
                timeline.set(field, None)
        return timeline

    def _find_identity_operations(self, event, current_tl, attributes_to_watch = None):
        attributes_to_watch = attributes_to_watch or []
        misrepresented = set()
        if event.old_values and event.new_values:
            for k in event.old_values or event.new_values:
                if k in attributes_to_watch and \
                        event.new_values.get(k) == event.old_values.get(k) and k in {PrehistoryEntry.Attr.INST_CODE, PrehistoryEntry.Attr.CMS_ACTIVITY}:
                    logging.debug('Identity operation for #%d on %s: %s (%s => %s)!' % (current_tl.cms_id, str(event.date), k, event.new_values.get(k), event.old_values.get(k)))
                    misrepresented.add(k)
        return misrepresented


    def _reinterpret_as_post_change_value_if_needed(self, event, current_tl):
        # translate the supposedly old (pre-change) values) into the actually new (post-change).
        # (some old history entries contain not the pre-change state, but the post-change one)
        for attr, next_tl_value in [(PrehistoryEntry.Attr.CMS_STATUS, current_tl.status_cms),
                                    (PrehistoryEntry.Attr.CMS_ACTIVITY, current_tl.activity_cms),
                                    (PrehistoryEntry.Attr.INST_CODE, current_tl.inst_code)]:
            if event.old_values and attr in event.old_values and (
                    event.new_values is None or attr not in event.new_values):
                if event.old_values[attr] == next_tl_value:
                    del (event.old_values[attr])
                    event.new_values = event.new_values or {}
                    event.new_values[attr] = next_tl_value

    def _check_consistency(self, time_line, attr, value, the_date):
        tl_attr = PrehistoryEntry.Attr.time_line_attr(attr)
        if value != time_line.get(tl_attr):
            raise PrehistoryException(
                'Found an inconsistency in %s for %d. On %s it seemed to be %s, '
                'but history line suggests %s.' % (attr, self.person.cmsId, the_date, time_line.status_cms, value))

    def _value_backward_lookahead(self, start_idx, sought_attr, likely_guess=None):
        """
        Sometimes we'll find what looks to be or definitely is the trace of post-change value only.
        So we'll need to look further into the past and find some events that could hint at the pre-change value of
        that attribute.
        Finding a new value is GREAT, it's the BEST thing that can happen to us now. (Donald wouldn't put it better)
        But sometimes we might find what seems to be an old value, which either is one indeed or is in fact the
        new value (wrt the event that we found).
        In case of the latter, the old value is anyway the nearest we can get to the truth. So let's use the bridging
        approach, where we'll assume that the old value has either remained unchanged or that it actually denotes the
        new value for the variable in question.

        :param start_idx: index of the first event to consider for lookup
        :param sought_attr: the attribute we're not sure of
        :param likely_guess: used when the calling context offers a good idea of what we might expect, like fromPH
        :return: the most likely previous value of the attribute in question or UNKNOWN if we really have not a clue
        """
        for idx in range(start_idx, len(self.events)):
            event = self.events[idx]
            # ensuring that likely guess has precedence over values from the former valset
            for values_set in [event.new_values, {} if likely_guess is None else {sought_attr: likely_guess}, event.old_values]:
                if values_set and sought_attr in values_set:
                    return values_set.get(sought_attr)
        logging.debug( f'The backward lookahead failed when looking for any previous mention of {sought_attr} for {self.person.cmsId}')
        return UNKNOWN

    def yield_timelines(self):
        """
        Goes deeper and deeper into the history, starting with the present state and attempting to recreate everything
        that happened in-between.
        :param suppress_prehistory_exceptions when True, discovering inconsistencies will stop processing the history
        but the time-lines found so far will still be returned.
        :return:
        """
        # current here is relative to how far back we are at the moment...
        current_tl = PrehistoryTimeline(cms_id=self.person.cmsId, start_date=None, status_cms=self.person.status,
                                         inst_code=self.person.instCode,
                                         activity_cms=self.person.activity and self.person.activity.name or None)

        registration_event = None

        for event_index, event in enumerate(self.events):

            if isinstance(event, PrehistoryChange):
                # in this method we rebrand some old values as new ones because they are consistent with the
                # now-present state
                self._reinterpret_as_post_change_value_if_needed(event, current_tl)
                self._find_identity_operations(event, current_tl, attributes_to_watch=[PrehistoryEntry.Attr.INST_CODE])
                # earlier line is the one we are constructing / discovering right now
                earlier_line = PrehistoryTimeline.copy(current_tl)
                earlier_line.end_date = event.date
                start_another_line = False
                Attr = PrehistoryEntry.Attr

                for attr in [Attr.CMS_STATUS, Attr.CMS_ACTIVITY, Attr.INST_CODE]:
                    time_line_attr = Attr.time_line_attr(attr)
                    if attr in event.old_values:
                        # old values are copied to the earlier line (I guess)
                        earlier_line.set(time_line_attr, event.old_values.get(attr))
                        start_another_line = True
                    elif attr in event.new_values:
                        self._check_consistency(current_tl, attr, event.new_values.get(attr), event.date)
                        # finding the old value
                        prev_value = self._value_backward_lookahead(event_index + 1, attr)
                        earlier_line.set(time_line_attr, prev_value)
                        start_another_line = True
                # the earlier line becomes the most recent as we move back in time
                if start_another_line:
                    # print ( f'Depositing another TimeLine with date {str(event.date)} -- {event.old_values} -> {event.new_values} ' )
                    current_tl.start_date = event.date
                    yield PrehistoryParser._nullify_unknown_fields(current_tl)
                    current_tl = earlier_line
            elif isinstance(event, PrehistoryRegistration):
                if registration_event is not None:
                    raise MultipleRegistrationLineException('Multiple registration events found for %d' % self.person.cmsId)
                registration_event = event
                current_tl.start_date = event.date

                unexpected_string = ''

                # fill the UNKNOWNs in the current timeline first:
                if current_tl.status_cms == UNKNOWN:
                    current_tl.status_cms = event.new_values.get(PrehistoryEntry.Attr.CMS_STATUS)
                if current_tl.inst_code == UNKNOWN:
                    current_tl.inst_code = event.new_values.get(PrehistoryEntry.Attr.INST_CODE)
                if current_tl.activity_cms == UNKNOWN:
                    current_tl.activity_cms = event.new_values.get(PrehistoryEntry.Attr.CMS_ACTIVITY)

                for attribute, reference in [(PrehistoryEntry.Attr.CMS_ACTIVITY, current_tl.activity_cms),
                                             (PrehistoryEntry.Attr.CMS_STATUS, current_tl.status_cms),
                                             (PrehistoryEntry.Attr.INST_CODE, current_tl.inst_code)]:
                    if event.new_values[attribute] != reference:
                        unexpected_string += unexpected_string and ', ' or ''
                        unexpected_string += 'for %s - expected %s, found %s' % (attribute, reference, event.new_values[attribute])

                if unexpected_string:
                    raise RegistrationIncongruenceException('Reached registration timeline for #%d and discovered inconsistencies: %s'
                                              % (self.person.cmsId, unexpected_string))

                yield PrehistoryParser._nullify_unknown_fields(current_tl)
            elif isinstance(event, PrehistoryFromPh):
                # this is a weird line that only indicates the current at a time (by convention called "new") values
                # however, it might help us capture some changes that failed to be recorded in the history
                # IF the next (more recent) event has old_values, they should be the same as this one's new_values
                #   - otherwise we have a missing change above this event
                # IF the previous event (more ancient) has new_values that are different than this one's new_values
                #   - we have missed a change but can use this event as approximate temporal boundary

                if PrehistoryEntry.Attr.CMS_STATUS in event.new_values:

                    present_value = event.new_values.get(PrehistoryEntry.Attr.CMS_STATUS)
                    # With fromPHs we have a strong hunch that the value reported might be correct both before and after
                    # still we should not make assumptions on what it could be, lest we get some undesired biasing in the result ... 
                    likely_guess = event.new_values.get(PrehistoryEntry.Attr.CMS_STATUS) if event.new_values.get(PrehistoryEntry.Attr.CMS_STATUS) != present_value else None
                    previous_value = self._value_backward_lookahead(event_index+1, PrehistoryEntry.Attr.CMS_STATUS, likely_guess=likely_guess)
                            # likely_guess=event.new_values.get(PrehistoryEntry.Attr.CMS_STATUS))

                    self._check_consistency(current_tl, PrehistoryEntry.Attr.CMS_STATUS, present_value, event.date)

                    if previous_value != UNKNOWN and present_value != previous_value:
                        logging.debug( f'A change in status for {self.person.cmsId} has potentially slipped through. Treating a fromPH event as a state boundary.')
                        # ASSUMING that the new_value here is the same as the old_value in previously examined event
                        earlier_line = PrehistoryTimeline.copy(current_tl)
                        earlier_line.end_date = event.date
                        earlier_line.status_cms = previous_value
                        current_tl.start_date = event.date
                        yield PrehistoryParser._nullify_unknown_fields(current_tl)
                        current_tl = earlier_line

        if current_tl.start_date is None:
            current_tl.start_date = self.person.dateCreation.date()
            # an alternative here would be to
            # current_tl.start_date = self.events and self.events[-1].date or self.person.dateCreation.date()
            yield PrehistoryParser._nullify_unknown_fields(current_tl)


