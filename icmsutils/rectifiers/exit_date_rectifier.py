import traceback
from icms_orm.cmspeople import Person, PersonHistory, PERSON_STATUS_CMS, PERSON_STATUS_EXMEMBER
from icms_orm.common import PersonStatus
from icms_orm.epr import TimeLineUser
from icmsutils.prehistory import PrehistoryParser
from icms_orm.common import PrehistoryTimeline
from icmsutils.exceptions import IcmsException
from datetime import date, datetime, timedelta
import logging
import sqlalchemy as sa


class ExitDateRectifier(object):
    def __init__(self, cms_id, new_departure_date):
        self._new_departure_date = new_departure_date
        self._cms_id = cms_id

        (self._person, self._history) = Person.session().query(Person, PersonHistory). \
            join(PersonHistory, Person.cmsId == PersonHistory.cmsId).filter(Person.cmsId == self._cms_id).one()

        pp = PrehistoryParser(history_string=self._history.get(PersonHistory.history), person_obj=self._person)
        self._parsed_tls = pp.generate_timelines(suppress_prehistory_exceptions=True)
        self._parsed_departure_date = None
        self._db_departure_date = None

        self._epr_post_departure_time_line = None
        self._epr_pre_departure_time_line = None

        self._common_post_departure_time_line = None
        self._common_pre_departure_time_line = None
        self._common_pre_departure_time_lines = []

        self.setup_and_check()

    def setup_and_check(self):
        """
        Asserts that the case is simple enough to be automatically fixed
        """
        try:
            self._db_departure_date = datetime.strptime(self._person.get(Person.exDate), '%Y-%m-%d').date()
        except Exception as e:
            raise IcmsException('Failed to parse the exit date stored in DB. Root cause: %s' % e.message)

        logging.debug('DB departure date is %s' % str(self._db_departure_date))

        for i in range(0, len(self._parsed_tls) - 1, 1):
            if self._parsed_tls[i].get(PrehistoryTimeline.status_cms) == 'EXMEMBER' and self._parsed_tls[i+1].get(
                    PrehistoryTimeline.status_cms) == 'CMS':
                self._parsed_departure_date = self._parsed_tls[i].get(PrehistoryTimeline.start_date)
                break

        logging.debug('Parsed departure date: %s' % str(self._parsed_departure_date))

        if self._parsed_departure_date is not None:
            if self._parsed_departure_date.year != date.today().year:
                logging.warn('Altering the departure date that is already before the Jan 1 of present year [DOUBLE CHECK THE OUTCOME!]')
            elif self._parsed_departure_date != self._db_departure_date:
                raise IcmsException('DB departure date differs from the one extracted from timelines')
            elif self._new_departure_date.year != self._parsed_departure_date.year:
                raise IcmsException('Requested departure date falls on a different year than the registered one!')
            elif self._new_departure_date > self._db_departure_date:
                raise IcmsException('Moving the departure date forward is not supported.')
        else:
            raise IcmsException('Could not find the history lines describing the departure moment.')

        # Find EPR time-lines to modify
        epr_lines = Person.session().query(TimeLineUser).filter(TimeLineUser.cmsId == self._person.cmsId). \
            filter(TimeLineUser.year == self._new_departure_date.year).order_by(TimeLineUser.timestamp).all()
        for i in range(0, len(epr_lines)-1, 1):
            (pre, post) = (epr_lines[i], epr_lines[i+1])
            if post.get(TimeLineUser.status) == PERSON_STATUS_EXMEMBER and pre.get(TimeLineUser.status) != PERSON_STATUS_EXMEMBER:
                # found the splitting point
                (self._epr_pre_departure_time_line, self._epr_post_departure_time_line) = (pre, post)
                logging.debug('EPR departure time-line starts on %s' % str(post.get(TimeLineUser.timestamp)))
                break
        if not 0 <= (self._epr_post_departure_time_line.get(TimeLineUser.timestamp).date() - self._db_departure_date).days <= 1:
            raise IcmsException('Found some weird post-departure EPR time-line. '
                                'Should be within one day of the previously stored departure date.')
        logging.debug('EPR post-departure time line starts on %s' % str(self._epr_post_departure_time_line.get(TimeLineUser.timestamp)))
        logging.debug('EPR pre-departure time line starts on %s' % str(self._epr_pre_departure_time_line.get(TimeLineUser.timestamp)))

        # Find common time-lines (PersonStatus objects) to modify
        status_lines = Person.session().query(PersonStatus).filter(PersonStatus.cms_id == self._cms_id).filter(sa.or_(
            PersonStatus.start_date <= self._db_departure_date,
            PersonStatus.end_date <= self._db_departure_date)).order_by(PersonStatus.start_date).all()

        if len(status_lines) == 0:
            logging.warn('Found no PersonStatus entries representing the departure...')
        else:
            try:
                post_departure_lines = [tl for tl in status_lines if tl.get(PersonStatus.start_date) >= self._db_departure_date]
                assert len(post_departure_lines) == 1, 'One and only one of found time-lines should qualify as post-departure (found %d)' % len(post_departure_lines)
                assert (lambda x: x is None or x >= self._new_departure_date)(post_departure_lines[0].get(PersonStatus.end_date)), \
                    'Post-departure time line [cms common!] ends [{time_line_end}] before the new departure time [{date_exit}]'.\
                        format(date_exit=self._new_departure_date, time_line_end=post_departure_lines[0].get(PersonStatus.end_date))
                self._common_post_departure_time_line = post_departure_lines[0]
                self._common_pre_departure_time_lines = status_lines[0:-1]
                logging.debug('Found %d pre-departure time lines (COMMON)' % len(self._common_pre_departure_time_lines))
                logging.debug('Found a post-departure time line (COMMON) starting at %s' % self._common_post_departure_time_line.start_date)

            except AssertionError as error:
                raise IcmsException(error.message)

    def _fix_people_db_entries(self):
        """
        Basically just replace the old timestamp with the new one [reshuffling lines NOT (yet?) supported]
        and set "exit date"
        """
        wrong_date_str, good_date_str = [x.strftime('%d/%m/%Y') for x in (self._parsed_departure_date, self._new_departure_date)]
        self._history.history = self._history.history.replace(wrong_date_str, good_date_str)
        self._person.set(Person.exDate, self._new_departure_date.strftime('%Y-%m-%d'))
        Person.session().add(self._history)
        Person.session().add(self._person)

    def _fix_epr_time_lines(self):
        pre, post = (self._epr_pre_departure_time_line, self._epr_post_departure_time_line)
        # epr time-line might have a starting date one day later than the db departure that would indicate
        frac_diff = (self._epr_post_departure_time_line.get(TimeLineUser.timestamp).date() - self._new_departure_date).days / 365.0
        logging.debug('Year fraction diff is %.2f' % frac_diff)

        frac_pre, frac_post = [tl.get(TimeLineUser.yearFraction) for tl in (pre, post)]

        for attr in [TimeLineUser.dueApplicant, TimeLineUser.dueAuthor]:
            # scaling the dues: up for the expanded tline (post), down for the shrunk one (post)
            pre.set(attr, pre.get(attr) * (1.0 - frac_diff / frac_pre))
            post.set(attr, post.get(attr) * (1.0 + frac_diff / frac_pre))
        pre.set(TimeLineUser.yearFraction, frac_pre - frac_diff)
        post.set(TimeLineUser.yearFraction, frac_post + frac_diff)

        # the new time-line needs to start after the previous one (might end up close around the Jan 1)
        new_timestamp = max(datetime(self._new_departure_date.year, self._new_departure_date.month, self._new_departure_date.day, 0, 0, 0), pre.get(TimeLineUser.timestamp))
        new_timestamp += timedelta(seconds=42)

        post.set(TimeLineUser.timestamp, new_timestamp)
        comment_text = '%s: moving back the departure date from %s to %s; ' % tuple((d.strftime('%Y-%m-%d') for d in [date.today(), self._db_departure_date, self._new_departure_date]))
        pre.set(TimeLineUser.comment, (pre.comment or '') + comment_text)
        post.set(TimeLineUser.comment, (post.comment or '') + comment_text)

        if pre.get(TimeLineUser.timestamp).date() == post.get(TimeLineUser.timestamp).date():
            Person.session().delete(pre)
        else:
            Person.session().add(pre)
        Person.session().add(post)

    def _fix_common_time_lines(self):
        post = self._common_post_departure_time_line
        if post:
            post.set(PersonStatus.start_date, self._new_departure_date)
            Person.session().add(post)
        for pre in self._common_pre_departure_time_lines:
            if pre.get(PersonStatus.end_date) <= self._new_departure_date:
                logging.debug('PersonStatus with ID %d ends before the new departure date - ignoring' % pre.get(PersonStatus.id))
            elif pre.get(PersonStatus.start_date) >= self._new_departure_date:
                logging.debug('PersonStatus with ID %d starts after the new departure date - deleting' % pre.get(PersonStatus.id))
                Person.session().delete(pre)
            else:
                # the end of 'pre' is overlapping but the beginning isn't - resize
                logging.debug('PersonStatus with ID %d starts before the new departure date and ends after - shrinking!' % pre.get(PersonStatus.id))
                pre.set(PersonStatus.end_date, self._new_departure_date)
                Person.session().add(pre)

    def fix(self):
        try:
            logging.info('Trying to retro-set the leaving date for %d as %s' % (self._cms_id, self._new_departure_date))
            self._fix_people_db_entries()
            self._fix_common_time_lines()
            self._fix_epr_time_lines()
            Person.session().commit()
        except Exception as e:
            Person.session().rollback()
            traceback.print_exc()
            raise e



