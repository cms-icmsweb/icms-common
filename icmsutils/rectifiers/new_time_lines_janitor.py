import logging


class TimeLinesJanitor(object):
    def __init__(self, target_type=None, filter_dict=None):
        self._type = target_type or filter_dict.keys()[0].class_
        self._filter_dict = filter_dict or dict()

    def __ssn(self):
        return self._type.session()

    def __query(self):
        q = self.__ssn().query(self._type)
        for k, v in self._filter_dict.items():
            q = q.filter(k==v)
        return q

    def declutter(self):
        """
        Removes meaningless time-lines of length 0 (when the state changed more than once during a day)
        """
        q = self.__query()
        q = q.filter(self._type.end_date == self._type.start_date)
        q.delete()
        self.__ssn().commit()

    def defragment(self):
        """
        Stitches together disjoint time-lines describing the same state
        :return:
        """
        rows = self.__query().order_by(self._type.id).all()
        dimensions = [c for c in self._type.ia_list() if c not in self._filter_dict.keys() and c.key not in ('id', 'start_date', 'end_date')]

        head = None

        def _are_mergable(r1, r2, dimensions):
            if r1.end_date != r2.start_date:
                # discontinuity discovered!
                logging.debug('%s and %s are not continuous' % (head, tail))
                return False
            for dimension in dimensions:
                if r1.get(dimension) != r2.get(dimension):
                    logging.debug('%s and %s differ' % (head, tail))
                    return False
            return True

        for tail in rows:
            head = head or tail
            if tail.id == head.id:
                continue
            else:
                if _are_mergable(head, tail, dimensions):
                    logging.debug('Merging two time lines: %s and %s' % (head, tail))
                    head.end_date = tail.end_date
                    tail.delete()
                else:
                    # cannot merge anything anymore with the current head
                    head = None
        self.__ssn().commit()

