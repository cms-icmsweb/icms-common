#!/bin/bash

cd "$(dirname "$0")"

# source /opt/rh/rh-postgresql95/enable
source venv-3.9/bin/activate

(PYTHONPATH=./:./venv-3.9/src/icms-orm:./venv-3.9/src/icms-common python scripts/$1)
