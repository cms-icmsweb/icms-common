import sqlalchemy as sa
from sqlalchemy import create_engine

countryRegionMap = { 
	"FR": { 'name': "France", 'countries': [ {"CountryCode" : "FR", "Country" : "France"} ] }, 
	"DE": { 'name': "Germany", 'countries': [ {"CountryCode" : "DE", "Country" : "Germany"} ] }, 
	"IT": { 'name': "Italy", 'countries': [ {"CountryCode" : "IT", "Country" : "Italy"} ] }, 
	"CH": { 'name': "Switzerland", 'countries': [ {"CountryCode" : "CH", "Country" : "Switzerland"} ] }, 
	"UK": { 'name': "United Kingdom", 'countries': [ {"CountryCode" : "GB", "Country" : "United Kingdom"} ] }, 
	"USA": { 'name': "United States of America", 'countries': [ {"CountryCode" : "US", "Country" : "United States of America"} ] }, 
	"CERN": { 'name': "CERN", 'countries': [ ] }, 
	"OCMS": { 'name': "Other CERN Member States", 'countries': [ 
		{ "CountryCode" : "AT", "Country" : "Austria" }, 
        { "CountryCode" : "BE", "Country" : "Belgium" }, 
        { "CountryCode" : "BG", "Country" : "Bulgaria" }, 
        { "CountryCode" : "FI", "Country" : "Finland" }, 
        { "CountryCode" : "GR", "Country" : "Greece" }, 
        { "CountryCode" : "HU", "Country" : "Hungary" }, 
        { "CountryCode" : "NL", "Country" : "Netherlands" },
        { "CountryCode" : "NO", "Country" : "Norway" }, 
        { "CountryCode" : "PL", "Country" : "Poland" }, 
        { "CountryCode" : "PT", "Country" : "Portugal" }, 
        { "CountryCode" : "RS", "Country" : "Serbia" }, 
        { "CountryCode" : "ES", "Country" : "Spain" },
	] },
	"OSA": { 'name': "Other States A", 'countries': [ 
		{ "CountryCode" : "CN", "Country" : "China" }, 
        { "CountryCode" : "IN", "Country" : "India" }, 
        { "CountryCode" : "IR", "Country" : "Iran" }, 
        { "CountryCode" : "KR", "Country" : "Korea, Republic of" }, 
        { "CountryCode" : "MY", "Country" : "Malaysia" }, 
        { "CountryCode" : "NZ", "Country" : "New Zealand" }, 
        { "CountryCode" : "PK", "Country" : "Pakistan" }, 
        { "CountryCode" : "LK", "Country" : "Sri Lanka" }, 
        { "CountryCode" : "SG", "Country" : "Singapore" }, 
        { "CountryCode" : "TW", "Country" : "Taiwan" }, 
        { "CountryCode" : "TH", "Country" : "Thailand" },
        { "CountryCode" : "TN", "Country" : "Tunisia" },
	] },
	"RDMS": { 'name': "Russia and Dubna Member States", 'countries': [ 
		{ "CountryCode" : "AM", "Country" : "Armenia" }, 
        { "CountryCode" : "BY", "Country" : "Belarus" }, 
        { "CountryCode" : "CZ", "Country" : "Czech Republic" }, 
        { "CountryCode" : "GE", "Country" : "Georgia" }, 
        { "CountryCode" : "RU", "Country" : "Russia" }, 
        { "CountryCode" : "UZ", "Country" : "Uzbekistan" },
	] },
	"OSB": { 'name': "Other States B", 'countries': [ 
		{ "CountryCode" : "BH", "Country" : "Bahrain" }, 
        { "CountryCode" : "BR", "Country" : "Brazil" }, 
        { "CountryCode" : "CL", "Country" : "Chile" }, 
        { "CountryCode" : "CO", "Country" : "Colombia" }, 
        { "CountryCode" : "HR", "Country" : "Croatia" }, 
        { "CountryCode" : "CY", "Country" : "Cyprus" }, 
        { "CountryCode" : "EC", "Country" : "Ecuador" }, 
        { "CountryCode" : "EG", "Country" : "Egypt" }, 
        { "CountryCode" : "EE", "Country" : "Estonia" }, 
        { "CountryCode" : "IE", "Country" : "Ireland" }, 
        { "CountryCode" : "JO", "Country" : "Jordan" }, 
        { "CountryCode" : "KW", "Country" : "Kuwait" }, 
        { "CountryCode" : "LV", "Country" : "Latvia" }, 
        { "CountryCode" : "LB", "Country" : "Lebanon" }, 
        { "CountryCode" : "LT", "Country" : "Lithuania" }, 
        { "CountryCode" : "MX", "Country" : "Mexico" }, 
        { "CountryCode" : "ME", "Country" : "Montenegro" }, 
        { "CountryCode" : "NG", "Country" : "Nigeria" }, 
        { "CountryCode" : "OM", "Country" : "Oman" }, 
        { "CountryCode" : "QA", "Country" : "Qatar" }, 
        { "CountryCode" : "SA", "Country" : "Saudi Arabia" }, 
        { "CountryCode" : "TR", "Country" : "Turkey" }, 
        { "CountryCode" : "UA", "Country" : "Ukraine" },
	] }
}

# Connection setup
# engine = create_engine('postgresql+psycopg2://postgres:docker@localhost/icms')
engine = create_engine('postgresql+psycopg2://ap@localhost/icms')
connection = engine.connect()
metadata = sa.MetaData()

# Defining two tables that will be used
country = sa.Table('country', metadata, autoload=True, autoload_with=engine)
region = sa.Table('region', metadata, autoload=True, autoload_with=engine)


# Select queries to check if country and region tables have data in them or not
select_query = sa.select([region])
result = connection.execute(select_query)
regions = result.fetchall()

select_query = sa.select([country])
result = connection.execute(select_query)
countries = result.fetchall()

# print( f'got: regions: {regions}, countries: {countries} ')

for rCode, item in countryRegionMap.items():
    if rCode not in [ x[0] for x in regions ]:
        print( f' adding  region  {rCode} ')
        rName = item['name']
        connection.execute( sa.insert(sa.Table('region', metadata)).values(code=rCode, name=rName) )

    cList = item['countries']
    if not cList or cList == [] : continue
    for country in cList:
        cCode = country['CountryCode']
        cName = country['Country']
        if cCode == '' and cName == '': continue
        if cCode in [ x[0] for x in countries]:
            print( f'updating region for country {cCode} ({cName}) - region {rCode} ')
            connection.execute( sa.update(sa.Table('country', metadata)).where((sa.Table('country', metadata)).c.code == cCode).values(region_code=rCode) )
        else:
            print( f' adding  country {cCode} ({cName}) - region {rCode} ')
            connection.execute( sa.insert(sa.Table('country', metadata)).values(code=cCode, name=cName, region_code=rCode) )

connection.close()
