import traceback

from scripts.prescript_setup import db, ConfigProxy
from icmsutils.prehistory import PersonAndPrehistoryWriter
import logging, sqlalchemy as sa
from icms_orm.common import PersonStatus, PersonStatusValues
from datetime import date
from icms_orm.cmsanalysis import PaperAuthor, PaperAuthorHistory as PAH, PaperAuthor as PA
from icms_orm.cmsanalysis import PaperAuthorHistoryActions as PAHAction, Analysis as A
from icmsutils.businesslogic.cadi.authorlists import AuthorListModel
from icms_orm.cmspeople import Person, PersonHistory


class NewYearDelayedExmembersCleanup(object):
    def __init__(self):
        self._year = date.today().year
        self._eve = date(self._year - 1, 12, 31)
        self._jan1 = date(self._year, 1, 1)
        self._to_be_fixed = set()
        self._to_be_skipped = set()
        self._people = dict()
        self._open_paper_codes = set()

    def __init_open_paper_codes(self):
        # finding AL paper codes of papers that are still open and where ALs have been generated this year
        q_cols = [PAH.code,
                  sa.case(
                      [(sa.func.max(sa.case([(PAH.action == PAHAction.LIST_CLOSE, 1)], else_=0)) == 1, 'closed')],
                      else_='open').label('status')
                  ]

        q = PAH.session().query(*q_cols). \
            filter(sa.and_(
            sa.or_(PAH.action == PAHAction.LIST_GEN, PAH.action == PAHAction.LIST_CLOSE),
            sa.func.str_to_date(PAH.date, '%d/%m/%Y') >= self._jan1)). \
            group_by(PAH.code)

        for _code, _status in q.all():
            if _status == 'open':
                logging.info('Paper {code} AL is {status}'.format(code=_code, status=_status))
                self._open_paper_codes.add(_code)
            else:
                logging.warn('OH NO! The paper {code} AL seems closed already!'.format(code=_code))

    def __init_cms_ids_to_be_fixed(self):
        s = db.session

        # subquery for authors with status CMS as of the last day of previous year
        _sq = s.query(PersonStatus.cms_id).filter(sa.and_(
            PersonStatus.start_date <= self._eve, PersonStatus.end_date > self._eve,
            PersonStatus.is_author == True, PersonStatus.status == PersonStatusValues.CMS
        )).subquery()

        # subquery for those whose status changed on Jan 1 and they were not authors (potentially NO LONGER authors)
        _sq_2 = s.query(PersonStatus.cms_id).filter(sa.and_(
            PersonStatus.start_date == self._jan1, PersonStatus.is_author == False
        )).subquery()

        statuses = s.query(PersonStatus).filter(sa.and_(
            PersonStatus.end_date == None, PersonStatus.is_author == False,
            PersonStatus.status.in_({PersonStatusValues.EXMEMBER, PersonStatusValues.CMSEXTENDED}))).\
            filter(sa.and_(PersonStatus.cms_id.in_(_sq), PersonStatus.cms_id.in_(_sq_2)))

        # finding people who had changed their status into EMERITUS / EXTENDED in January
        for _stat in statuses.all():
            assert isinstance(_stat, PersonStatus)
            logging.debug('CMS id {cms_id} became {status} on {date}'.format(cms_id=_stat.cms_id, status=_stat.status, date=_stat.start_date))
            if _stat.start_date.month <= 2:
                logging.debug('Happened in January, will auto-fix.')
                self._to_be_fixed.add(_stat.cms_id)
            else:
                logging.debug('Looks like the change happened after January, will not fix.')
                self._to_be_skipped.add(_stat.cms_id)

    def __precache_people(self):
        for _cms_id, _p in db.session.query(Person.cmsId, Person).join(PersonHistory, Person.cmsId == PersonHistory.cmsId).filter(sa.or_(
            Person.cmsId.in_(self._to_be_fixed), Person.cmsId == ConfigProxy.get_updater_cms_id_for_scripts()
        )).all():
            self._people[_cms_id] = _p

    def __browse_and_fill_authorlists(self):
        try:
            # add people to author lists given they are not yet there
            for code in self._open_paper_codes:
                alm = AuthorListModel(code=code, actor=ConfigProxy.get_updater_cms_id_for_scripts(), resync_after_commits=False, defer_commits=True)
                for cms_id in self._to_be_fixed:
                    if not alm.has_author(cms_id, only_active=False):
                        logging.info('Paper {code} AL does NOT mention {cms_id} yet!'.format(code=code, cms_id=cms_id))
                        alm.add_author(cms_id)
                    else:
                        logging.info('Paper {code} AL DOES mention {cms_id} already!'.format(code=code, cms_id=cms_id))
            # Set the DateEndSign for every person
            for cms_id in self._to_be_fixed:
                _person = self._people.get(cms_id)
                assert isinstance(_person, Person)
                _new_date_end_sign = date(self._year + 1, 1, 1)
                if _person.dateEndSign is None or _person.dateEndSign < _new_date_end_sign:
                    logging.info('Setting date end sign as January 1 {next_year} for {cms_id}'.format(
                        next_year=_new_date_end_sign, cms_id=cms_id))
                    writer = PersonAndPrehistoryWriter(db_session=db.session,
                                                       object_person=self._people.get(cms_id),
                                                       actor_person=self._people.get(
                                                           ConfigProxy.get_updater_cms_id_for_scripts()))
                    writer.set_new_value(Person.dateEndSign, _new_date_end_sign)
                    writer.apply_changes(do_commit=False)
                else:
                    logging.info('Date end sign for {cms_id} is already {des}'.format(cms_id=cms_id, des=_person.dateEndSign))
            db.session.commit()
        except:
            db.session.rollback()
            logging.error(traceback.format_exc())

    def __call__(self, *args, **kwargs):
        self.__init_cms_ids_to_be_fixed()
        self.__init_open_paper_codes()
        self.__precache_people()
        self.__browse_and_fill_authorlists()


if __name__ == '__main__':
    NewYearDelayedExmembersCleanup()()
