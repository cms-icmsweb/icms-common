#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

from scripts.prescript_setup import db, config
from os import listdir, pardir, remove
from os.path import isfile, join, dirname, realpath, normpath, join, exists
from datetime import datetime, date
import re
import logging
import tarfile


def find_logs_dir():
    _current_dir = dirname(realpath(__file__))
    _is_dir = lambda _dir: 'logs' in listdir(_dir) and 'icmsutils' in listdir(_dir)
    while not(_is_dir(_current_dir)):
        _current_dir = '/'.join(_current_dir.split('/')[:-1])
        if _current_dir == '/':
            raise Exception('Reached root while looking for the logs folder')
    _logs_dir = join(_current_dir, 'logs')
    logging.info('Found the logs dir: ' + _logs_dir)
    return _logs_dir


def compactify_logs(logs_dir):
    _stages = (_stage_merge_days, _stage_tarball_months) = ('merge_days', 'tarball_months')
    for _stage in _stages:
        _files = [f for f in listdir(logs_dir) if isfile(join(logs_dir, f))]
        logging.info('STAGE %s: found %d files!' % (_stage, len(_files)))

        _ptn = r'(\w+)_(\d{4}-\d{2}-\d{2})'
        _name_roots = set()
        _tree = dict()

        for _file in _files:
            m = re.search(_ptn, _file)
            if m:
                _name_roots.add(m.group(1))
                _date = datetime.strptime(m.group(2), '%Y-%m-%d').date()
                # nasty hack: for stage two reducing the date to year and month
                if _stage == _stage_tarball_months:
                    _date = date(_date.year, _date.month, 13)

                _tree[m.group(1)] = _tree.get(m.group(1), dict())
                _tree[m.group(1)][_date] = _tree[m.group(1)].get(_date, [])
                _tree[m.group(1)][_date].append(_file)

        logging.debug(_name_roots)
        logging.debug(_tree)

        if _stage == _stage_merge_days:
            # part 1: for everything having more than one entry per day: concatenate!
            for _stem, _st in _tree.items():
                for _date, _entries in _st.items():
                    if len(_entries) == 1:
                        logging.debug('Only 1 file found for name_root %s on a day %s' % (_stem, _date))
                    else:
                        # concatenate existing files
                        _f_target = '{name_root}_{date}.log'.format(name_root = _stem, date=_date)
                        logging.debug('Target filename is: %s' % _f_target)
                        with open(join(logs_dir, _f_target), 'a') as f_out:
                            # ensuring that the source files for each day are sorted
                            for _f_source in sorted(_entries):
                                if _f_source != _f_target:
                                    with open(join(logs_dir, _f_source)) as f_in:
                                        f_out.write(f_in.read())
                                    # delete the input files
                                    remove(join(logs_dir, _f_source))

        elif _stage == _stage_tarball_months:
            for _stem, _st in _tree.items():
                _tarballed_already = set()
                # we're not interested in a full date anymore, but in month and year only
                for _date, _entries in _st.items():
                    if _date.month != date.today().month and (_date.year, _date.month) not in _tarballed_already:
                        logging.info('Every %s from %d.%d qualifies for tarballing!' % (_stem, _date.year, _date.month))
                        _tarballed_already.add((_date.year, _date.month))
                        _tarball_path = join(logs_dir, '%d.%d_%s.tar' % (_date.year, _date.month, _stem))
                        if not exists(_tarball_path):
                            with tarfile.open(_tarball_path, 'w:gz') as _tarball:
                                _ = [_tarball.add(join(logs_dir, _f)) for _f in _entries]
                            _ = [remove(join(logs_dir, _f)) for _f in _entries]
                        else:
                            logging.warn(
                                'Some stray files found for %s in %d.%d. Cannot append to tarball, leaving as is!'
                                    % (_stem, _date.year, _date.month))


if __name__ == '__main__':
    logs_dir = find_logs_dir()
    compactify_logs(logs_dir)
