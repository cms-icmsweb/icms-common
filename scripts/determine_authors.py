"""
This file will one day replace the old script resetting the author flag each night.
Goals:
- check all the CMS members
- log the changes with some information about their reasons
"""
#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

from scripts.prescript_setup import ConfigProxy
from email_templates.author_flag_emails import EmailTemplateAuthorshipStatusChange
from scripts.fix_ex_year import ExYearFixer
import sys
from icmsutils.businesslogic.authorship import AuthorRightsChecker as Checker


if __name__ == '__main__':
    ExYearFixer().run()
    checker = Checker()
    checker.set_send_emails(
        ConfigProxy.get_authorship_flag_check_send_emails())
    checker.set_updater_cms_ids(ConfigProxy.get_updater_cms_id_for_scripts())
    checker.set_updater_login(ConfigProxy.get_updater_login_for_scripts())

    dry_run: bool = ConfigProxy.get_authorship_flag_check_dry_run_only()
    dry_run = dry_run or any([x in map(str.lower, sys.argv)
                              for x in ['dry', 'dryrun', 'dry-run']])
    checker.run(force_dry_run=dry_run)
