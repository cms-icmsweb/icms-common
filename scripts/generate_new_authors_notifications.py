#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

from icms_orm.toolkit import AuthorshipApplicationCheck as AAC
from icms_orm.cmspeople.institutes import Institute
from icms_orm.cmspeople.people import Person
from scripts.prescript_setup import db, config
from sqlalchemy import func
from icms_orm.common import EmailMessage
from datetime import date
from icmsutils.businesslogic import servicework
from icmsutils import timedate
import logging
import sqlalchemy as sa


def main():
    today = date.today()
    (last_run, ) = db.session.query(func.max(AAC.runNumber)).one()
    logging.debug('The last applications check run number is %d' % last_run)

    successfuls = db.session.query(AAC).\
        filter(sa.and_(AAC.runNumber == last_run, AAC.passed == True, AAC.notificationId == None)).all()
    by_inst = dict()
    for aac in successfuls:
        by_inst[aac.instCode] = by_inst.get(aac.instCode, [])
        by_inst[aac.instCode].append(aac)

    inst_codes = {aac.instCode for aac in successfuls}
    insts = {i.code: i for i in db.session.query(Institute).filter(Institute.code.in_(inst_codes)).all()}
    person_str = lambda x: u'{first} {last}'.format(first=x.firstName, last=x.lastName)

    for inst_code, aacs in by_inst.items():
        inst = insts[inst_code]
        cbds = [x for x in (inst.cbiPerson, inst.cbdPerson, inst.cbd2Person) if x is not None]
        cbi = cbds.pop(0)
        inst_name = inst.name
        epr_per_author_as_of_today = '%.2f' % \
                (timedate.year_fraction_left(today) * servicework.get_annual_author_due(year=today.year),)

        ppl = {p.cmsId: p for p in db.session.query(Person).filter(Person.cmsId.in_({aac.cmsId for aac in aacs})).all()}

        mail_body=u'Dear {cbi},\n\n'\
                u'This message was generated automatically to notify you that some members of {inst_name} '\
                u'are now ready to become CMS Authors.\n'.format(cbi=person_str(cbi), inst_name=inst_name)

        mo_ok = [x for x in aacs if getattr(x, AAC.moStatus.key)]
        mo_bad = [x for x in aacs if not getattr(x, AAC.moStatus.key)]

        if mo_ok:
            mail_body += u'\nThe following members have fulfilled all the requirements and can now become authors: \n'
            for aac in mo_ok:
                mail_body += u'- {person_str} (CMS id {cms_id})\n'.format(person_str=person_str(ppl[aac.cmsId]), cms_id=aac.cmsId)

        if mo_bad:
            mail_body += u'\nThe following members will be able to become authors as soon as their MO requirements are fulfilled:\n'

            for aac in mo_bad:
                mail_body += u'- {person_str} (CMS id {cms_id})\n'.format(person_str=person_str(ppl[aac.cmsId]), cms_id=aac.cmsId)

        mail_body += '\n' \
                    u'Using the following page: https://icms.cern.ch/tools/institute/pending-authors you can approve '\
                    u'anyone listed above to become an author or prevent them from becoming authors now '\
                    u'while keeping the possibility to award them with signing rights at any later moment.' \
                    '\n\n'\
                    u'Please take note that all the people for whom no action is taken within the next 14 days ' \
                    u'will automatically be set as allowed for authorship. ' \
                    u'As a result, anyone meeting the MO requirements '\
                    u'will become an author at that point. ' \
                    '\n\n'
        mail_body += u'If done today, adding new authors will increase the {year} EPR due of {inst_name} by ' \
                     u'approximately {epr_due} per each new author.'.format(year=today.year, inst_name=inst_name,
                                                                  epr_due=epr_per_author_as_of_today)

        mail_body += '\n\n'

        mail_body += u'Sincerely,\n'
        mail_body += u'iCMS mailing system'.format()
        # db.session is passed, the mails will get saved in DB for dispatching
        email, _ = EmailMessage.compose_message(
            sender=config['mailing']['sender'],
            to=cbi.user.get_email(),
            cc=','.join([x.user.get_email() for x in cbds]),
            bcc=config['mailing']['sender'],
            subject='Members of %s ready to become CMS Authors' % inst_code,
            body=mail_body,
            reply_to='cms.secretariat@cern.ch',
            db_session=db.session
        )
        for aac in aacs:
            aac.set(AAC.notificationId, email.get(EmailMessage.id))
            db.session.add(aac)
    db.session.commit()


if __name__ == '__main__':
    main()
