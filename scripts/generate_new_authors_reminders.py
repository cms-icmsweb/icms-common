#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

from scripts.prescript_setup import db, config
from icms_orm.toolkit import AuthorshipApplicationCheck as AAC
from icms_orm.cmspeople import Person, Institute
from icms_orm.common import EmailMessage, EmailLog
from datetime import datetime as dt, timedelta as td
from sqlalchemy import and_
from collections import namedtuple
import logging
"""
The script finds people ready to start signing papers and who have not been decided upon by their CBIs.
"""

def main():
    person_str = lambda x: ' '.join([unicode(y).encode('utf8') for y in (x.firstName, x.lastName)])

    # need to find people for whom an email has been sent but they still are author-wise blocked
    month_ago = dt.today() - td(days=31)
    emailed = db.session.query(AAC).join(EmailMessage, AAC.notificationId == EmailMessage.id).filter(EmailMessage.last_modified > month_ago).all()
    cms_ids = {aac.cmsId for aac in emailed}

    fields_of_interest = [Person.cmsId, Person.lastName, Person.firstName, Person.instCode]

    PxPerson = namedtuple('PxPerson', [x.key for x in fields_of_interest])

    still_blocked = {PxPerson(*x) for x in db.session.query(*fields_of_interest).filter(
        and_(Person.cmsId.in_(cms_ids), Person.isAuthorBlock == True)).all()}

    logging.debug('Found %d people not decided upon!' % (len(still_blocked),))

    by_inst_code = dict()
    for p in still_blocked:
        ic = getattr(p, Person.instCode.key)
        by_inst_code[ic] = by_inst_code.get(ic, [])
        by_inst_code[ic].append(p)

    logging.debug('Distribution by institute: %s' % {x: len(y) for x,y in by_inst_code.items()})

    insts = {getattr(x, Institute.code.key): x for x in
             db.session.query(Institute).filter(Institute.code.in_(by_inst_code.keys())).all()}

    for ic in by_inst_code:
        inst = insts[ic]

        leaders = [x for x in (inst.cbiPerson, inst.cbdPerson, inst.cbd2Person) if x is not None]
        cbi = leaders.pop(0)

        mail_body = 'Dear %s,\n\n' % person_str(cbi)
        mail_body += 'This is an automated message to notify you that ' \
                     'the following members of {ic} are still awaiting your feedback ' \
                     'regarding their authorship status:\n\n'.format(ic=ic)
        for p in by_inst_code[ic]:
            mail_body += '- %s (CMS id %d)\n' % (person_str(p), p.cmsId)
        mail_body += '\n'
        mail_body += 'You can provide your input at the following page: ' \
                     'https://icms.cern.ch/tools/institute/pending-authors\n\n'
        mail_body += 'In case no instructions are received within the next week, ' \
                     'all mentioned applicants will automatically be considered as intended for authorship.\n'
        mail_body += '\n'
        mail_body += 'Sincerely,\niCMS mailing system'

        email, _ = EmailMessage.compose_message(
            sender=config['mailing']['sender'],
            to=cbi.user.get_email(),
            cc=','.join([x.user.get_email() for x in leaders]),
            bcc=config['mailing']['sender'],
            subject='[REMINDER] Members of %s ready to become CMS Authors' % ic,
            body=mail_body,
            reply_to='cms.secretariat@cern.ch',
            db_session=db.session
        )


if __name__ == '__main__':
    main()


