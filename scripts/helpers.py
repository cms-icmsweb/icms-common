#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

from scripts.prescript_setup import db, ConfigProxy
from icms_orm.cmspeople import Person


def get_script_actor_person():
    cms_id = ConfigProxy.get_updater_cms_id_for_scripts()
    person = None
    if cms_id > 0:
        person = db.session.query(Person).filter(Person.cmsId == cms_id).one()
    else:
        username = ConfigProxy.get_updater_login_for_scripts()
        person = Person.from_ia_dict(
            {Person.cmsId: 0, Person.instCode: 'CERN', Person.hrId: 0, Person.loginId: username,
             Person.niceLogin: username})
    return person
