#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

from icmsutils.prehistory.prehistory_model import PrehistoryModel
from scripts.prescript_setup import db, ConfigProxy
from icmsutils.prehistory import ActivityResolver
from icms_orm.cmspeople.people import PersonHistory as Hist, Person, PersonHistory
from icmsutils.prehistory import PrehistoryPatcher


def try_on_all(apply_fixes=False):
    data = db.session.query(Person, Hist).join(Hist, Person.cmsId == Hist.cmsId).filter(Person.cmsId>0).all()
    # 11604
    prehistory_models = dict()

    zero_length, tainted = ([], [])

    smooth = 0
    for person, history in data:
        cms_id = person.get(Person.cmsId)
        if len(prehistory_models) % 1000 == 0:
            print('Processed %d PrehistoryModels' % len(prehistory_models))
        pm = PrehistoryModel(cms_id=cms_id, person=person, history=history)
        prehistory_models[cms_id] = pm
        if pm.is_tainted():
            tainted.append(cms_id)
        elif len(pm.get_prehistory_time_lines()) == 0:
            zero_length.append(cms_id)
        else:
            smooth += 1

    exc_hist = dict()
    total = len(prehistory_models)

    print('%d/%d history entries handled smoothly' % (smooth, total))
    print('%d pre-history entries have length 0: %s' % (len(zero_length), str(zero_length)))
    print('%d pre-history tainted entries: %s' % (len(tainted), str(tainted)))
    print('Encountered exceptions: %s' % str(exc_hist))
    print(' - ' * 20)

    success_count = 0
    attempts = 0

    fixed_cms_ids = []

    for cms_id in tainted:
        print('%d: %s' % (cms_id, prehistory_models.get(cms_id)._exception.message))
        outcome = attempt_recovery(cms_id, apply_fixes=apply_fixes)
        attempts += (outcome is not None and 1 or 0)
        success_count += (outcome and 1 or 0)
        if outcome:
            fixed_cms_ids.append(cms_id)

    print('Seemingly salvaged %d of %d attempted fixes of %d broken history records!' % (success_count, attempts, len(tainted)))
    print('Fixed CMS IDs: %s' % str(fixed_cms_ids))


def attempt_recovery(cms_id, apply_fixes=False):
    """Returns True if resulting patched history was successfully parsed by teh PrehistoryModel's constructor"""
    try:
        patcher = PrehistoryPatcher.new_instance(cms_id=cms_id)
        missing_events = patcher.get_missing_events()
        if missing_events:
            patched = patcher.get_patched_history(actor_cms_id=0)
            print(patched)

            hist, person = PersonHistory.session().query(PersonHistory, Person) \
                .join(Person, Person.cmsId == PersonHistory.cmsId).filter(PersonHistory.cmsId == cms_id).one()
            hist.set(PersonHistory.history, patched)

            pm = PrehistoryModel(cms_id=cms_id, person=person, history=hist)
            if apply_fixes and not pm.is_tainted() and len(pm.get_prehistory_time_lines()) > 0:
                db.session.add(hist)
                db.session.commit()
            else:
                db.session.rollback()
            return not pm.is_tainted()
        else:
            return None
    except Exception as e:
        return False


if __name__ == '__main__':
    try_on_all(apply_fixes=ConfigProxy.get_prehistory_auto_apply_fixes())
