from scripts.sync.brokers import AbstractBroker

class BaseSyncAgent(object):
    """
    Base class for sync agents
    """

    def __init__(self, sync_broker: AbstractBroker):
        """
        :param sync_broker: A broker instance that relays a query for source data.
        """
        self._broker = sync_broker

    def sync(self):
        raise NotImplementedError('Method to be overridden by subclasses')
