from scripts.prescript_setup import db
import logging
import re

import pycountry
import sqlalchemy as sa
from icms_orm.cmspeople import Institute as SrcInst
from icms_orm.cmspeople import MoData as SrcMo
from icms_orm.common import Country as DstCountry, FundingAgency as FA
from icms_orm.common import InstituteStatus as DstInstStatus, Institute as DstInst

from scripts.mockables import today
from .sync_agents_common import BaseSyncAgent


class CountriesSyncAgent(BaseSyncAgent):

    @staticmethod
    def resolve_country(country_name):
        """
        :param country_name:
        :return: alpha_2 official country code
        """
        country_name = country_name or ''
        cc = None
        try:
            country = pycountry.countries.lookup(country_name)
            if country:
                cc = country.alpha_2
        except LookupError as e:
            # try to resolve the exceptions as some countries have ambiguous names in the old iCMS DB
            _exceptions = {'RUSSIA': 'RU', 'KOREA': 'KR', 'IRAN': 'IR'}
            cc = _exceptions.get(country_name.upper())
            # desperate attempt #1: try to find any known country in the country_name (longest match wins)
            if not cc:
                _best_score = 0
                for country in pycountry.countries:
                    if country.name.upper() in country_name.upper():
                        if len(country.name) > _best_score:
                            _best_score = len(country_name)
                            cc = country.alpha_2
            # desperate attempt #2: try to find any of the exceptions in country_name (first one wins)
            if not cc:
                for exception_key, exception_value in _exceptions.items():
                    if exception_key.upper() in country_name.upper():
                        cc = exception_value
                        break
            # desperate attempt #3: try to find any of the official codes in country_name (lowest token index wins)
            if not cc:
                for country in pycountry.countries:
                    _lowest_token_idx = 999
                    m = re.findall(r'(\w+)', country_name)
                    for _token_idx, _token in enumerate(m):
                        if _token in {country.alpha_2, country.alpha_3} and _token_idx < _lowest_token_idx:
                            _lowest_token_idx = _token_idx
                            cc = country.alpha_2
        return cc

    def sync(self):
        synced_codes = {c[0] for c in db.session.query(DstCountry.code).all()}
        if len(pycountry.countries) > len(synced_codes):
            for country in pycountry.countries:
                if country.alpha_2 not in synced_codes:
                    db.session.add(
                        DstCountry.from_ia_dict({DstCountry.code: country.alpha_2, DstCountry.name: country.name}))
            db.session.commit()


class FundingAgenciesSyncAgent(BaseSyncAgent):
    def sync(self):
        pre_synced = {r[0] for r in db.session.query(FA.name).all()}
        # mapping FA names to institutes' countries
        sources = {r[0]: r[1] for r in self._broker.relay(db.session.query(SrcInst.fundingAgency, SrcInst.country).
                                                          filter(SrcInst.fundingAgency != None).distinct())}
        # to catch those FA names that are never mentioned for an institute, but do appear in the MO table

        _q = None
        for _c in [SrcMo.get_columns_for_year(_y)[-1] for _y in range(2011, 2021)]:
            _new_q = db.session.query(_c).distinct()
            _q = _q and _q.union(_new_q) or _new_q
        # Skipping the broker as an underlying call to Query.entities fails for union products self._broker.relay(_q)
        _extra_sources = {_fa: '' for (_fa,) in _q.all() if _fa not in sources}
        sources.update(_extra_sources)

        for fa_name, country_name in sources.items():
            if fa_name not in pre_synced:
                cc = CountriesSyncAgent.resolve_country(country_name in fa_name and country_name or fa_name)
                new = FA.from_ia_dict({FA.name: fa_name, FA.country_code: cc})
                db.session.add(new)
        db.session.commit()


class InstitutesSyncAgent(BaseSyncAgent):
    asset_name_join_dates = 'dict_of_cms_join_date_by_inst_code'

    def create_a_new_status(self, code, status):
        logging.info('Creating a new status "%s" for institute %s' % (status, code))
        return DstInstStatus.from_ia_dict(
            {DstInstStatus.code: code, DstInstStatus.start_date: today(), DstInstStatus.status: status})

    def __translate(self, dst_column, src_value):
        if dst_column == DstInst.country_code:
            return CountriesSyncAgent.resolve_country(src_value)
        return src_value

    def sync(self):
        # merely a local helper method...

        ssn = db.session

        # columns to_be_queried
        tbq = [SrcInst.code, SrcInst.cmsStatus, sa.func.coalesce(SrcInst.name, SrcInst.shortName, SrcInst.code), SrcInst.dateCmsIn, SrcInst.country]
        sources = {r[0]: r for r in self._broker.relay(ssn.query(*tbq))}
        existing = {getattr(r, DstInst.code.key): r for r in ssn.query(DstInst).all()}
        statuses = {getattr(r, DstInstStatus.code.key): r for r in
                    ssn.query(DstInstStatus).filter(DstInstStatus.end_date == None).all()}

        # InstitutesSyncAgent.__store_cms_join_dates({r[0]: r[3] for r in sources.values() if (r[1] and r[1].lower() == 'yes')})

        for code, data in sources.items():
            if code in existing:
                current = existing.get(code)
                for new_col, old_col_idx in {DstInst.name: 2, DstInst.official_joining_date: 3, DstInst.country_code: 4}.items():
                    _value_that_should_be = self.__translate(new_col, data[old_col_idx])
                    if current.get(new_col) != _value_that_should_be:
                        current.set(new_col, _value_that_should_be)
                        ssn.add(current)
            else:

                params_map = {}
                for dst_col, source_col_idx in {
                    DstInst.code: 0, DstInst.name: 2, DstInst.official_joining_date: 3, DstInst.country_code: 4
                }.items():
                    params_map[dst_col] = self.__translate(dst_col, data[source_col_idx])

                new_inst = DstInst.from_ia_dict(params_map)
                ssn.add(new_inst)
                ssn.flush()
                logging.info('Adding a new institute: %s' % code)
            if code in statuses:
                existing_status = statuses.get(code)
                if getattr(existing_status, DstInstStatus.status.key) != data[1]:
                    # terminate current status
                    setattr(existing_status, DstInstStatus.end_date.key, today())
                    # create a new status
                    db.session.add(self.create_a_new_status(data[0], data[1]))
                    db.session.add(existing_status)
            else:
                # create a new status
                db.session.add(self.create_a_new_status(data[0], data[1]))
        ssn.commit()
