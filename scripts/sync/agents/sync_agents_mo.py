from icms_orm.cmspeople import MoData as SrcMo
from icms_orm.common import MO as DstMo, MoStatusValues
from icms_orm.cmspeople import Institute as SrcInst
from icms_orm.common import FundingAgency as FA
from icmsutils.exceptions import IcmsException, FwdSyncException
from icmsutils.prehistory import SupersedersResolver
import logging
from scripts.mockables import today
from scripts.prescript_setup import db
from .sync_agents_common import BaseSyncAgent


class FinalMoSyncAgent(BaseSyncAgent):

    """
    How will we convert the old data into the new structure for any given year:
    MO  FRE PHD
    0   0   0   -   NOTHING [means no MO, nothing (make sure there's nothing)]
    0   0   1   -   APPROVED, SWAPPED_OUT [means someone made it onto the list, but got taken off subsequently]
    0   1   0   -   NOTHING?? [shouldn't happen but we might see that for the future (before someone flips the master MO)]
    1   0   0   -   PROPOSED [means the wish was there but hasn't been granted]
    0   1   1   -   APPROVED, SWAPPED_OUT: PHDMO > FREEMO, so the same as having only PHD
    1   0   1   -   APPROVED [the cleanest case possible perhaps]
    1   1   0   -   APPROVED_FREE [also a clean case]
    1   1   1   -   APPROVED [quite clean too, but PHDMO is stronger than FREEMO]

    For the next year, we only expect the clean cases: MO with PHDMO, perhaps a few MO with FREEMO.
    FREEMO without MO cannot be expressed in the new system (granted-but-unwanted?)
    """

    def __init__(self, *args, **kwargs):
        # this sync is supposed to leave a clean state once the PHD list for 2019 is closed.
        super(FinalMoSyncAgent, self).__init__(*args, **kwargs)
        self._sources = {c.get(SrcMo.cmsId): c for c in self._broker.relay(db.session.query(SrcMo))}
        self._fa_map = None
        if kwargs.get('force') is True:
            self.delete_pre_synced_info()

    def delete_pre_synced_info(self):
        """
        Removes from the target DB all MO information for given CMS ID (covers the 0 0 0 case, when invoked for every source row)
        """
        db.session.query(DstMo).filter(DstMo.cms_id).delete()

    def sync_as_approved(self, cms_id, year, mo, free_mo, phd_mo, fa, phd_ic):
        """
        For those having the PHDMO (and possibly FREE MO) and master MO for given year (1 1 1 and 1 0 1 cases)
        """
        db.session.add(DstMo.from_ia_dict(
            {DstMo.year: year, DstMo.cms_id: cms_id, DstMo.status: MoStatusValues.APPROVED,
             DstMo.fa_id: self._fa_code_to_id(fa), DstMo.inst_code: phd_ic or None}))

    def sync_as_swapped_out(self, cms_id, year, mo, free_mo, phd_mo, fa, phd_ic):
        """
        For those having the PHDMO (and possibly FREEMO) but not the master MO for given year (0 1 1 and 0 0 1 cases)
        """
        logging.debug('Apparently %d got swapped out in year %d' % (cms_id, year))
        _tmp = {DstMo.year: year, DstMo.cms_id: cms_id, DstMo.status: MoStatusValues.APPROVED, DstMo.inst_code: phd_ic, DstMo.fa_id: self._fa_code_to_id(fa)}
        db.session.add(DstMo.from_ia_dict(_tmp))
        _tmp.update({DstMo.status: MoStatusValues.SWAPPED_OUT})
        _swapped = DstMo.from_ia_dict(_tmp)
        db.session.add(_swapped)

    def sync_as_proposed(self, cms_id, year, mo, free_mo, phd_mo, fa, phd_ic):
        """
        For those having nothing but the master MO enabled for given year (1 0 0 case)
        """
        _new = DstMo.from_ia_dict({DstMo.year: year, DstMo.cms_id: cms_id, DstMo.status: MoStatusValues.PROPOSED,
                                   DstMo.inst_code: phd_ic or None, DstMo.fa_id: self._fa_code_to_id(fa)})
        db.session.add(_new)

    def sync_as_proposed_free(self, cms_id, year, mo, free_mo, phd_mo, fa, phd_ic):
        """
        For those having nothing but FREEMO for the year given (0 1 0 case)
         - forwarding the call to sync identically as for those with approved FREE MO.
        """
        self.sync_as_approved_free(cms_id, year, mo, free_mo, phd_mo, fa, phd_ic)

    def sync_as_approved_free(self, cms_id, year, mo, free_mo, phd_mo, fa, phd_ic):
        """
        For those having master MO and FREEMO enabled for the year given (1 1 0 case)
        """
        _new = DstMo.from_ia_dict({DstMo.year: year, DstMo.cms_id: cms_id, DstMo.status: MoStatusValues.APPROVED_FREE_GENERIC})
        db.session.add(_new)

    def check_and_set(self, cms_id, year, mo, free_mo, phd_mo, phd_ic, phd_fa):
        """
        Checks against the pre-synced data and orders necessary updates / inserts / deletes to be performed
        :param cms_id:
        :param year:
        :param mo:
        :param free_mo:
        :param phd_mo:
        :param phd_ic:
        :param phd_fa:
        :return:
        """

    def pick_sync_method(self, mo, free_mo, phd_mo, cms_id, year):
        """
        Returns a sync method to delegate the writing to - that way we can separately test the decision making
        and the subsequent changes written to the database
        :param src_mo_row: MO object
        :param year: a number
        :return:
        """
        if mo:
            # All the cases with master MO (1 ? ?)
            if phd_mo:
                # Cases 1 1 1 and 1 0 1 (APPROVED)
                return self.sync_as_approved
            elif free_mo:
                # Case 1 1 0 (APPROVED_FREE)
                return self.sync_as_approved_free
            else:
                # Case 1 0 0 (PROPOSED)
                return self.sync_as_proposed
        else:
            # All the cases without master MO (0 ? ?)
            if phd_mo:
                # Cases 0 1 1 and 0 0 1 (APPROVED, SWAPPED_OUT)
                return self.sync_as_swapped_out
            elif free_mo:
                # Case 0 1 0 (PROPOSED_FREE)
                return self.sync_as_proposed_free
            else:
                # Case 0 0 0 - nothing to do as long as any pre-synced rows have been deleted by now
                logging.info('No MO at all for CMS ID %d in %d' % (cms_id, year))
        return None

    def sync(self):
        if db.session.query(DstMo.id).count() > 0:
            raise FwdSyncException('Forward MO sync has already been performed. Aborting.')

        _is_yes = lambda x: x and x.lower() == 'yes'
        for _year in range(2011, today().year + 1):
            _cols = [mo_col, phd_mo_col, free_mo_col, phd_ic_col, phd_fa_col] = SrcMo.get_columns_for_year(_year)
            for _cms_id, _src_mo_row in self._sources.items():
                _vals = [_src_mo_row.get(_c) for _c in _cols]
                _mo, _phd_mo, _free_mo = (_is_yes(_v) for _v in _vals[0:3])

                method = self.pick_sync_method(mo=_mo, free_mo=_free_mo, phd_mo=_phd_mo, cms_id=_cms_id, year=_year)
                if method:
                    method(**dict(cms_id=_cms_id, year=_year, mo=_mo, free_mo=_free_mo, phd_mo=_phd_mo, fa=_vals[4],
                                  phd_ic=SupersedersResolver.get_present_value(SrcInst.code, _vals[3])))
        db.session.commit()

    def _fa_code_to_id(self, fa_name):
        if not self._fa_map:
            self._fa_map = {_fa.get(FA.name): _fa for _fa in db.session.query(FA).all()}
        _fa = self._fa_map.get(fa_name, None)
        return _fa and _fa.get(FA.id) or None


class MoSyncAgent(BaseSyncAgent):
    def sync(self):
        sources = self._broker.relay(db.session.query(SrcMo))
        synced = db.session.query(DstMo).filter(DstMo.end_date == None).all()
        funding_agencies = {fa.name: fa for fa in db.session.query(FA).all()}

        # reshape the data for easy access
        synced = {(mo.year, mo.cms_id): mo for mo in synced}
        sources = {src.cmsId: src for src in sources}
        logging.info('Found %d MO records to cross chceck with pre-synchronised data' % len(sources))
        # a very local helper
        def get_differing_mo_attributes(a, b):
            assert a.cms_id == b.cms_id, 'Differing CMS IDs for what was to be same person\'s records!'
            result = []
            for attr in [DstMo.inst_code, DstMo.is_free, DstMo.is_listed, DstMo.approval_date, DstMo.year]:
                if getattr(a, attr.key) != getattr(b, attr.key):
                    result.append(attr)
            return result

        for cms_id, src in sources.items():
            for year in range(2015, today().year + 2):
                # create a possible entry for the cms_id and year in question...
                fa = funding_agencies.get(getattr(src, SrcMo.phdFa2017.key.replace('2017', str(year))), None)
                possible_entry = DstMo.from_ia_dict({
                    DstMo.inst_code: SupersedersResolver.get_present_value(
                        SrcInst.code, getattr(src, SrcMo.phdInstCode2017.key.replace('2017', str(year))) or None),
                    DstMo.cms_id: cms_id,
                    DstMo.approval_date: None,
                    DstMo.is_free: getattr(src, SrcMo.freeMo2017.key.replace('2017', str(year))) == 'YES',
                    DstMo.is_listed: getattr(src, SrcMo.phdMo2017.key.replace('2017', str(year))) == 'YES',
                    DstMo.year: year,
                    DstMo.start_date: today(),
                    DstMo.fa_id: fa and fa.id or None
                })
                # and now check if we already have one like that. if so, skip, if they differ, merge
                key = (year, cms_id)
                existing_entry = synced.get(key, None)

                # None means there was no existing entry, [] diff means they were identical, [...] means there was some difference
                differing_cols = get_differing_mo_attributes(existing_entry, possible_entry) if existing_entry else []

                if existing_entry is None or len(differing_cols) > 0:
                    # a new entry needs to be created given that MO is granted
                    if possible_entry.is_listed or possible_entry.is_free or possible_entry.inst_code:
                        db.session.add(possible_entry)
                if existing_entry is not None and len(differing_cols) > 0:
                    # the existing entry will be superseded
                    existing_entry.end_date = today()
                    db.session.add(existing_entry)
            db.session.flush()
        # wrap-up, commit
        db.session.commit()
