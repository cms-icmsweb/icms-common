import logging

import sqlalchemy as sa
from icms_orm import common
from icms_orm.cmspeople import Institute as SrcInst
from icms_orm.cmspeople import Person as SrcPerson, User as SrcUser, MemberActivity as SrcActivity
from icms_orm.cmspeople import PersonHistory as SrcHistory
from icms_orm.common import InstituteLeader, Affiliation
from icms_orm.common import Person as DstPerson, PersonStatus as DstPersonStatus
from icms_orm.common import PrehistoryTimeline
from sqlalchemy import case

from icmsutils.prehistory import SupersedersResolver
from icmsutils.prehistory.prehistory_model import PrehistoryModel
from scripts.mockables import today
from scripts.prescript_setup import db
from scripts.sync.agents import CountriesSyncAgent
from scripts.sync.agents.sync_agents_common import BaseSyncAgent


class AffiliationsSyncAgent(BaseSyncAgent):
    def sync(self):
        synced = {r.cms_id: r for r in db.session.query(Affiliation).filter(Affiliation.end_date == None)}
        source = {r[0]: r[1] for r in self._broker.relay(db.session.query(SrcPerson.cmsId, SrcPerson.instCode)) if r[0] is not None and r[0] > 0}
        logging.info('Found %d affiliation records to cross chceck with pre-synchronised data' % len(source))
        for cms_id in source:
            old_record = synced.get(cms_id, None)
            if old_record is None or old_record.inst_code != source[cms_id]:
                new_record = Affiliation.from_ia_dict({
                    Affiliation.cms_id: cms_id,
                    Affiliation.is_primary: True,
                    Affiliation.inst_code: SupersedersResolver.get_present_value(SrcInst.code, source[cms_id]),
                    Affiliation.start_date: today()
                })
                db.session.add(new_record)
                if cms_id in synced:
                    synced[cms_id].date_end = today()
                    db.session.add(synced[cms_id])
            if old_record and old_record.inst_code != source[cms_id]:
                # closing the previous affiliation entry
                setattr(old_record, Affiliation.end_date.key, today())
                db.session.add(old_record)
        db.session.commit()
