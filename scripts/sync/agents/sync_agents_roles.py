from scripts.prescript_setup import db
from icms_orm.cmspeople import Institute as SrcInst, PeopleFlagsAssociation
from icms_orm.common import InstituteLeader, LegacyFlag
from icmsutils.prehistory import SupersedersResolver
from scripts.mockables import today
from scripts.sync.brokers import ChangeLogAwareBroker
from scripts.sync.agents.sync_agents_common import BaseSyncAgent
import logging


class TeamLeadersSyncAgent(BaseSyncAgent):
    def sync(self):
        # composite key necessary - one might lead multiple institutes
        synced_leaders_data = {(r.inst_code, r.cms_id): r for r in db.session.query(InstituteLeader).
            filter(InstituteLeader.end_date == None).filter(InstituteLeader.is_primary == True).all()}
        synced_leaders = {r.inst_code: r.cms_id for r in synced_leaders_data.values()}
        source_leaders = {SupersedersResolver.get_present_value(SrcInst.code, r[0]): r[1] for r in
                          self._broker.relay(db.session.query(SrcInst.code, SrcInst.cbiCmsId))}
        # now the actual sync
        for inst_code in sorted(set( list(synced_leaders.keys()) + list(source_leaders.keys()) )):
            cms_id_synced = synced_leaders.get(inst_code, None)
            cms_id_source = source_leaders.get(inst_code, None)
            if cms_id_synced:
                if cms_id_source is not None and cms_id_source != cms_id_synced:
                    # just close the old entry
                    current_record = synced_leaders_data.get((inst_code, cms_id_synced))
                    setattr(current_record, InstituteLeader.end_date.key, today())
                    db.session.add(current_record)
            if cms_id_source:
                if cms_id_synced is None or cms_id_synced != cms_id_source:
                    # just open the new entry
                    db.session.add(InstituteLeader.from_ia_dict({
                        InstituteLeader.inst_code: inst_code,
                        InstituteLeader.cms_id: cms_id_source,
                        InstituteLeader.is_primary: True,
                        InstituteLeader.start_date: today()
                    }))
        db.session.commit()


class DeputyTeamLeadersSyncAgent(BaseSyncAgent):
    def sync(self):
        synced_deputies_rows = {(r.inst_code, r.cms_id): r for r in db.session.query(InstituteLeader).
            filter(InstituteLeader.end_date == None). \
            filter(InstituteLeader.is_primary == False).all()}
        synced_deputies = {}
        for r in synced_deputies_rows.values():
            synced_deputies[r.inst_code] = synced_deputies.get(r.inst_code, set())
            synced_deputies[r.inst_code].add(r.cms_id)

        source_deputies = {}
        for r in self._broker.relay(db.session.query(SrcInst.code, SrcInst.cbdCmsId, SrcInst.cbd2CmsId)):
            source_deputies[SupersedersResolver.get_present_value(SrcInst.code, r[0])] = {x for x in r[1:] if x}

        for inst_code in sorted( set( list(synced_deputies.keys()) + list(source_deputies.keys()) )):
            source_set = source_deputies.get(inst_code, set())
            synced_set = synced_deputies.get(inst_code, set())

            # removing only when data for that institute comes with the update-++
            if inst_code in source_deputies.keys():
                to_remove = synced_set - source_set
                for cms_id in to_remove:
                    record = synced_deputies_rows[(inst_code, cms_id)]
                    setattr(record, InstituteLeader.end_date.key, today())
                    db.session.add(record)

            to_add = source_set - synced_set
            for cms_id in to_add:
                db.session.add(InstituteLeader.from_ia_dict({
                    InstituteLeader.cms_id: cms_id,
                    InstituteLeader.start_date: today(),
                    InstituteLeader.inst_code: inst_code,
                    InstituteLeader.is_primary: False
                }))
        db.session.commit()


class LegacyFlagsSyncAgent(BaseSyncAgent):
    def sync(self):
        _ssn = db.session

        _remote = self._broker.relay(_ssn.query(PeopleFlagsAssociation))

        # dealing with marking flags as inactive
        if self._broker.is_incremental():
            # either we grab the remote_ids of removed objects...
            _gone_ids = self._broker.get_ref_ids_of_deleted_records(entity_class=PeopleFlagsAssociation)
            logging.debug('Received {len} ids of deleted remote objects'.format(len=len(_gone_ids)))
            _q_to_go =_ssn.query(LegacyFlag).filter(LegacyFlag.remote_id.in_(set(int(_r[0]) for _r in _gone_ids)))
        else:
            # or we grab all locally known remote ids that are not in the latest batch
            _q_to_go = _ssn.query(LegacyFlag).\
                filter(LegacyFlag.remote_id.notin_(set(_r.get(PeopleFlagsAssociation.id) for _r in _remote)))
        # then we use the query to retrieve them and mark as expired
        _to_go = _q_to_go.all()
        for _f in _to_go:
            if _f.remote_id < 0: continue
            _f.set(LegacyFlag.end_date, today())
            _ssn.add(_f)
        _ssn.commit()

        # now checking for active flags: will compare locally known flags to those coming from the last batch
        _local = dict()
        _local_by_remote_id = dict()
        _q_local = _ssn.query(LegacyFlag).filter(LegacyFlag.end_date == None)
        # if we have the smarter broker, we can constrain the set of local objects that will be of interest now
        if self._broker.is_incremental():
            # again, if we use the smarter broker, we can just get the ids and retrieve less from local DB
            _changed_ids = set(_r.get(PeopleFlagsAssociation.id) for _r in _remote)
            _q_local = _q_local.filter(LegacyFlag.remote_id.in_(_changed_ids))
        for _f in _q_local.all():
            # now we'll catalogue them
            assert isinstance(_f, LegacyFlag)
            _local[_f.get(LegacyFlag.cms_id)] = _local.get(_f.get(LegacyFlag.cms_id), dict())
            _local[_f.cms_id][_f.flag_code] = _f
            _local_by_remote_id[_f.remote_id] = _f

        # finally we look through the newest remote batch to compare against the catalogue and create entries if needed
        for _pfa in _remote:
            assert isinstance(_pfa, PeopleFlagsAssociation)
            if _local.get(_pfa.cmsId, dict()).get(_pfa.flagId) is None:
                logging.debug('Did NOT find a local counterpart for remote flag {flag} for {cms_id} (remote id: {remote_id})'.format(
                    flag=_pfa.flagId, cms_id=_pfa.cmsId, remote_id=_pfa.id
                ))
                _ssn.add(LegacyFlag.from_ia_dict({
                    LegacyFlag.remote_id: _pfa.id,
                    LegacyFlag.flag_code: _pfa.flagId,
                    LegacyFlag.cms_id: _pfa.cmsId,
                    LegacyFlag.start_date: today()
                }))
        _ssn.commit()
