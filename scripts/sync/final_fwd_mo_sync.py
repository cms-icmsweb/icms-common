from scripts.prescript_setup import db, config
from scripts.sync import ForwardSyncManager, CountriesSyncAgent, FundingAgenciesSyncAgent
from scripts.sync import InstitutesSyncAgent, PeopleSyncAgent, FinalMoSyncAgent
from scripts.sync.brokers import DefaultBroker
from icms_orm.common import MO
import sys


if __name__ == '__main__':

    if 'force' in map(str.lower, sys.argv):
        db.session.query(MO).delete()
        db.session.commit()

    agents = [CountriesSyncAgent, FundingAgenciesSyncAgent, InstitutesSyncAgent, PeopleSyncAgent, FinalMoSyncAgent]
    ForwardSyncManager.launch_sync(agent_classes_override=agents, broker_class_override=DefaultBroker)
