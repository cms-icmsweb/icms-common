"""
Some tests break here? Imports' order might, regrettably, be relevant.
"""
#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

from datetime import datetime as dt
import time
import sys
import icms_orm
from sqlalchemy import and_, asc, or_, orm, func
from sqlalchemy_continuum import make_versioned, versioning_manager, plugins
versioning_manager.options['base_classes'] = (icms_orm.IcmsDeclarativeBasesFactory.get_for_bind_key(icms_orm.epr_bind_key()), )
make_versioned(user_cls=None, plugins=[plugins.PropertyModTrackerPlugin()])
from icms_orm.epr import ContinuumTransaction
versioning_manager.transaction_cls = ContinuumTransaction
orm.configure_mappers()
from scripts.prescript_setup import db, config, ConfigProxy
from icms_orm.epr import TimeLineInst, TimeLineUser, EprUser, EprInstitute as EprInst, Manager, Permission
from icms_orm.cmspeople import Person, Institute, PersonHistory
from icmsutils.timelinesutils import create_time_line, institute_mapper, is_time_line_split_needed, split_timeline, \
    category_mapper, logger, is_authorship_allowing_activity_id, session_wrapper
from datetime import date, datetime


def sync_dbs(icms_insts, icms_ppl_and_histories, day_now=None, dry_run=False, only_insert=False):
    now = isinstance(day_now, datetime) and day_now or isinstance(day_now, date) \
                and datetime(day_now.year, day_now.month, day_now.day) or datetime.now()

    # epr_insts = {inst.code: inst for inst in db.session.query(EprInst).all()}
    epr_insts = {inst.code: inst for inst in db.session.query(EprInst).all()}
    epr_inst_cern_ids = {inst.cernId for inst in epr_insts.values()}

    # dictionary of inst_code to list of tuples(0 - Manager, 1 - User)
    epr_cbis = {}
    for mgr_user in db.session.query(Manager, EprUser).filter(and_(Manager.itemType == 'institute', Manager.year == now.year))\
            .filter(or_(Manager.status.like('active'), Manager.status.like('deleted'))).join(EprUser, Manager.userId == EprUser.id):
        if mgr_user[0].itemCode not in epr_cbis.keys():
            epr_cbis[mgr_user[0].itemCode] = []
        epr_cbis[mgr_user[0].itemCode].append(mgr_user)

    for icms_inst in icms_insts:
        if icms_inst.code in epr_insts.keys():
            epr_inst = epr_insts[icms_inst.code]
            if epr_inst.cmsStatus != icms_inst.cmsStatus and now > epr_inst.timestamp:
                epr_inst.cmsStatus = icms_inst.cmsStatus
                epr_inst.timestamp = now
                logger.info('UPDATE: %s', str(epr_inst))
                db.session.add(epr_inst)
        elif icms_inst.cernInstId in epr_inst_cern_ids:
            if is_inst_epr_addable(icms_inst):
                logger.warning('PROBLEM: CERN inst id %s already in EPR DB but we wanted to register %s with that id',
                            str(icms_inst.cernInstId), icms_inst.code)
        elif is_inst_epr_addable(icms_inst):
            epr_inst = EprInst(cernId=icms_inst.cernInstId, name=icms_inst.name,
                               country=icms_inst.country, code=icms_inst.code,
                               cmsStatus=icms_inst.cmsStatus, year=day_now.year, comment='')
            epr_inst.timestamp = now
            logger.info('CREATE insititute: %s', epr_inst)
            db.session.add(epr_inst)
            db.session.flush()
        else:
            logger.debug('Not adding institute %s [status %s]' % (icms_inst.code, icms_inst.cmsStatus))
        # Check for managers now:
        icms_cbi_ids = [x for x in (icms_inst.cbiCmsId, icms_inst.cbdCmsId, icms_inst.cbd2CmsId) if x is not None]
        if icms_inst.code in epr_cbis.keys():
            for mgr_user in epr_cbis[icms_inst.code]:
                if mgr_user[0].timestamp < now:
                    if mgr_user[1].cmsId not in icms_cbi_ids and mgr_user[0].status != 'deleted':
                        mgr_user[0].status = 'deleted'
                        mgr_user[0].timestamp = now
                        logger.info('Marking user %d as inactive CBI for %s', mgr_user[1].cmsId, icms_inst.code)
                        logger.debug('icms_cbi_ids: %s', str(icms_cbi_ids))
                        db.session.add(mgr_user[0])
                    elif mgr_user[1].cmsId in icms_cbi_ids and mgr_user[0].status != 'active':
                        mgr_user[0].status = 'active'
                        logger.info('Reactivating user %d as CBI for %s', mgr_user[1].cmsId, icms_inst.code)
                        mgr_user[0].timestamp = now
                        db.session.add(mgr_user[0])

        # for every CBI seen on ICMS side check if they are seen on EPR side
        for icms_cbi_id in icms_cbi_ids:
            if icms_inst.code not in epr_cbis.keys() or icms_cbi_id not in \
                    (mgr_user[1].cmsId for mgr_user in epr_cbis[icms_inst.code]):
                epr_user = db.session.query(EprUser).filter(EprUser.cmsId == icms_cbi_id).one_or_none()
                if epr_user is None:
                    if is_inst_epr_addable(icms_inst):
                        logger.warning('Was about to set CMS id %d as CBI for %s but the user was not found in EPR DB',
                                    icms_cbi_id, icms_inst.code)
                elif icms_inst.code not in epr_insts.keys():
                    if is_inst_epr_addable(icms_inst):
                        logger.warning('Was about to set CMS id %d as CBI for %s but the institute was not found in EPR DB',
                                    icms_cbi_id, icms_inst.code)
                else:
                    manager = Manager(itemType='institute', itemCode=icms_inst.code, userId=epr_user.id, year=now.year,
                                      timestamp=now, role=Permission.MANAGEINST)
                    logger.info('Creating a new CBI entry for user %s and institute %s', str(icms_cbi_id), icms_inst.code)
                    db.session.add(manager)

    epr_users = {}
    used_hrids = set()
    used_logins = set()

    for p in db.session.query(EprUser).all():
        epr_users[p.cmsId] = p
        used_hrids.add(p.hrId)
        used_logins.add(p.username)

    for icms_p, icms_h in icms_ppl_and_histories:
        if icms_p.cmsId not in epr_users.keys():
            if icms_p.hrId in used_hrids:
                if is_person_epr_addable(icms_p):
                    logger.warning('PROBLEM: HR id %d already in EPR DB but we would like to attribute it to CMSid %d now!',
                               icms_p.hrId, icms_p.cmsId)
            elif is_person_epr_addable(icms_p):
                epr_user = EprUser(username=icms_p.loginId,
                               name=', '.join([icms_p.lastName, icms_p.firstName]),
                               hrId=icms_p.hrId, cmsId=icms_p.cmsId, instId=None,
                               authorReq=icms_p.isAuthor, status=icms_p.status, isSusp=icms_p.isAuthorSuspended,
                               categoryName=None,
                               comment='', year=day_now.year, mainInst=institute_mapper(icms_p.instCode))
                epr_user.category = category_mapper(icms_p.activity and icms_p.activity.name or None)
                if epr_user.category is None:
                    logger.warning('User category None: %s' % epr_user)
                if epr_user.username in used_logins:
                    logger.error('A new EPR user (CMSid %d) with login "%s" cannot be added as this login is already known to EPR!'
                                 % (epr_user.cmsId, epr_user.username))
                else:
                    epr_user.timestamp = now
                    logger.info('CREATED EprUser with CMS ID=%d, HR ID=%d, username=\'%s\''
                            % tuple([epr_user.get(attr) for attr in [EprUser.cmsId, EprUser.hrId, EprUser.username]]))
                    db.session.add(epr_user)
        else:
            epr_user = epr_users[icms_p.cmsId if icms_p.cmsId in epr_users.keys() else used_hrids[icms_p.hrId]]
            assert isinstance(epr_user, EprUser) and isinstance(icms_p, Person)
            if epr_user.timestamp < now:
                changeset = {}
                _name_as_it_should_be = u'{last}, {first}'.format(last=icms_p.lastName, first=icms_p.firstName)
                if epr_user.name != _name_as_it_should_be:
                    changeset[EprUser.name.key] = (epr_user.name, _name_as_it_should_be)
                    epr_user.name = _name_as_it_should_be
                if epr_user.authorReq != icms_p.isAuthor:
                    changeset[EprUser.authorReq.key] = (epr_user.authorReq, icms_p.isAuthor)
                    epr_user.authorReq = icms_p.isAuthor
                if epr_user.isSuspended != icms_p.isAuthorSuspended:
                    changeset[EprUser.isSuspended.key] = (epr_user.isSuspended, icms_p.isAuthorSuspended)
                    epr_user.isSuspended = icms_p.isAuthorSuspended
                if str(epr_user.status) != str(icms_p.status):
                    # oracle will store an empty string as null anyway
                    if str(icms_p.status) or epr_user.status is not None:
                        changeset[EprUser.status.key] = (epr_user.status, icms_p.status)
                        epr_user.status = icms_p.status
                if epr_user.mainInst is not None and icms_p.instCode is not None:
                    if epr_user.institute.code != icms_p.instCode:
                        changeset[EprUser.mainInst.key] = (epr_user.mainInst, institute_mapper(icms_p.instCode))
                        epr_user.mainInst = institute_mapper(icms_p.instCode)
                elif epr_user.mainInst is not None:
                    changeset[EprUser.mainInst.key] = (epr_user.mainInst, None)
                    epr_user.mainInst = None
                elif icms_p.instCode is not None:
                    changeset[EprUser.mainInst.key] = (epr_user.mainInst, institute_mapper(icms_p.instCode))
                    epr_user.mainInst = institute_mapper(icms_p.instCode)

                if epr_user.category is not None and icms_p.activity is not None:
                    if epr_user.category_rel.name != icms_p.activity.name:
                        changeset[EprUser.category.key] = (epr_user.category, category_mapper(icms_p.activity.name))
                        epr_user.category = category_mapper(icms_p.activity.name)
                elif epr_user.category is not None:
                    changeset[EprUser.category.key] = (epr_user.category, None)
                    epr_user.category = None
                elif icms_p.activity is not None:
                    changeset[EprUser.category.key] = (epr_user.category, category_mapper(icms_p.activity.name))
                    epr_user.category = category_mapper(icms_p.activity.name)

                # UPDATE: always prioritize a MEANINGFUL login from the OLD ICMS that differs from what we see in EPR
                if (icms_p.loginId or '').strip() and (icms_p.loginId.strip() != epr_user.username):
                    if icms_p.loginId in used_logins:
                        logger.error('User with CMSid %d was about to be assigned a new login %s that already exists '
                                     'in EPR!' % (icms_p.cmsId, icms_p.loginId))
                    else:
                        changeset[EprUser.username.key] = (epr_user.username, icms_p.loginId)
                        logger.debug('Updating login for #%d from "%s" to "%s"' % (epr_user.cmsId, epr_user.username, icms_p.loginId))
                        epr_user.username = icms_p.loginId

                if epr_user.creationTime is None:
                    changeset[EprUser.creationTime.key] = (epr_user.creationTime, icms_p.dateCreation)
                    epr_user.creationTime = icms_p.dateCreation

                if len(changeset.keys()) > 0:
                    if epr_user.mainInst is None:
                        logger.warning('PROBLEM: wanted to commit a user "%s" without a main institute', epr_user.username)
                    else:
                        epr_user.timestamp = now
                        logger.info('Updating user information for CMSid %d, changeset: %s', epr_user.cmsId, str(changeset))
                        db.session.add(epr_user)
    db.session.flush()


def update_time_lines(icms_insts, icms_ppl_and_histories, day_now=date.today(), dry_run=False, only_insert=False):
    now = day_now if isinstance(day_now, datetime) else datetime(day_now.year, day_now.month, day_now.day)

    # We only update for ppl/insts already in epr db
    epr_cms_ids = {user.cmsId for user in db.session.query(EprUser.cmsId).all()}
    epr_inst_codes = {inst.code for inst in db.session.query(EprInst.code).all()}

    # (the last one overwrites any earlier in the map)
    saved_user_lines = {time_line.cmsId: time_line for time_line in
                        db.session.query(TimeLineUser).order_by(asc(TimeLineUser.timestamp)).filter(
                            TimeLineUser.year == now.year).all()}

    sq = db.session.query(func.max(TimeLineInst.id).label('max_id')).group_by(TimeLineInst.code).subquery()
    saved_inst_lines = {(line.code, line.cmsStatus, line.year) for line in db.session.query(TimeLineInst).join(
        sq, sq.c.max_id == TimeLineInst.id
    ).filter(
        TimeLineInst.year == now.year
    ).all()}

    # For every institute from iCMS db create a time-line entry if needed
    for icms_inst in icms_insts:
        if icms_inst.code in epr_inst_codes and (icms_inst.code, icms_inst.cmsStatus, day_now.year) not in saved_inst_lines:
            db.session.add(create_time_line(icms_inst, now=day_now))

    for person, history in icms_ppl_and_histories:
        if person.cmsId in epr_cms_ids:
            new_timeline = create_time_line(person, history, now=now)
            if new_timeline.instCode not in epr_inst_codes:
                logger.warning('PROBLEM: created a time line for user CMSid "%s" referencing an inst code "%s" unknown '
                               'to EPR. Person is: %s', str(new_timeline.cmsId), new_timeline.instCode, person)
            else:
                if new_timeline.cmsId not in saved_user_lines.keys():
                    db.session.add(new_timeline)
                elif new_timeline.timestamp > saved_user_lines[person.cmsId].timestamp \
                        and is_time_line_split_needed(older=saved_user_lines[person.cmsId], newer=new_timeline):
                    time_lines = split_timeline(older=saved_user_lines[person.cmsId], newer=new_timeline)
                    db.session.add_all(time_lines)


def _init_caches():
    category_mapper.wipe()
    institute_mapper.wipe()
    _ = is_authorship_allowing_activity_id(None, db_session=db.session)
    _ = session_wrapper(db.session)


def main(day_now=None, new_only=False, dry_run_only=False, reraise_all=False):   
    day_now = day_now or date.today()
    start_time = time.time()
    (condition_institute, condition_people) = get_conditions(only_inserts=new_only)
    _init_caches()
    try:
        logger.debug('Main method called with dry_run_only="%s"', str(dry_run_only))
        icms_insts = db.session.query(Institute).filter(condition_institute).all()
        icms_people_and_histories = db.session.query(Person, PersonHistory).join(PersonHistory, Person.cmsId == PersonHistory.cmsId)\
            .join(Institute, Person.instCode == Institute.code) \
            .filter(condition_people).all()
        sync_dbs(icms_insts, icms_people_and_histories, day_now, dry_run=dry_run_only)
        update_time_lines(icms_insts, icms_people_and_histories, day_now, dry_run=dry_run_only)
    except BaseException as e:
        db.session.rollback()
        logger.error(e, exc_info=True)
        if reraise_all:
            raise e

    if dry_run_only:
        logger.info('Dry mode invoked, rolling back the transaction')
        db.session.rollback()
    else:
        logger.info('DB commit imminent %s' % day_now)
        db.session.commit()

    time_summary_str = 'Running the updates took %.2f seconds for date: %s' % (time.time() - start_time, str(day_now))
    logger.info(time_summary_str)


def is_inst_epr_addable(inst):
    return inst.cmsStatus in ['Yes', 'Leaving', 'Associated', 'Incoming', 'Cooperating']


def is_person_epr_addable(person):
    if person.cmsId in ConfigProxy.get_epr_force_import_cms_ids():
        return True
    return person.status in ['CMS', 'CMSEXTENDED', 'CMSEMERITUS'] and person.instCode != 'ZZZ' and person.hrId != -1 \
           and is_inst_epr_addable(person.institute)


def get_conditions(only_inserts=False):
    """

    :param only_inserts:    if True, the people and insts from iCMS side that are taken into account
                            must not be present in EPR database
    :return:
    """
    # condition_institute = Institute.cmsStatus.in_(['Yes', 'Leaving', 'Associated', 'Incoming'])
    # condition_people = and_(and_(Person.status.in_(['CMS', 'CMSEXTENDED', 'CMSEMERITUS']),
    #                     and_(Person.instCode != 'ZZZ', Person.hrId != -1)), condition_institute)

    condition_institute = True
    condition_people = True

    if only_inserts:
        epr_inst_codes = [i.code for i in db.session.query(EprInst.code).all()]
        condition_new_insts = Institute.code.notin_(epr_inst_codes)
        condition_institute = and_(condition_institute, condition_new_insts)

    if only_inserts:
        epr_cms_ids = [u.cmsId for u in db.session.query(EprUser.cmsId).all()]
        condition_new_people = Person.cmsId.notin_(epr_cms_ids)
        condition_people = and_(condition_people, condition_new_people)

    return (condition_institute, condition_people)


if __name__ == '__main__':
    only_insert = False
    dry_run = False
    for arg in sys.argv:
        if arg.lower() == 'dryrun':
            dry_run = True
        if arg.lower() == 'onlynew':
            only_insert = True
    logger.info('Launching with "%s" as the EPR DB, dry_run="%s", only_insert="%s"', config['db']['epr'], dry_run, only_insert)
    main(new_only=only_insert, dry_run_only=dry_run)
