from scripts.watchdog.units.watchdog_base import WatchdogBase
from scripts.prescript_setup import db
from icms_orm.cmspeople import Institute, Person, InstStatusValues as InstStatus
from icms_orm.cmspeople import MemberStatusValues as MemberStatus
import sqlalchemy as sa


class MysqlIncorrectInfoWatchdog(WatchdogBase):
    
    def check_for_missing_nationalities(self):
        q = db.session().query(Person.cmsId, Person.isAuthor, Person.nationality)
        q = q.filter(sa.func.coalesce(Person.nationality, '') == '')
        q = q.filter(Person.status == MemberStatus.PERSON_STATUS_CMS)
        for cms_id, is_author, nationality in q.all():
            self.add_issue('Person with CMS ID {cms_id}({not_author}author) has no nationality info set. («{nationality}»)'.
                format(cms_id=cms_id, not_author = not is_author and 'not ' or '', nationality=nationality))

    def check_for_wrong_inst_sign_names(self):
        q = db.session().query(Institute.code, Institute.town, Institute.country, Institute.nameSign)
        q = q.filter(Institute.cmsStatus == InstStatus.YES)
        q = q.filter(sa.func.coalesce(Institute.nameSign, '') != '')
        q = q.filter(Institute.nameSign.notlike(sa.func.concat('%', Institute.town, ', ', Institute.country)))
        wrong = q.all()
        for code, town, country, name_sign in wrong:
            # INFN institutes can have a different town set by the last institute
            if 'INFN' in name_sign and town in name_sign and name_sign.endswith(country): continue
            # US institutes also have the state-code in the name:
            if country == 'USA' and town in name_sign and name_sign.endswith(country): continue
            self.add_issue('Institute {code} uses a sign name «{name_sign}» that does\'t end with «{town}, {country}»'.
                format(code=code, town=town, country=country, name_sign=name_sign))

    
    def check(self):
        self.check_for_missing_nationalities()
        self.check_for_wrong_inst_sign_names()
