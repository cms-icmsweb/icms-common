from scripts.watchdog.units.watchdog_base import AutonomousWatchdogBase, WatchdogRemedy
from icms_orm.common import Affiliation, Assignment, PersonStatus, InstituteLeader
from icms_orm import IcmsModelBase
import sqlalchemy as sa
import logging


class DuplicateRowsWatchdog(AutonomousWatchdogBase):
    def check(self):
        for mpr in [InstituteLeader, Affiliation, Assignment, PersonStatus]:
            pks = mpr.primary_keys()
            non_pks = [_c for _c in mpr.ia_list() if _c not in pks]
            count_col = sa.func.count(pks[0]).label('N')
            cols = [count_col] + non_pks
            q = mpr.session().query(*cols).group_by(*non_pks).from_self().filter(count_col > 1)

            for row in q.all():
                count = row[0]
                if count > 1:
                    self.add_issue('Table {0} has {1} rows of identical values of {2}'.format(mpr, row[0], row[1:]))
                    filters = dict()
                    for _col in non_pks:
                        filters[_col] = getattr(row, _col.key)
                    self.add_remedy(RowDuplicationRemedy(mpr, filters, limit=count - 1))



class RowDuplicationRemedy(WatchdogRemedy):
    counter = 0

    def __init__(self, mapper_class, filter_dict, limit):
        self.mpr = mapper_class
        self.filters = filter_dict
        self.limit = limit
        RowDuplicationRemedy.counter += 1
        self.counter_state = RowDuplicationRemedy.counter
        
    def apply(self):
        pks = self.mpr.primary_keys()
        q = self.mpr.session.query(*pks)
        for _k, _v in self.filters.items():
            q = q.filter(_k == _v)
        spared_id = q.first()
        q = q.filter(sa.or_(*[_pk != getattr(spared_id, _pk.key) for _pk in pks]))
        deleted = q.delete()
        assert deleted == self.limit, 'Deletion limit was set at {0} but delete matched {1} rows!'.format(
            self.limit, deleted)
        logging.debug('Applying deduplication remedy {0}/{1}. Sparing record ID {2}, deleted: {3}'.format(
            self.counter_state, RowDuplicationRemedy.counter, spared_id, deleted))
        
