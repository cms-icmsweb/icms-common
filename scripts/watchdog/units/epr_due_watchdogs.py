import logging
import traceback
from typing import Type

import sqlalchemy as sa
from datetime import date, datetime, timedelta
from scripts.prescript_setup import db
from scripts.watchdog.units.watchdog_base import AutonomousWatchdogBase, WatchdogRemedy
from icms_orm.epr import TimeLineUser as TLU, Category, EprUser, TimeLineInst as TLI
from icms_orm.common import EprAggregate, EprAggregateUnitValues
from icms_orm.cmspeople import MemberStatusValues as MSV, InstStatusValues as ISV, Person


class ApplicantDueRemedy(WatchdogRemedy):

    def apply(self):
        ssn = TLU.session()
        time_lines = ssn.query(TLU).join(Category, TLU.category == Category.id).\
            filter(sa.and_(
                TLU.cmsId == self._cms_id, TLU.year == self._year, TLU.timestamp >= sa.func.coalesce(
                    self._application_start, datetime(self._year, 1, 1, 0, 0)),
                TLU.status == MSV.PERSON_STATUS_CMS, TLU.isSuspended == False, Category.authorOK == True
            )).all()
        sum_of_fractions = sum([_tl.yearFraction for _tl in time_lines])
        for _tl in time_lines:
            assert isinstance(_tl, TLU)
            _tl.dueApplicant = self._correct_due * \
                (_tl.yearFraction / sum_of_fractions)
            ssn.add(_tl)

    def __init__(self, cms_id, year, correct_app_due, application_start) -> None:
        super().__init__()
        self._cms_id = cms_id
        self._year = year
        self._correct_due = correct_app_due
        self._application_start = application_start


class ApplicantDueWatchdog(AutonomousWatchdogBase):

    def __init__(self, epr_year=None):
        super(ApplicantDueWatchdog, self).__init__()
        self._epr_year = epr_year or date.today().year
        self._unblock_dates = dict()
        self._block_dates = dict()
        self._author_blocks = dict()
        self._inst_statuses = dict()
        self._affiliations = dict()
        self._past_dues = dict()
        self._applied_dues = dict()
        self._is_author = dict()
        self._status = dict()
        self._activity = dict()
        
        self.__init_author_data()
        self.__init_status_data()
        self.__init_activity_data()
        self.__init_author_block_data()
        self.__init_app_start_data()
        self.__init_billed_dues_data()
        self.__init_predating_credit_data()
        self.__init_inst_status_data()
        self.__init_affiliation_data()

    def __init_author_block_data(self):
        ssn = db.session
        block_q = ssn.query(Person.cmsId, Person.dateAuthorBlock, Person.dateAuthorUnblock, Person.isAuthorBlock).\
            filter(Person.status == MSV.PERSON_STATUS_CMS)
        for _cms_id, _date_block, _date_unblock, is_blocked in block_q.all():
            if _date_block:
                self._block_dates[_cms_id] = _date_block
            if _date_unblock:
                self._unblock_dates[_cms_id] = _date_unblock
            self._author_blocks[_cms_id] = is_blocked

    def __init_app_start_data(self):
        ssn = db.session
        sq = ssn.query(
            TLU.id.label('current'),
            sa.func.lag(TLU.id, 1).over(partition_by=TLU.cmsId, order_by=TLU.timestamp).label('previous')).\
            subquery()

        prev_tlu_alias = sa.orm.aliased(TLU)
        prev_category_alias = sa.orm.aliased(Category)
        latest_category_alias = sa.orm.aliased(Category)
        prev_tli_alias = sa.orm.aliased(TLI)

        q = ssn.query(TLU.cmsId, TLU.timestamp, TLI.timestamp, prev_tlu_alias.timestamp, prev_tli_alias.timestamp). \
            join(sq, TLU.id == sq.c.current). \
            join(Category, TLU.category == Category.id). \
            join(TLI, sa.and_(TLU.instCode == TLI.code, TLU.year == TLI.year, TLU.timestamp >= TLI.timestamp)). \
            join(prev_tlu_alias, sq.c.previous == prev_tlu_alias.id, isouter=True). \
            join(prev_category_alias, prev_tlu_alias.category == prev_category_alias.id, isouter=True). \
            join(prev_tli_alias, sa.and_(prev_tlu_alias.instCode == prev_tli_alias.code, prev_tlu_alias.year ==
                                         prev_tli_alias.year, prev_tli_alias.timestamp >= prev_tlu_alias.timestamp), isouter=True)
        # WARNING: join in the latest user data to ensure that they're still applicants
        q = q.join(EprUser, sa.and_(EprUser.cmsId == TLU.cmsId, EprUser.isSuspended == TLU.isSuspended, EprUser.status == TLU.status)).\
            join(latest_category_alias, sa.and_(EprUser.category ==
                                                latest_category_alias.id, latest_category_alias.authorOK == True))
        q = q.filter(sa.and_(
            TLI.cmsStatus == ISV.YES,
            TLU.status == MSV.PERSON_STATUS_CMS, Category.authorOK == True, TLU.isSuspended == False, TLU.isAuthor == False,
            sa.or_(prev_tlu_alias.status == None, prev_tlu_alias.status != 'CMS', prev_tlu_alias.isSuspended == True, prev_tlu_alias.isSuspended == None,
                   prev_category_alias.authorOK == False, prev_category_alias.authorOK == None, prev_tli_alias.cmsStatus != ISV.YES)
        ))
        # CRITICAL: there might be many TLIs for given inst-year combo - here we create windows to prefer most recent
        # TLIs wrt TLU's timestamp
        q = q.order_by(TLU.cmsId, sa.desc(TLU.timestamp), TLU.timestamp-TLI.timestamp, prev_tlu_alias.timestamp -
                       prev_tli_alias.timestamp).distinct(TLU.cmsId).from_self(TLU.cmsId, TLU.timestamp)

        self._app_starts = dict()
        rows = q.all()
        for _cms_id, _estimated_start in rows:

            blkOn = False
            # it usually takes one more day to establish authorship, so check the day after:
            if self.was_blocked_on(_cms_id, _estimated_start + timedelta(days=1)):
                self._app_starts[_cms_id] = _estimated_start
                blkOn = True
            else:
                _date_unblocked = self.get_unblock_date(_cms_id)
                if _date_unblocked is not None and _date_unblocked.year in [self._epr_year, self._epr_year - 1] and _date_unblocked != _estimated_start.date() :
                    # todo: unblocked this or past year - we should check to rule out recent app start
                    self.add_issue('APP START NEEDS FINE CHECKING: CMS ID {0}, estimated start: {1}, unblock: {2}'.
                                   format(_cms_id, _estimated_start.date(), _date_unblocked))
                self._app_starts[_cms_id] = None

    def __init_billed_dues_data(self):
        ssn = db.session
        billed_dues_query = ssn.query(TLU.cmsId, TLU.year, sa.func.sum(TLU.dueApplicant)). \
            filter(TLU.year > self._epr_year - 3).group_by(TLU.cmsId, TLU.year)
        for _cms_id, _year, _dues in billed_dues_query.all():
            if _year == self._epr_year:
                self._applied_dues[_cms_id] = _dues
            self._past_dues[(_cms_id, _year)] = _dues

    def __init_predating_credit_data(self):
        ssn = db.session
        q = ssn.query(EprAggregate.cms_id, EprAggregate.year, sa.func.sum(EprAggregate.aggregate_value)).\
            filter(sa.or_(EprAggregate.aggregate_unit == EprAggregateUnitValues.PLEDGES_DONE, EprAggregate.aggregate_unit == EprAggregateUnitValues.SHIFTS_DONE)).\
            group_by(EprAggregate.cms_id, EprAggregate.year)

        self._credits = dict()
        for _cms_id, _year, _credit in q.all():
            self._credits[_cms_id] = self._credits.get(_cms_id, dict())
            self._credits[_cms_id][_year] = _credit

    def __init_inst_status_data(self):
        ssn = db.session
        q = ssn.query(TLI.code, TLI.cmsStatus).filter(TLI.year == self._epr_year).order_by(
            TLI.code, sa.desc(TLI.timestamp)).distinct(TLI.code)
        self._inst_statuses = {_code: _status for _code, _status in q.all()}

    def __init_affiliation_data(self):
        ssn = db.session
        q = ssn.query(TLU.cmsId, TLU.instCode).filter(TLU.year == self._epr_year).order_by(
            TLU.cmsId, sa.desc(TLU.timestamp)).distinct(TLU.cmsId)
        self._affiliations = {_cms_id: _code for _cms_id, _code in q.all()}

    def __init_author_data(self):
        ssn = db.session
        q = ssn.query(TLU.cmsId, TLU.isAuthor).filter(TLU.year == self._epr_year).order_by(
            TLU.cmsId, sa.desc(TLU.timestamp)).distinct(TLU.cmsId)
        self._is_author = {_cms_id: _auth for _cms_id, _auth in q.all()}

    def __init_status_data(self):
        ssn = db.session
        q = ssn.query(TLU.cmsId, TLU.status).filter(TLU.year == self._epr_year).order_by(
            TLU.cmsId, sa.desc(TLU.timestamp)).distinct(TLU.cmsId)
        self._status = {_cms_id: _status for _cms_id, _status in q.all()}

    def __init_activity_data(self):
        ssn = db.session
        q = ssn.query(TLU.cmsId, TLU.category).filter(TLU.year == self._epr_year).order_by(
            TLU.cmsId, sa.desc(TLU.timestamp)).distinct(TLU.cmsId)
        self._activity = {_cms_id: _activity for _cms_id, _activity in q.all()}

    @property
    def applicant_cms_ids(self):
        return self._app_starts.keys()

    def get_inst_status(self, code):
        return self._inst_statuses.get(code)

    def get_affiliation(self, cms_id):
        return self._affiliations.get(cms_id)

    def get_author(self, cms_id):
        return self._is_author.get(cms_id)

    def get_status(self, cms_id):
        return self._status.get(cms_id, 'NOTCMS')

    def get_activity(self, cms_id):
        return self._activity.get(cms_id, -1)


    def get_app_start_date(self, cms_id) -> date:
        return (lambda x: isinstance(x, datetime) and x.date() or None)(self._app_starts.get(cms_id))

    def get_dues(self, cms_id, year=None):
        year = year or self._epr_year
        return self._past_dues.get((cms_id, year))

    def get_credit(self, cms_id, year):
        return self._credits.get(cms_id, {}).get(year, 0)

    def get_credit_predating_application(self, cms_id):
        total = 0
        _app_start = self.get_app_start_date(cms_id)
        _app_start_year = _app_start is not None and _app_start.year or 0
        for year, sub_credit in self._credits.get(cms_id, {}).items():
            # from 2023 onwards, allow only up to three months in the calendar year before the start of the application
            if year == _app_start_year-1:
                total += min( 3, sub_credit)
        if _app_start_year is not None:
            # total += self.get_overhead_credit_for_the_year_of_application_start( cms_id )
            # add all of the EPR work done in the year of application_start:
            total += self.get_credit(cms_id=cms_id, year=_app_start_year)
        return total

    def get_overhead_credit_for_the_year_of_application_start(self, cms_id):
        """
        For the work completed on the year of application's start it is impossible to establish the chronology.
        However, assuming a working rate of 1 month per month certain amount can be considered to predate
        the application, depending on when the application started and how big the EPR credit was.
        """
        _app_start = self.get_app_start_date(cms_id)
        if _app_start is None:
            return 0
        _credit = self.get_credit(cms_id=cms_id, year=_app_start.year)
        if _credit == 0:
            return 0
        _max_months_first_app_year = 12 * (date(_app_start.year + 1, 1, 1) - _app_start).days / (
            date(_app_start.year + 1, 1, 1) - date(_app_start.year, 1, 1)).days
        result = max(0, _credit - _max_months_first_app_year)
        if result > 0:
            logging.debug("User %d earned %.2f EPR in %d but only started the application on %s (with %.2f months left) - %.2f constitutes an overhead.",
                          cms_id, _credit, _app_start.year, _app_start, _max_months_first_app_year, result)
        return result

    def get_app_target(self, cms_id):
        return 6

    def get_amortized_app_target(self, cms_id):
        return self.get_app_target(cms_id) - self.get_credit_predating_application(cms_id)

    def get_expected_this_year_due(self, cms_id):
        _app_start = self.get_app_start_date(cms_id=cms_id)
        if _app_start is not None and _app_start.year in [self._epr_year - x for x in (0, 1)]:
            # that is the fraction carried over to the next year
            _fraction = (_app_start - date(_app_start.year, 1, 1)).days / 365
            if _app_start.year == self._epr_year:
                _fraction = 1 - _fraction
            return max(0, self.get_amortized_app_target(cms_id=cms_id) * _fraction)
        return 0

    def get_block_date(self, cms_id):
        return self._block_dates.get(cms_id)

    def get_unblock_date(self, cms_id):
        return self._unblock_dates.get(cms_id)

    def is_blocked(self, cms_id):
        return self._author_blocks.get(cms_id)

    def was_blocked_on(self, cms_id, request_date):
        request_date = request_date.date() if isinstance(
            request_date, datetime) else request_date
        _date_block = self.get_block_date(cms_id)
        _date_unblock = self.get_unblock_date(cms_id)
        _is_blocked = self.is_blocked(cms_id)

        if _is_blocked:
            if _date_block is None or _date_block <= request_date:
                return True
        else:
            if _date_unblock is not None and _date_unblock >= request_date:
                return True
        return False

    def add_issue_for(self, subject_str, issue_str):
        self.add_issue('{subject:<5}: {issue} [{url}]'.format(subject=subject_str, issue=issue_str,
                                                              url='https://icms.cern.ch/tools/users/profile/{0}'.format(subject_str)))

    def check(self):
        pass


class RoamingAppDueWatchdog(ApplicantDueWatchdog):
    def __init__(self, epr_year=None):
        super().__init__(epr_year=epr_year)

    def check(self):
        mistreated_applicant_ids = set()
        for _cms_id, _billed_due in self._applied_dues.items():

            # only consider active members who are not yet an author and whose activity is eligilble for applicantship:
            if ( not self.get_author( _cms_id )
                 and self.get_status( _cms_id ).startswith('CMS')
                 and self.get_activity( _cms_id ) in [ 1,2,7,8,10,11 ] 
                ) : 
                if self.get_dues(_cms_id) > 0:
                    _app_start = self._app_starts.get(_cms_id)
                    _issue = None
                    if _app_start is None:
                        _ic = self.get_affiliation(_cms_id)
                        _inst_status = self.get_inst_status(_ic)
                        if _inst_status == 'Yes':
                            _issue = '{total:.2f} EPR due in {year} but no application start found [{ic}, {inst_status}]!'.format(
                                total=self.get_dues(_cms_id), year=self._epr_year, ic=_ic, inst_status=_inst_status)
                    elif _app_start.year < self._epr_year - 1:
                        '{total:.2f} EPR due in {year} but seems to have started on {app_start}!'.format(
                            total=self.get_dues(_cms_id), app_start=_app_start, year=self._epr_year)
                    if _issue is not None:
                        self.add_issue_for(_cms_id, _issue)
                        self.add_remedy(ApplicantDueRemedy(
                            _cms_id, self._epr_year, 0, _app_start))


class IncorrectAppDueWatchdog(ApplicantDueWatchdog):
    def __init__(self, epr_year=None):
        super().__init__(epr_year=epr_year)

    def check(self):
        total_discrepancy = 0
        nCases = 0
        nAuth = 0
        for _cms_id in self.applicant_cms_ids:                
                _expected = self.get_expected_this_year_due(_cms_id)
                _billed = self.get_dues(_cms_id)                
                if abs(_expected - _billed) > 0.025:
                    # ignore people who are already authors, non-active members and people who have a category not allowing to be an applicant:
                    if ( not self.get_author( _cms_id )
                        #  and self.get_status( _cms_id ).startswith('CMS')
                        #  and self.get_activity( _cms_id ) in [ 1,2,7,8,10,11 ] 
                       ) :
                        self.add_issue_for(_cms_id, 'Expected due: {0:.2f}, billed : {1:.2f}, applicant since: {2}, X: {3:5.2f}, block: {4}, unblocked: {5}, blocked: {6}'.
                                        format(_expected, _billed,
                                                self.get_app_start_date(
                                                    _cms_id) or '  NEVER   ',
                                                self.get_credit_predating_application(
                                                    _cms_id),
                                                'Y' if self.is_blocked(
                                                    cms_id=_cms_id) else 'N',
                                                self.get_unblock_date(
                                                    _cms_id) or '  NEVER   ',
                                                self.get_block_date(
                                                    _cms_id) or '  NEVER   '
                                                ))
                        total_discrepancy += (_billed - _expected)
                        nCases += 1
                        self.add_remedy(ApplicantDueRemedy(
                            _cms_id, self._epr_year, _expected, self.get_app_start_date(_cms_id)))
                    else: # this "case" is already an author, count separately
                        if _billed > 0. :
                            nAuth += 1
                            self.add_issue_for(_cms_id, 'Actual author got billed : {0:.2f} as applicant !! '.format(_billed,))
                            self.add_remedy(ApplicantDueRemedy(
                                _cms_id, self._epr_year, 0., self.get_app_start_date(_cms_id)))

        if total_discrepancy != 0:
            self.add_issue( f'Total discrepancy: {total_discrepancy:.3} for {nCases} people (plus {nAuth} authors).' )
