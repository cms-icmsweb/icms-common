import logging
from icms_orm.cmspeople.institutes import InstStatusValues
import sqlalchemy as sa
from icms_orm.old_notes import WorkflowRecord
from sqlalchemy import and_

from scripts.prescript_setup import db
from icmsutils.businesslogic import paperstats
from icms_orm.toolkit import AuthorshipApplicationCheck as AAC
from icms_orm.cmspeople import MoData, Person, PersonHistory, PersonStatus, Institute
from datetime import date, datetime
from scripts.watchdog.units.watchdog_base import WatchdogBase


class SuspendedAuthorsWatchdog(WatchdogBase):
    """
    Used to find authors that are also suspended as that should not happen
    """

    def check(self):

        paper_ref_stats = paperstats.count_generated_al_per_year()

        q = db.session.query(Person.cmsId, Person.lastName, Person.firstName).\
            join( PersonStatus, PersonStatus.cmsId == Person.cmsId ).\
            filter(sa.or_( PersonStatus.status != 'EXMEMBER', PersonStatus.status != 'DECEASED' )).\
            filter(sa.and_(Person.isAuthorSuspended ==
                           True, Person.isAuthor == True))

        for cms_id, last_name, first_name in q.all():
            paper_own_stats = paperstats.count_signed_papers_by_year(
                cms_id=cms_id)
            report_line = '{first_name} {last_name} [CMS ID {cms_id}] is an author while being suspended! Was signing in {signing_string}'.format(
                cms_id=cms_id, signing_string=', '.join(
                    [str(x) for x in self._was_author_based_on_signing(paper_own_stats, paper_ref_stats)]),
                first_name=first_name, last_name=last_name
            )
            self.add_issue(report_line)

    def _was_author_based_on_signing(self, own_stats, ref_stats):
        years = []
        for year in sorted(own_stats.keys()):
            if own_stats.get(year) * 1.0 / ref_stats.get(year) > 0.5:
                years.append(year)
        return years


class MoImbalanceWatchodg(WatchdogBase):
    """Checks if on the institute level """

    def check(self):
        year = date.today().year

        mo_columns = [mo_column, phd_mo_column, free_mo_column] = \
            [getattr(MoData, c.key.replace('2018', str(year)))
             for c in [MoData.mo2018, MoData.phdMo2018, MoData.freeMo2018]]

        ssn = Person.session()

        [sq_flag_mo, sq_paid_mo, sq_free_mo] = [
            ssn.query(Person.instCode.label('ic'), sa.func.count(MoData.cmsId).label('count')).
            join(MoData, Person.cmsId == MoData.cmsId).filter(c == 'YES').filter(Person.status == 'CMS').
            group_by(Person.instCode).subquery() for c in mo_columns
        ]

        stats = ssn.query(sq_flag_mo.c.ic, sq_flag_mo.c.count, sq_paid_mo.c.count, sq_free_mo.c.count).\
            join(sq_paid_mo, sq_paid_mo.c.ic == sq_flag_mo.c.ic).join(sq_free_mo, sq_free_mo.c.ic == sq_flag_mo.c.ic).\
            all()

        for inst_code, flags, paid, free in stats:
            if flags > paid + free:
                self.add_issue('%s is above the MO cap! %d flagged, %d paid for and %d free' % (
                    inst_code, flags, paid, free))
            elif flags < paid + free:
                self.add_issue('%s is below the MO cap! %d flagged, %d paid for and %d free' % (
                    inst_code, flags, paid, free))


class MultipleCurrentWorkflowRecordEntriesWatchdog(WatchdogBase):
    """
    Detects cases when in the notes workflow there are multiple WorkflowRecord rows marked as current
    """

    def check(self):
        ssn = WorkflowRecord.session()
        sq = ssn.query(WorkflowRecord.process.label('process'), sa.func.count(WorkflowRecord.id).label('q')).\
            group_by(WorkflowRecord.process).filter(
                WorkflowRecord.status.like('%current%')).subquery()
        data = ssn.query(sq.c.process, sq.c.q).filter(sq.c.q > 1).all()
        for process, q in data:
            self.add_issue(
                'Process %d has %d WorkflowRecord entries marked as current' % (process, q))


class StalledAuthorshipCheckWatchdog(WatchdogBase):
    """
    Checks if the last authorship application check's date is not too old
    """

    def check(self):
        last_run_time = AAC.session().query(sa.func.max(AAC.datetime)).one()[0]
        if (datetime.now() - last_run_time).days > 1:
            self.add_issue(
                'Authorship Application Check does not seem to have run since %s' % last_run_time)
        else:
            logging.debug('All fine, AAC last run at %s' % last_run_time)


class ApprovedEmeritiWatchdog(WatchdogBase):
    def check(self):
        """
        Find all the approved emeritus that have not been updated accordingly in the DB
        :return:
        """
        _year = date.today().year
        _data = db.query(PersonStatus, Person).join(Person, Person.cmsId == PersonStatus.cmsId).\
            join(PersonHistory, PersonStatus.cmsId == PersonHistory.cmsId).filter(and_(
                PersonStatus.statusConf == 'CMSEMERITUS',
                PersonStatus.year == _year,
                PersonStatus.chairConfDate != None,
                PersonStatus.chairConfDate != '',
                PersonHistory.history.notlike(
                    '%CMSEMERITUS for {year} Confirmed%'.format(year=_year)),
                PersonHistory.history.notlike(
                    '%CMSEMERITUS for {year} Accepted%'.format(year=_year))
            )).all()
        for _status, _person in _data:
            assert isinstance(_person, Person)
            assert isinstance(_status, PersonStatus)
            self.add_issue('{name} {surname} [CMS ID {cms_id}] has been fully approved as CMSEMERITUS for {year} '
                           'on {date} but the corresponding DB update seems to be missing!'
                           .format(name=_person.firstName, surname=_person.lastName, cms_id=_person.cmsId,
                                   year=_year, date=_status.chairConfDate))


class MissingInstJoinDateWatchdog(WatchdogBase):
    def check(self):
        rows = db.query(Institute.code, Institute.dateCreate).filter(sa.and_(
            Institute.cmsStatus == InstStatusValues.YES, Institute.dateCmsIn == None)).all()
        for code, creation_date in rows:
            self.add_issue(
                f'CMS Member institute {code} was added to database on {creation_date} but the start date of their CMS membership is missing (dateCMSIn)')
