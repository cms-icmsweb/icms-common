from typing import List
import sqlalchemy as sa
from scripts.watchdog.units.watchdog_base import WatchdogBase
from icms_orm.common import PersonStatus, Affiliation, InstituteLeader, Assignment, PersonView
import abc


class PresentStateWatchdog(WatchdogBase, abc.ABC):
    """
    Wording isn't great on that but the goal is to check if someone has mor than 1 row denoting the present state.
    Subclasses define the tables to be checked and columns to group by.
    """
    @abc.abstractmethod
    def get_mapper_class(self):
        pass

    @abc.abstractmethod
    def get_grouping_columns(self) -> List:
        pass

    def get_filtering(self):
        return self.get_mapper_class().end_date == None

    def get_tablename(self):
        return self.get_mapper_class().__tablename__

    def check(self):
        ssn = self.get_mapper_class().session()
        _mpr = self.get_mapper_class()
        _cols = self.get_grouping_columns()
        _count_col = sa.func.count(_mpr.primary_keys()[0]).label('N')
        _cols.append(_count_col)
        q = ssn.query(*_cols).group_by(*self.get_grouping_columns())
        q = q.filter(self.get_filtering())
        q = q.from_self().filter(_count_col > 1)
        for row in q.all():
            _mesg = 'Multiple current states found! Table: {0}, keys: {1}, count: {2}'.format(self.get_tablename(), row[0:-1], row[-1])
            self.add_issue(_mesg)


class PresentPersonStatusWatchdog(PresentStateWatchdog):
    def get_grouping_columns(self):
        return [PersonStatus.cms_id]

    def get_mapper_class(self):
        return PersonStatus
        

class PresentAssignmentWatchdog(PresentStateWatchdog):
    def get_mapper_class(self):
        return Assignment
    def get_grouping_columns(self):
        return [Assignment.cms_id, Assignment.project_code]


class PresentAffiliationWatchdog(PresentStateWatchdog):
    def get_mapper_class(self):
        return Affiliation
    def get_grouping_columns(self):
        return [Affiliation.cms_id, Affiliation.inst_code]


class PresentTeamLeaderWatchodg(PresentStateWatchdog):
    def get_mapper_class(self):
        return InstituteLeader
    def get_grouping_columns(self):
        return [InstituteLeader.cms_id, InstituteLeader.inst_code]


class PresentPersonViewWatchdog(PresentStateWatchdog):
    def get_mapper_class(self):
        return PersonView
    def get_grouping_columns(self):
        return [PersonView.cms_id]
    def get_filtering(self):
        return True
