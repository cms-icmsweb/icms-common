#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

from setuptools import setup, find_packages

from os import path

here = path.abspath(path.dirname(__file__))

setup(
    name='icms-common',
    version='0.4',
    description='Scripts and library methods used by new iCMS web applications',
    long_description='Scripts and library methods used by new iCMS web applications',
    url='https://gitlab.cern.ch/cms-icmsweb/icms-common/',
    author='iCMS-support',
    author_email='icms-support@cern.ch',
    license='?',
    classifiers=[
        'Development Status :: 5 - Stable',

        'Programming Language :: Python :: 3.6',
    ],
    keywords='CERN iCMS',
    packages=['icmscommon', 'icmsutils'],
    install_requires=['psycopg2>=2.8.6,<3.0.0', 'python-ldap==3.2.0', 'pytz==2019.1'],
    entry_points={}
)
