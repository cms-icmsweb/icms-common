#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

from scripts.prescript_setup import db, config
import pytest
from icmsutils.icmstest.mock import MockInstFactory, MockPersonFactory, MockPersonMoFactory, MockActivityFactory, \
    MockFlagFactory, MockUserFactory
from icmsutils.icmstest.ddl import recreate_people_db
from icmsutils.icmstest.assertables import assert_attr_val
from icmsutils.icmstest.mock import set_mock_date
from icms_orm.cmspeople import Person, Institute, MoData
from icmsutils.businesslogic.authorship import AuthorRightsChecker as Checker
from datetime import date, datetime, timedelta
from icms_orm.cmspeople import PERSON_STATUS_CMS, PERSON_STATUS_EXMEMBER, PERSON_STATUS_CMSEMERITUS, PERSON_STATUS_DECEASED
from icmsutils.businesslogic.flags import Flag


class TimeGuide(object):
    """
    A simple helper for getting this year, past year, n years ago and so on
    """
    @staticmethod
    def past_year(years_ago):
        return date.today().year - years_ago

    @staticmethod
    def this_year():
        return TimeGuide.past_year(0)

    @staticmethod
    def past_date(days_ago):
        _d = date.today()
        _d -= timedelta(days=days_ago)
        return _d

    @staticmethod
    def today():
        return date.today()


inst_codes = (MEMBER, NON_MEMBER, ASSOCIATED, COOPERATING) = ['MEMBER', 'NON-MEMBER', 'ASSOCIATED', 'COOPERATING']

insts_data = [
    {Institute.code: MEMBER, Institute.cmsStatus: 'Yes'},
    {Institute.code: NON_MEMBER, Institute.cmsStatus: 'No'},
    {Institute.code: ASSOCIATED, Institute.cmsStatus: 'Associated'},
    {Institute.code: COOPERATING, Institute.cmsStatus: 'Cooperating'}
]
test_data = [
    ({Person.cmsId: 1001, Person.activityId: 9, Person.instCode: MEMBER, Person.status: PERSON_STATUS_CMS}, {
        MoData.get_mo_column_for_year(TimeGuide.this_year()): 'NO',
        MoData.get_phd_mo_column_for_year(TimeGuide.this_year()): 'NO'}, True),
    ({Person.cmsId: 1002, Person.activityId: 6, Person.instCode: MEMBER}, {
        k: 'YES' for k in [MoData.get_mo_column_for_year(TimeGuide.this_year()), MoData.get_free_mo_column_for_year(TimeGuide.this_year())]}, True),
    ({Person.cmsId: 1003, Person.activityId: 2, Person.instCode: MEMBER}, {
        k: 'YES' for k in [MoData.get_mo_column_for_year(TimeGuide.this_year()), MoData.get_phd_mo_column_for_year(TimeGuide.this_year())]}, True),
    # A non-doc student should not sign
    ({Person.cmsId: 1004, Person.activityId: 13, Person.instCode: MEMBER}, {}, False),
    # Should sign by DateEndSign
    ({Person.cmsId: 1005, Person.activityId: 6, Person.instCode: MEMBER, Person.dateEndSign:  TimeGuide.past_date(-90), Person.status: PERSON_STATUS_EXMEMBER}, {}, True),
    # Should not sign - grace period is up
    ({Person.cmsId: 2005, Person.activityId: 6, Person.instCode: MEMBER, Person.exDate:
        TimeGuide.past_date(370).strftime('%Y-%m-%d'), Person.status: PERSON_STATUS_EXMEMBER}, {}, False),
    # Should not sign - shorter explicit grace period (dateendsign) overrides the implicit one year (exdate)
    ({Person.cmsId: 2006, Person.activityId: 6, Person.instCode: MEMBER, Person.exDate:  '%d-01-01' % TimeGuide.this_year(),
        Person.status: PERSON_STATUS_EXMEMBER, Person.dateEndSign:  TimeGuide.past_date(days_ago=3)}, {}, False),
    # Should sign thanks to implicit grace period (how to know if he was an author while leaving though ?!)
    ({Person.cmsId: 2007, Person.activityId: 6, Person.instCode: MEMBER, Person.exDate:  '%d-01-01' % TimeGuide.this_year(),
        Person.status: PERSON_STATUS_EXMEMBER},
     {k: 'YES' for k in [MoData.get_mo_column_for_year(TimeGuide.this_year()), MoData.get_phd_mo_column_for_year(TimeGuide.this_year())]}, True),
    # Should not sign - not from a CMS Member Institute
    ({Person.cmsId: 1006, Person.activityId: 9, Person.instCode: NON_MEMBER, Person.status: PERSON_STATUS_CMS},
        {MoData.get_free_mo_column_for_year(TimeGuide.this_year()): 'NO', MoData.get_phd_mo_column_for_year(TimeGuide.this_year()): 'NO'}, False),
    # One year of extra authorship after leaving (todo: how do we check they were actually signing while leaving?)
    ({Person.cmsId: 1010, Person.activityId: 9, Person.instCode: MEMBER, Person.exDate: '%d-01-01' % TimeGuide.this_year(), Person.status: PERSON_STATUS_CMSEMERITUS}, {}, True),
    ({Person.cmsId: 2010, Person.activityId: 6, Person.instCode: MEMBER, Person.exDate: '%d-01-01' % TimeGuide.past_year(6), Person.status: PERSON_STATUS_CMSEMERITUS}, {}, True),
    ({Person.cmsId: 2011, Person.activityId: 2, Person.instCode: MEMBER, Person.dateEndSign: date(2015, 1, 1), Person.status: PERSON_STATUS_CMSEMERITUS}, {}, True),
    ({Person.cmsId: 2012, Person.activityId: 6, Person.instCode: MEMBER, Person.status: PERSON_STATUS_DECEASED, Person.exDate: TimeGuide.past_date(361).strftime('%Y-%m-%d')}, {
        MoData.get_mo_column_for_year(TimeGuide.past_year(1)): 'YES'}, True),
    ({Person.cmsId: 2013, Person.activityId: 6, Person.instCode: MEMBER, Person.status: PERSON_STATUS_DECEASED, Person.exDate: '%d-01-01' % TimeGuide.this_year()},
        {MoData.get_mo_column_for_year(TimeGuide.this_year()): 'YES'}, True),
    # Should be signing - has the author_no_mo flag
    ({Person.cmsId: 1020, Person.activityId: 6, Person.instCode: MEMBER}, {MoData.get_free_mo_column_for_year(TimeGuide.this_year()): 'NO'}, {Flag.MISC_AUTHORNOMO},  True),
    # Should not be signing - has the author_no flag
    ({Person.cmsId: 1021, Person.activityId: 9, Person.instCode: MEMBER, Person.status: PERSON_STATUS_CMS},
        {MoData.get_free_mo_column_for_year(TimeGuide.this_year()): 'YES'}, {Flag.MISC_AUTHORNO}, False),
    ({Person.cmsId: 1022, Person.activityId: 6, Person.instCode: MEMBER}, {MoData.get_phd_mo_column_for_year(TimeGuide.this_year()): 'YES'}, {Flag.MISC_AUTHORNO}, False),
    # MISC_authoryes should be omni-powerful when it comes to deciding authorship
    ({Person.cmsId: 3033, Person.activityId: 6, Person.instCode: COOPERATING, Person.status: PERSON_STATUS_EXMEMBER}, {}, {Flag.MISC_AUTHORYES}, True)
]

# People from non-member institutes should not be authors (is inst leaving CMS similar to a person leaving?)
# People with non-CMS status should not be signing (well, unless their date of exit allows or date end sign does so)
# People with MISC_authorno should not sign


@pytest.fixture(scope='session')
def setup_config():
    # ensure config is setup appropriately
    config.set(section='scripts', option='determine_authors_dry_run', value='False')
    config.set(section='scripts', option='updater_cms_id', value='-1')


@pytest.fixture(scope='session')
def create_mocks():
    MockPersonMoFactory.set_overrides(MoData,
        {
            MoData.get_mo_column_for_year(TimeGuide.this_year()): 'NO',
            MoData.get_phd_mo_column_for_year(TimeGuide.this_year()): 'NO',
            MoData.get_free_mo_column_for_year(TimeGuide.this_year()): 'NO'
        }
    )
    recreate_people_db()
    for flag in MockFlagFactory.create_all():
        db.session.add(flag)

    for overrides in insts_data:
        inst = MockInstFactory.create_instance(overrides)
        db.session.add(inst)
    _ = [db.session.add(x) for x in MockActivityFactory.create_all()]
    db.session.flush()
    db.session.commit()

    for entry in test_data:
        flags = []
        if len(entry) == 3:
            person_data, mo_data, _ = entry
        elif len(entry) == 4:
            person_data, mo_data, flags, _ = entry
        mock_person = MockPersonFactory.create_instance(instance_overrides=person_data)
        mock_mo = MockPersonMoFactory.create_instance(instance_overrides=mo_data, person=mock_person)
        mock_user = MockUserFactory.create_instance(person=mock_person)
        [db.session.add(obj) for obj in [mock_person, mock_mo, mock_user]]
        for flag in flags:
            db.session.add(MockFlagFactory.create_instance(person=mock_person, flag_id=flag))
        db.session.flush()

    db.session.commit()


@pytest.fixture(scope='session')
def checker(setup_config, create_mocks):
    c = Checker()
    c.run(only_create_model=False)
    return c


@pytest.mark.parametrize('cms_id, should_sign', [(r[0][Person.cmsId], r[-1]) for r in test_data])
def test_outcome(checker, cms_id, should_sign):
    assert_attr_val(Person, {Person.cmsId: cms_id}, {Person.isAuthor: should_sign})


@pytest.mark.parametrize('cms_id, is_actually_doctoral', [(r[0].get(Person.cmsId), r[0].get(Person.activityId) == 9) for r in test_data])
def test_is_doctoral(checker, cms_id, is_actually_doctoral):
    assert Checker._is_doctoral(checker, cms_id) == is_actually_doctoral


@pytest.mark.parametrize('cms_id, should_sign', [(r[0].get(Person.cmsId), r[-1]) for r in test_data])
def test_if_checker_decides_correctly(checker, cms_id, should_sign):
    assert Checker._check_individual_requirements(checker, cms_id) == should_sign, 'Remarks: %s' % Checker._get_remarks(checker, cms_id)


@pytest.mark.parametrize('cms_id, current_author', [(r[0].get(Person.cmsId), r[0].get(Person.isAuthor, False)) for r in test_data])
def test_is_current_author(checker, cms_id, current_author):
    assert Checker.is_current_author(checker, cms_id) == current_author


@pytest.mark.parametrize('cms_id, actual_days', [(r[0].get(Person.cmsId), (TimeGuide.today() - datetime.strptime(r[0].get(Person.exDate), '%Y-%m-%d').date()).days) for r in test_data if Person.exDate in r[0]])
def test_days_since_departure(checker, cms_id, actual_days):
    assert Checker._get_days_since_departure(checker, cms_id) == actual_days


def test_checker_is_initialized(checker):
    assert len(checker._cms_ids) == len(test_data)


@pytest.mark.parametrize('cms_id, actual_date', [(r[0].get(Person.cmsId), r[0].get(Person.dateEndSign)) for r in test_data if Person.dateEndSign in r[0]])
def test_date_end_sign_is_recognized(checker, cms_id, actual_date):
    assert checker._get_date_end_sign(cms_id) == actual_date
