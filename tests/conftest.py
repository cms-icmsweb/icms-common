import pytest
from icmsutils.icmstest.ddl import recreate_common_db, recreate_people_db


@pytest.fixture(name='recreate_common_db', scope='class')
def fixture_recreate_common_db():
    return recreate_common_db()


@pytest.fixture(name='recreate_people_db', scope='class')
def fixture_recreate_people_db():
    return recreate_people_db()
