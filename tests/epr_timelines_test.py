#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

# has to be the top import because: it reconfigures the db (a little) and it sets up the versioning magic
from scripts.sync_epr_timelines import main as sync_tls, db
import pytest
from datetime import date
from icms_orm.cmspeople.people import Person, MemberActivity, PersonHistory
from icms_orm.cmspeople.institutes import Institute
from icms_orm.epr import TimeLineInst as TLI, TimeLineUser as TLU, EprUser, EprInstitute as EprInst, Category, Pledge, Task
from icmsutils.prehistory import PersonAndPrehistoryWriter
from icmsutils.icmstest.assertables import assert_attr_val, count_in_db
from icmsutils.businesslogic import authorship, servicework
from icmsutils.icmstest.mock import icms_inst, icms_user, stu, phy, doc, activity_names, setup_capacities
from .testutils.scenarios import BogotaScenario, BogotaScenarioWithFreeloaders
from icms_orm.cmspeople import MemberStatusValues
from icmsutils.icmstest import ddl, assertables, mock

"""
Will need to reset the people DB as well as the EPR db.
Then will need to playback some fake people DB evolution story
"""

@pytest.fixture()
def reset_epr():
    return ddl.recreate_epr_db()

@pytest.fixture()
def reset_ppl():
    return ddl.recreate_people_db()


@pytest.mark.parametrize('run_no', range(1, 4))
def no_test_reloading_db(run_no, reset_epr, reset_ppl, setup_capacities):
    """
    Minimalistic method meant tu run a couple of times and reload the DB every time.
    Disabled by default but might come handy one day if DB recreation becomes an issue again...
    """

    print( 'So the main database is: %s' % db.config.get('SQLALCHEMY_DATABASE_URI', 'NOT FOUND?!') )

    db.session.add(icms_inst('UTRECHT', 'Yes', 'Utrecht', 'Netherlands', date(2010, 1, 1)))
    db.session.flush()
    for x in icms_user(12, 21, 'Jeroen', 'Dijkstra', 'CMS', 'UTRECHT', phy, False, False, 'dijkstra', date(2013, 2, 1)):
        # print 'Adding %s to session' % x
        db.session.add(x)
    for x in (
    icms_user(112, 211, 'Rodney', 'De Jong', 'CMS', 'UTRECHT', stu, False, False, 'dejong', date(2014, 12, 1))):
        # print 'Adding %s to session' % x
        db.session.add(x)
    db.session.commit()

    assert db.session.query(Person).count() == 2
    assert db.session.query(Institute).count() == 1

    sync_tls(day_now=date(2015, 1, 1))

    assert db.session.query(EprUser).count() == 2
    assert db.session.query(EprInst).count() == 1

    db.session.close()


def inst_due(code, year):
    return servicework.get_institute_due(code, year, db.session)


def inst_done(code, year):
    return servicework.get_institute_worked(db_session=db.session, inst_code=code, year=year)


def user_due(cms_id, year):
    return servicework.get_person_due(db_session=db.session, cms_id=cms_id, year=year)


def user_done(cms_id, year):
    return servicework.get_person_worked(cms_id, year, db.session)


def test_no_applicant_dues_for_pre_2015_entrance(reset_epr, reset_ppl):
    """
    Create an institute and add some people, change some activities etc but all these people will be registered before
    01.01.2015 so any launch of time lines sync should not attribute the applicant due

    In this scenario, Dijkstra and De Jong get registered before 01.01.2015 and never get any dues assigned.
    Jonk however registers later so as soon as he becomes an applicant (changing to doctoral student), he gets assigned some due
    """
    setup_capacities(db)
    db.session.add(icms_inst('UTRECHT', 'Yes', 'Utrecht', 'Netherlands', date(2010, 1, 1)))
    db.session.flush()
    for x in icms_user(12, 21, 'Jeroen', 'Dijkstra', 'CMS', 'UTRECHT', phy, False, False, 'dijkstra', date(2013, 2, 1)):
        # print 'Adding %s to session' % x
        db.session.add(x)
    for x in (icms_user(112, 211, 'Rodney', 'De Jong', 'CMS', 'UTRECHT', stu, False, False, 'dejong', date(2014, 12, 1))):
        # print 'Adding %s to session' % x
        db.session.add(x)
    db.session.commit()

    assert db.session.query(Person).count() == 2
    assert db.session.query(Institute).count() == 1

    sync_tls(day_now=date(2015, 1, 1))

    assert_attr_val(EprUser, {EprUser.cmsId: 12}, {EprUser.username: 'dijkstra'})
    assert_attr_val(EprUser, {EprUser.cmsId: 112}, {EprUser.username: 'dejong'})

    assert db.session.query(EprUser).count() == 2
    assert db.session.query(EprInst).count() == 1

    assert servicework.get_person_due(12, 2015, db.session) == 0
    assert servicework.get_person_due(112, 2015, db.session) == 0

    sync_tls(day_now=date(2015, 4, 1))

    assert servicework.get_person_due(12, 2015, db.session) == 0
    assert servicework.get_person_due(112, 2015, db.session) == 0

    sync_tls(day_now=date(2015, 7, 1))

    assert servicework.get_person_due(12, 2015, db.session) == 0
    assert servicework.get_person_due(112, 2015, db.session) == 0

    sync_tls(day_now=date(2015, 10, 1))

    assert servicework.get_person_due(12, 2015, db.session) == 0
    assert servicework.get_person_due(112, 2015, db.session) == 0

    _ = [db.session.add(x) for x in icms_user(1345, 5431, 'Jim', 'Jonk', 'CMS', 'UTRECHT', stu, False, False, 'jjonk', date(2015, 11, 1))]
    db.session.commit()
    """
    Rodney de Jong changes activity and becomes a Doctoral Student
    """
    rodney_de_jong = db.session.query(Person).filter(Person.firstName == 'Rodney').one()
    writer = PersonAndPrehistoryWriter(db_session=db.session, object_person=rodney_de_jong, actor_person=rodney_de_jong, date=date(2015, 11, 15))
    writer.set_new_value(Person.activityId, doc)
    writer.apply_changes(do_commit=True)
    sync_tls(day_now=date(2015, 11, 16))
    """
    Rodney de Jong should by now be considered an applicant - not sure though if he should get the due or not 
    (joined before 2015, but started the application only in 2015)
    """

    # make sure that Rodney's activity ID is now that of a doctoral student
    assert_attr_val(Person, {Person.firstName: 'Rodney'}, {Person.activityId: doc})

    sync_tls(day_now=date(2016, 1, 1))

    assert_attr_val(EprUser, {EprUser.cmsId: 1345}, {EprUser.username: 'jjonk'})

    assert 0 == servicework.get_person_due(12, 2016, db.session)
    # Rodney should get something between 5 and 5.5 months this year
    assert 4 < servicework.get_person_due(112, 2016, db.session) < 5.5
    assert 0 == servicework.get_person_due(1345, 2016, db.session)

    sync_tls(day_now=date(2016, 4, 1))

    assert servicework.get_person_due(12, 2016, db.session) == 0
    assert 4 < servicework.get_person_due(112, 2016, db.session) < 5.5
    assert servicework.get_person_due(1345, 2016, db.session) == 0

    # Now Jim Jonk becomes a doctoral student
    jim_jonk = db.session.query(Person).filter(Person.cmsId == 1345).one()
    writer = PersonAndPrehistoryWriter(db_session=db.session, object_person=jim_jonk, actor_person=jim_jonk, date=date(2016, 7, 1))
    writer.set_new_value(Person.activityId, doc)
    writer.apply_changes(do_commit=True)

    sync_tls(day_now=date(2016, 7, 1))

    assert servicework.get_person_due(12, 2016, db.session) == 0
    assert 4 < servicework.get_person_due(112, 2016, db.session) < 5.5

    # Checking that for a post-2015 application start the due is attributed correctly
    accounted_due = servicework.get_person_due(1345, 2016, db.session)
    expected_due = 0.5 * servicework.get_annual_applicant_due(2016)

    assert assertables.close_enough(accounted_due, expected_due, 0.06, 0.01), \
        'Expected the due to be around %.2f but got %.2f instead.' % (expected_due, accounted_due)

    sync_tls(day_now=date(2016, 10, 1))

    assert servicework.get_person_due(12, 2016, db.session) == 0
    assert 4 < servicework.get_person_due(112, 2016, db.session) < 5.5
    assert assertables.close_enough(servicework.get_person_due(1345, 2016, db.session), 0.5 * servicework.get_annual_applicant_due(2016), 0.06, 0.01)

    db.session.close()


def test_syncs(reset_epr, reset_ppl):
    setup_capacities(db)
    assert db.session.query(MemberActivity).count() == len(activity_names), 'People DB should now contain 5 capacity types'
    assert db.session.query(Category).count() == len(activity_names), 'EPR DB should now contain 5 capacity types'

    insts_data = (
        ['MOSCOW', 'Yes', 'Moscow', 'Russia', date(2015, 7, 1)],
        ['BERLIN', 'Cooperating', 'Berlin', 'Germany', date(2016, 1, 1)],
        ['LONDON', 'Associated', 'London', 'United Kingdom', None]
    )

    assert EprInst.query.count() == 0, 'EPR DB should contain no institutes at the moment'

    people_data = (
        [112, 7712, 'Ivan', 'Kuznetzov', 'CMS', 'MOSCOW', stu, False, False, 'ivank', date(2015, 1, 1), True, True, True],
        [113, 7713, 'Alexander', 'Romanov', 'EXMEMBER', 'MOSCOW', phy, False, True, 'sashar', date(2015, 2, 10), True, True, True],
        [114, 7714, 'Dmitri', 'Bystry', 'CMS', 'MOSCOW', stu, False, False, 'bystryd', date(2015, 3, 20), True, True, True],
        [1222, 8822, 'Jurgen', 'Heinz', 'CMS', 'BERLIN', doc, False, True, 'jheinz', date(2015, 4, 30), True, True, True],
        [1223, 8823, 'Joachim', 'Muller', 'CMS', 'BERLIN', stu, False, True, 'random2', date(2015, 5, 7), True, True, True],
        [4313, 12513, 'John', 'Miller', 'CMS', 'LONDON', phy, False, True, 'random4', date(2015, 5, 17), True, True, True],
        [4314, 12514, 'Joe', 'Doe', 'CMS', 'LONDON', phy, False, True, 'random5', date(2015, 5, 27), True, True, True],
    )

    latecomers_data = (
        [1224, 8824, 'Ottmar', 'Schmidt', 'CMS', 'BERLIN', doc, False, True, 'random3', date(2016, 3, 23), True, True, True],
        [4315, 12515, 'Bobby', 'Smith', 'CMS', 'LONDON', doc, False, True, 'random6', date(2016, 8, 1), True, True, True],
    )

    for _inst_data in insts_data:
        db.session.add(icms_inst(*_inst_data))
    db.session.flush()

    for _person_data in people_data:
        for x in icms_user(*_person_data):
            db.session.add(x)
    db.session.commit()

    sync_tls(day_now=date(2016, 1, 1))

    # AFTER THE FIRST TIME-LINES SYNC
    assert db.session.query(EprInst).filter(EprInst.year == 2016).count() == len(insts_data), \
        'Exactly %d institutes should now be in EPR db: MOSCOW and LONDON' % len(insts_data)

    # CMSid 112 should now be in EPR
    # epr_cms_ids = {x[0] for x in db.session.query(EprUser.cmsId).filter(EprUser.year == 2016).all()}
    active_members_in_src_db_count = count_in_db(Person, {Person.status: MemberStatusValues.PERSON_STATUS_CMS})
    assert count_in_db(EprUser.cmsId, {EprUser.year: 2016}) == active_members_in_src_db_count , 'Exactly %d people should now be in EPR DB' % active_members_in_src_db_count

    london = Institute.query.get('LONDON')
    setattr(london, Institute.cmsStatus.key, 'Yes')
    setattr(london, Institute.dateCmsIn.key, date(2016, 3, 23))
    db.session.add(london)
    db.session.commit()
    sync_tls(day_now=date(2016, 3, 23))

    # AFTER THE SECOND TIME_LINES SYNC
    assert db.session.query(TLI).filter(TLI.year == 2016).filter(
        TLI.code == 'LONDON').count() == 2, 'LONDON should have 2 TLs in 2016'

    sync_tls(day_now=date(2016, 8, 1))

    sync_tls(day_now=date(2016, 10, 11))

    sync_tls(day_now=date(2017, 1, 1))

    db.session.close()


def test_6_minus_X_rule(reset_epr, reset_ppl):
    scenario = BogotaScenario(db=db)

    oscar_cms_id = None
    jorge_cms_id = None
    jesus_cms_id = None

    for day in scenario.run():
        sync_tls(day_now=day, reraise_all=True)

        if day.year > 2015:
            assert inst_done('BOGOTA', 2015) == 2

        if day.year >= 2016:
            assert inst_due('BOGOTA', 2016) == 0

        if day == date(2016, 3, 18):
            rows = {row[0]: row[1] for row in db.session.query(Person.lastName, Person.cmsId).all()}
            oscar_cms_id, jorge_cms_id, jesus_cms_id = (rows.get(x) for x in ['Sanchez', 'Martinez', 'Gabriel'])
            if any(x is None for x in [oscar_cms_id, jorge_cms_id, jesus_cms_id]):
                raise ValueError('Failed to map some surnames to CMS ids')

        elif day == date(2016, 12, 31):
            assert user_due(oscar_cms_id, 2016) == 0
            assert user_due(jorge_cms_id, 2016) == 0
            assert user_due(jesus_cms_id, 2016) == 0
            assert user_due(jesus_cms_id, 2015) == 0
            assert inst_done('BOGOTA', 2016) == 14

        elif day == date(2017, 4, 1):
            assert assertables.close_enough(user_due(oscar_cms_id, 2017), 2.2602, 0.01, 0.01) # 1.875, 0.01, 0.01)
            assert assertables.close_enough(user_due(jorge_cms_id, 2017), 0, 0.01, 0.01)
            assert assertables.close_enough(user_due(jesus_cms_id, 2017),  2.2602, 0.01, 0.01) #  1.5, 0.01, 0.01)
            assert assertables.close_enough(inst_due('BOGOTA', 2017), 2*2.2602, 0.01, 0.01) # 3.375, 0.03, 0.01)

        elif day == date(2018, 1, 1):
            assert assertables.close_enough(user_due(oscar_cms_id, 2018), 0.7397, 0.01, 0.01)  #  0.625, 0.01, 0.01)
            assert assertables.close_enough(user_due(jorge_cms_id, 2018), 0, 0.01, 0.01)
            assert assertables.close_enough(user_due(jesus_cms_id, 2018), 0.7397, 0.01, 0.01)  # 0.5, 0.01, 0.01)
            assert assertables.close_enough(inst_due('BOGOTA', 2018), 2.*0.7397, 0.03, 0.01)  # 1.125, 0.03, 0.01)
    db.session.close()


def test_with_latecomer_freeloader(reset_epr, reset_ppl):
    scenario = BogotaScenarioWithFreeloaders(db)

    matheo_cms_id = None

    for day in scenario.run():
        sync_tls(day_now=day, reraise_all=True)

        if day == date(2016, 4, 1):
            matheo_cms_id = db.session.query(Person.cmsId).filter(Person.lastName == 'Pereira').one()[0]
        if day >= date(2016, 12, 31):
            assert assertables.close_enough(user_due(matheo_cms_id, 2016), 4.5, 0.01, 0.01)
        if day.year >= 2017:
            assert assertables.close_enough(user_due(matheo_cms_id, 2017), 1.5, 0.01, 0.01)
        if day.year >= 2018:
            assert assertables.close_enough(user_due(matheo_cms_id, 2018), 0, 0.01, 0.01)

    db.session.close()
