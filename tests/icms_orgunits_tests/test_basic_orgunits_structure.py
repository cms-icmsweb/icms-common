from tests.icms_orgunits_tests.fixtures import *
import pytest
from icms_orm.common import OrgUnit, OrgUnitType, Tenure, OrgUnitTypeName, PositionName, Position
from scripts.populate_org_unit_tables import Data


class Test2018Structure(object):
    @pytest.mark.parametrize('unit_domain, unit_type, unit_level', [
        ('Collaboration', OrgUnitTypeName.BOARD, 1), ('Publications', OrgUnitTypeName.COMMITTEE, 1),
        ('Diversity', OrgUnitTypeName.OFFICE, 1), ('L1 Trigger', OrgUnitTypeName.SUBDETECTOR, 2)
    ])
    def test_org_units(self, insert_org_units_as_of_2018, unit_type, unit_domain, unit_level):
        unit = OrgUnit.session().query(OrgUnit).filter(OrgUnit.domain == unit_domain).one()
        assert unit_level == unit.get(OrgUnit.type).get(OrgUnitType.level)
        assert unit_type == unit.get(OrgUnit.type).get(OrgUnitType.name)

    @pytest.mark.parametrize('cms_id, unit_domain, unit_type, position', Data.get_data())
    def test_tenures(self, insert_2018_tenures, cms_id, unit_domain, unit_type, position):
        tenure = Tenure.session().query(Tenure).filter(Tenure.cms_id == cms_id).one()
        assert isinstance(tenure, Tenure)
        assert position == tenure.position.get(Position.name)
        assert unit_type == tenure.unit.get(OrgUnit.type).get(OrgUnitType.name)
        assert unit_domain == tenure.unit.get(OrgUnit.domain)

