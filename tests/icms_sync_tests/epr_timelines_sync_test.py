import logging

import pytest
import sqlalchemy as sa
from scripts.prescript_setup import db, config
from icms_orm.cmspeople import Institute as I, Person as P, InstStatusValues as ISV, MemberStatusValues as MSV
from icms_orm.common import Person as PersonDst, Institute as InstDst
from datetime import date
from icmsutils.icmstest.assertables import assert_attr_val, count_in_db, close_enough
from icmsutils.icmstest.mock import OldIcmsMockPersonFactory, MockInstFactory, MockActivityFactory, MockFactory
from icmsutils.icmstest.ddl import recreate_people_db, recreate_common_db, recreate_epr_db
from scripts.sync_epr_timelines import main as epr_tls_main
from icms_orm import epr
from icmsutils.icmstest.mock import set_mock_date
from icmsutils.timelinesutils import year_fraction_left
from icmsutils.businesslogic import servicework


class Truth(object):
    _inst_data = [
        {
            'MOSCOW': {I.cmsStatus: ISV.YES, I.cernInstId: 123, I.country: 'Russian Federation'},
            'BEIRUT': {I.cmsStatus: ISV.YES, I.cernInstId: 234, I.country: 'Lebanon'},
            'TOKYO': {I.cmsStatus: ISV.COOPERATING, I.cernInstId: 345, I.country: 'Japan'},
            'SYDNEY': {I.cmsStatus: ISV.LEAVING, I.cernInstId: 456, I.country: 'Australia'},
            'RABAT': {I.cmsStatus: ISV.NO, I.cernInstId: 567, I.country: 'Morocco'},
            'LIMA': {I.cmsStatus: ISV.ASSOCIATED, I.cernInstId: 678, I.country: 'Lima'}
        }, {}, {}, {}, {}
    ]

    _ppl_data = [
        # Day 0
        {1: {
            P.firstName: 'Peter', P.lastName: 'Kuznetsov',
            P.instCode: 'MOSCOW',
            P.activityId: 6,
            P.isAuthorBlock: True,
            P.isAuthor: False,
            P.dateRegistration: date(2016, 8, 1),
            P.dateCreation: date(2016, 8, 1),
            P.isAuthorSuspended: False,
            P.status: MSV.PERSON_STATUS_CMS
        }, 2: {
            P.firstName: 'Hassan', P.lastName: 'El Hassan',
            P.instCode: 'BEIRUT',
            P.activityId: 6,
            P.isAuthorBlock: True,
            P.isAuthor: False,
            P.dateRegistration: date(2017, 1, 1),
            P.dateCreation: date(2017, 1, 1),
            P.isAuthorSuspended: False,
            P.status: MSV.PERSON_STATUS_CMS
        }, 3: {
            P.firstName: 'Hakim', P.lastName: 'El Hamit',
            P.instCode: 'BEIRUT',
            P.activityId: 6,
            P.isAuthorBlock: True,
            P.isAuthor: False,
            P.dateRegistration: date(2017, 1, 1),
            P.dateCreation: date(2017, 1, 1),
            P.status: MSV.PERSON_STATUS_CMS,
            P.isAuthorSuspended: False
        }},
        # Day 1
        {3: {P.isAuthorSuspended: True}},
        # Day 2
        {1: {P.firstName: 'Imaginame', P.lastName: 'Purposefully'}},
        # Day 3
        {1: {P.isAuthorBlock: False, P.isAuthor: True}},
        # Day 4
        {}
    ]

    @classmethod
    def people_data(cls, day):
        return cls._ppl_data[day] if len(cls._ppl_data) > day else {}

    @classmethod
    def insts_data(cls, day):
        return cls._inst_data[day] if len(cls._inst_data) > day else {}

    @staticmethod
    def cartesian(a, b):
        (a, b) = (list(_e() if callable(_e) else _e) for _e in (a, b))
        return zip(sorted(a * len(b)), b * len(a))

    @classmethod
    def days(cls):
        return [date(2017, 1, 1), date(2017, 7, 1), date(2018, 1, 1), date(2018, 7, 1), date(2019, 1, 1)]

    @classmethod
    def cms_ids(cls):
        return range(1, 4)

    @classmethod
    def inst_codes(cls):
        return ['MOSCOW', 'BEIRUT', 'TOKYO', 'SYDNEY', 'RABAT', 'LIMA']

    @classmethod
    def days_cms_ids(cls):
        return cls.cartesian(range(0, len(cls.days())), cls.cms_ids())

    @classmethod
    def days_inst_codes(cls):
        return cls.cartesian(range(0, len(cls.days())), cls.inst_codes())

    @classmethod
    def person_attr(cls, cms_id, day_no, attr):
        _data = cls.person_data(cms_id, day_no)
        return _data.get(attr)

    @classmethod
    def inst_data(cls, code, day_no=0):
        _data = {}
        _data.update(cls.insts_data(0).get(code, {}))
        for day in range(1, day_no + 1):
            _data.update(cls.insts_data(day).get(code, {}))
        _data.update({I.code: code})
        return _data

    @classmethod
    def person_data(cls, cms_id, day_no=0):
        _data = {}
        _data.update(cls.people_data(0).get(cms_id, {}))
        for day in range(1, day_no + 1):
            _data.update(cls.people_data(day).get(cms_id, {}))
        _data.update({P.cmsId: cms_id})
        return _data

    @classmethod
    def cern_inst_id_for_person(cls, cms_id, day_no=0):
        _data = cls.person_data(cms_id, day_no=day_no)
        _inst_data = cls.inst_data(code=_data.get(P.instCode), day_no = day_no)
        return _inst_data.get(I.cernInstId)

    @classmethod
    def inst_code_for_person(cls, cms_id, day_no=0):
        _data = cls.person_data(cms_id, day_no=day_no)
        _inst_data = cls.inst_data(code=_data.get(P.instCode), day_no=day_no)
        return _inst_data.get(I.code)

    @classmethod
    def cms_status_for_person(cls, cms_id, day_no=0):
        _data = cls.person_data(cms_id, day_no=day_no)
        return _data.get(P.status)

    @classmethod
    def cms_activity_for_person(cls, cms_id, day_no=0):
        return None

    @classmethod
    def suspension_for_person(cls, cms_id, day_no=0):
        return cls.person_data(cms_id, day_no).get(P.isAuthorSuspended)

    @classmethod
    def applicant_due(cls, cms_id, day_no, year):
        _dues = {_y: 0 for _y in range(2000, 2020)}
        _person_data = {}
        _begin = None

        def is_applicant(data):
            rules = {
                P.isAuthor: lambda v: v is False,
                P.isAuthorSuspended: lambda v: v is False,
                P.isAuthorBlock: lambda v: v is True,
                P.status: lambda v: v == MSV.PERSON_STATUS_CMS,
                P.activityId: lambda v: v in {6, 9}
            }
            return all([rules.get(key)(data.get(key)) for key in rules])

        for _d in range(0, day_no + 1):
            _person_data.update(cls.people_data(_d).get(cms_id, {}))
            if is_applicant(_person_data) and _begin is None:
                _begin = cls.days()[_d]
                # asking if they are authorship applicants
            if _begin is not None and (not is_applicant(_person_data) or _d == day_no):
                _end = cls.days()[_d]
                if is_applicant(_person_data) and _d == day_no:
                    _end = date(_end.year + 1, 1, 1)
                for _year in range(_begin.year, _end.year + 1):
                    fraction_start = year_fraction_left(_begin.year != _year and date(_year, 1, 1) or _begin)
                    fraction_end = _end.year == _year and year_fraction_left(_end) or 0
                    _dues[_year] = _dues.get(year, 0) + (fraction_start - fraction_end) * \
                            (servicework.get_annual_applicant_due(_year) - sum([_v for _k, _v in _dues.items() if _k < _year]))
                _begin = None
        return _dues.get(year, 0)

    @classmethod
    def author_due(cls, cms_id, day_no, year):
        _dues = {_y: 0 for _y in range(2000, 2020)}
        _begin = None
        _person_data = {}
        for _d in range(0, day_no + 1):
            _person_data.update(cls.people_data(_d).get(cms_id, {}))
            if _person_data.get(P.isAuthor) and _begin is None:
                _begin = cls.days()[_d]
            if (not _person_data.get(P.isAuthor) or _d == day_no) and _begin is not None:
                _end = cls.days()[_d]
                if _person_data.get(P.isAuthor) and _d == day_no:
                    _end = date(_end.year + 1, 1, 1)
                for _year in range(_begin.year, _end.year + 1):
                    fraction_start = year_fraction_left(_begin.year != _year and date(_year, 1, 1) or _begin)
                    fraction_end = _end.year == _year and year_fraction_left(_end) or 0
                    _dues[_year] = _dues.get(_year, 0) + servicework.get_annual_author_due(_year) * (fraction_start - fraction_end)
                _begin = None
        return _dues.get(year, 0)

    @classmethod
    def inst_status(cls, code, day_no=0):
        _data = cls.inst_data(code, day_no)
        return _data.get(I.cmsStatus)

    @classmethod
    def should_inst_be_in_epr(cls, code, day_no=0):
        """
        Any institute that has ever been a member remains in the EPR
        """
        for _d, _data in enumerate(cls._inst_data):
            if _d > day_no:
                break
            if _data.get(code, {}).get(I.cmsStatus, None) in {ISV.YES, ISV.COOPERATING, ISV.LEAVING, ISV.ASSOCIATED}:
                return True
        return False


@pytest.fixture(scope='class')
def epr_inst_id_by_code():
    return {_i.get(epr.EprInstitute.code): _i.get(epr.EprInstitute.id) for _i in db.session.query(epr.EprInstitute).all()}


@pytest.fixture(scope='class')
def day(request):
    Fixture()(request)
    return request.param


class Fixture(object):
    __used_scopes = set()
    __instance = None

    def __new__(cls):
        # single-threaded singleton
        if cls.__instance is None:
            logging.debug('Creating new fixture instance and storing that within the class')
            cls.__instance = super(Fixture, cls).__new__(cls)
        logging.debug('Returning fixture instance')
        return cls.__instance

    @classmethod
    def describe_scope(cls, request):
        return request.cls, request.param

    @classmethod
    def is_scope_used(cls, request):
        return cls.describe_scope(request) in cls.__used_scopes

    @classmethod
    def mark_scope_as_used(cls, request):
        _scope_desc = cls.describe_scope(request)
        ans = _scope_desc in cls.__used_scopes
        cls.__used_scopes.add(_scope_desc)
        return ans

    def fixture_body(self, request):
        day_no = request.param
        set_mock_date(Truth.days()[day_no])
        if day_no == 0:
            recreate_common_db()
            recreate_people_db()
            recreate_epr_db()
            MockFactory.reset_overrides()
            _ = [I.session().add(_a) for _a in MockActivityFactory.create_all()]

        s = I.session()
        _insts = {_i.get(I.code): _i for _i in s.query(I).all()}
        _ppl = {_p.get(P.cmsId): _p for _p in s.query(P).all()}

        for _code in Truth.inst_codes():
            _inst_data = Truth.inst_data(_code, day_no)
            if _inst_data:
                _inst = _insts.get(_code, MockInstFactory.create_instance())
                _ = [_inst.set(_k, _v) for _k, _v in _inst_data.items()]
                s.add(_inst)
        for _id in Truth.cms_ids():
            _person_data = Truth.person_data(_id, day_no)
            if _person_data:
                _perso = _ppl.get(_id, OldIcmsMockPersonFactory.create_instance())
                _ = [_perso.set(_k, _v) for _k, _v in _person_data.items()]
                s.add(_perso)
        db.session.commit()
        epr_tls_main(day_now=Truth.days()[day_no])

    def __call__(self, request):
        if self.is_scope_used(request):
            return
        self.mark_scope_as_used(request)
        self.fixture_body(request)


class TestTheTruth(object):
    """
    Here, at the expense of a bit of coupling, we can check if the Truth provider does a proper job
    """
    def test_suspension_changes(self):
        assert Truth.suspension_for_person(cms_id=3) is False
        assert Truth.suspension_for_person(cms_id=3, day_no=4) is True
        assert Truth.suspension_for_person(cms_id=3, day_no=0) is False

    @pytest.mark.parametrize('cms_id, day_no, year, expected', [
        (3, 0, 2017, 6), (1, 0, 2017, 6), (1, 1,
                                           2017, 6), (0, 1, 2018, 0), (1, 1, 2018, 0),
        (2, 1, 2018, 0), (1, 2, 2018, 0),
        # Member suspended on July 1, no retro-suspension though
        (3, 1, 2017, 6 * (31 + 28 + 31 + 30 + 31 + 30) / 365)
    ])
    def test_app_due_estimates_are_accurate(self, cms_id, day_no, year, expected):
        assert expected == Truth.applicant_due(cms_id, day_no, year)

    @pytest.mark.parametrize('cms_id, day_no, year, expected', [(1, 0, 2017, 0), (1, 3, 2018, 2), (1, 4, 2018, 2)])
    def test_author_due_estimates_are_accurate(self, cms_id, day_no, year, expected):
        assert close_enough(expected, Truth.author_due(cms_id, day_no, year), 0.02)


class TestTimeLineUser(object):
    @pytest.mark.parametrize('day, cms_id', Truth.days_cms_ids(), indirect=['day'])
    def test(self, day, cms_id):
        TLU = epr.TimeLineUser
        assert_attr_val(TLU, {TLU.cmsId: cms_id}, {
            TLU.instCode: Truth.inst_code_for_person(cms_id, day),
            TLU.isSuspended: Truth.suspension_for_person(cms_id, day),
        }, lambdas_list=[lambda q: q.order_by(sa.desc(TLU.timestamp)).limit(1)])

        year = Truth.days()[day].year
        person_due = servicework.get_person_due(cms_id, year, TLU.session())
        print(f'YEAR {year}, DUE: {person_due}')
        total = sum([due_func(cms_id, day, year)
                     for due_func in (Truth.author_due, Truth.applicant_due)])
        assert close_enough(
            total, person_due, 0.01), f'Total of {total} not close enough to person_due of {person_due}'


class TestUser(object):
    @pytest.mark.parametrize('day, cms_id', Truth.days_cms_ids(), indirect=['day'])
    def test(self, day, cms_id, epr_inst_id_by_code):
        assert_attr_val(epr.EprUser, {epr.EprUser.cmsId: cms_id}, {
            epr.EprUser.mainInst: epr_inst_id_by_code.get(Truth.inst_code_for_person(cms_id, day_no=day)),
            epr.EprUser.name: '{ln}, {fn}'.format(ln=Truth.person_attr(cms_id, day, P.lastName), fn=Truth.person_attr(cms_id, day, P.firstName))
        })


class TestInst(object):
    @pytest.mark.parametrize('day, inst_code', Truth.days_inst_codes(), indirect=['day'])
    def test(self, day, inst_code):
        if Truth.should_inst_be_in_epr(inst_code, day):
            assert 1 == count_in_db(epr.EprInstitute, {epr.EprInstitute.code: inst_code})
            assert_attr_val(epr.EprInstitute, {epr.EprInstitute.code: inst_code}, {
                epr.EprInstitute.cmsStatus: Truth.inst_status(inst_code, day)
            })
        else:
            assert 0 == count_in_db(epr.EprInstitute, {epr.EprInstitute.code: inst_code})


class TestTimeLineInst(object):
    @pytest.mark.parametrize('day, inst_code', Truth.days_inst_codes(), indirect=['day'])
    def test(self, day, inst_code):
        TLI = epr.TimeLineInst
        if Truth.should_inst_be_in_epr(inst_code):
            assert_attr_val(TLI, {TLI.code: inst_code}, {TLI.cmsStatus: Truth.inst_status(inst_code, day)},
                            lambdas_list=[lambda q: q.order_by(sa.desc(TLI.timestamp)).limit(1)])
        else:
            assert 0 == count_in_db(TLI, {TLI.code: inst_code})
