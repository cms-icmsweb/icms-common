from icmsutils.icmstest.ddl import recreate_common_db, recreate_people_db
from icmsutils.icmstest.assertables import assert_attr_val
from icmsutils.icmstest.mock import MockPersonFactory, MockActivityFactory, MockInstFactory, MockUserFactory, MockPersonMoFactory
from scripts.watchdog.units.present_states_watchdogs import PresentAssignmentWatchdog
from scripts.prescript_setup import db, config
from icms_orm.cmspeople import Institute, Person, User, MoData as OldMo
import pytest, random
from datetime import date, datetime
from scripts import sync_to_new_db
from scripts.sync import CountriesSyncAgent, FinalMoSyncAgent, ForwardSyncManager, FundingAgenciesSyncAgent, InstitutesSyncAgent, PeopleSyncAgent, brokers, launch
from icms_orm.common import Institute as NuInstitute, Person as NuPerson, MO as NuMO, InstituteLeader as NuLeader
from icms_orm.common import PersonStatus as NuPersonStatus, InstituteStatus as NuInstStatus
from icms_orm.common import Country as NuCountry, FundingAgency as FA
from icms_orm.common import Affiliation as NuAffiliation, Assignment, Project
from icms_orm import common
import sqlalchemy as sa

from .fixtures import BRN, GVA, ZRH, inst_codes, FIRST_DAY, SECOND_DAY, THIRD_DAY, FOURTH_DAY, FIFTH_DAY, SIXTH_DAY
from .fixtures import FA_CH_1, FA_CH_2, FA_CH_3
from .fixtures import set_mock_date
from scripts.sync.agents.sync_agents_projects import AssignmentsSyncAgent, ProjectsSyncAgent
from scripts.watchdog.units.duplicate_rows_watchdog import DuplicateRowsWatchdog

gva_cbi_id = zrh_cbi_id = brn_cbi_id = None


@pytest.fixture
def setup_stage():
    global zrh_cbi_id, gva_cbi_id, brn_cbi_id

    # IMPORTANT: enforce the default broker (ignoring triggers output)
    # config['sync']['fwd_sync_broker_class'] = 'DefaultBroker'
    brokers.DefaultBroker.force_enabled()

    recreate_common_db()
    recreate_people_db()

    for mock_activity in MockActivityFactory.create_all():
        db.session.add(mock_activity)

    db.session.flush()
    db.session.commit()

    MockPersonFactory.set_overrides(Person, {Person.activityId: 6, Person.isAuthor: True, Person.project: 'ECAL'})

    set_mock_date(FIRST_DAY)

    gva = MockInstFactory.create_instance({Institute.code: GVA, Institute.country: 'Switzerland',
                                           Institute.name: 'Geneva', Institute.cmsStatus: 'Cooperating',
                                           Institute.fundingAgency: FA_CH_1,
                                           Institute.dateCmsIn: date(2010, 1, 1)})
    zrh = MockInstFactory.create_instance({Institute.code: ZRH, Institute.country: 'Switzerland',
                                           Institute.name: 'Zurich', Institute.cmsStatus: 'ForReference',
                                           Institute.fundingAgency: FA_CH_2})
    brn = MockInstFactory.create_instance({Institute.code: BRN, Institute.country: 'Switzerland',
                                           Institute.name: 'Bern', Institute.cmsStatus: 'Yes',
                                           Institute.fundingAgency: FA_CH_3})

    db.session.add(gva)
    db.session.add(zrh)
    db.session.add(brn)
    db.session.flush()

    cbi_gva = MockPersonFactory.create_instance({Person.instCode: GVA, Person.birthDate: '1974-12-09'})
    cbi_gva_user = MockUserFactory.create_instance(person=cbi_gva)
    cbi_gva_mo = MockPersonMoFactory.create_instance(person=cbi_gva)
    db.session.add(cbi_gva)
    db.session.add(cbi_gva_user)
    db.session.add(cbi_gva_mo)
    db.session.flush()
    gva.cbiCmsId = cbi_gva.cmsId
    db.session.flush()
    gva_cbi_id = cbi_gva.cmsId

    cbi_zrh = MockPersonFactory.create_instance({Person.instCode: ZRH})
    cbi_zrh_user = MockUserFactory.create_instance(person=cbi_zrh)
    cbi_zrh_mo = MockPersonMoFactory.create_instance(person=cbi_zrh)
    db.session.add(cbi_zrh)
    db.session.add(cbi_zrh_user)
    db.session.add(cbi_zrh_mo)
    db.session.flush()
    zrh.cbiCmsId = cbi_zrh.cmsId
    db.session.flush()
    zrh_cbi_id = cbi_zrh.cmsId

    cbi_brn = MockPersonFactory.create_instance({Person.instCode: BRN, Person.birthDate: '1949-11-10'})
    cbi_brn_user = MockUserFactory.create_instance(person=cbi_brn)
    cbi_brn_mo = MockPersonMoFactory.create_instance(person=cbi_brn)
    db.session.add(cbi_brn)
    db.session.add(cbi_brn_user)
    db.session.add(cbi_brn_mo)
    db.session.flush()
    brn.cbiCmsId = cbi_brn.cmsId
    db.session.flush()
    brn_cbi_id = cbi_brn_user.cmsId

    total = 12
    birth_dates = ['1955-05-19', '1999-01-12', '1988-09-25'] # these result in an "Incorrect date value:" on mysql 8: , '0000-00-00', '0000-00-01']
    while total > 0:
        total -= 1
        mock_person = MockPersonFactory.create_instance(instance_overrides={
            Person.instCode: inst_codes[total % len(inst_codes)],
            Person.isAuthorSuspended: bool(random.getrandbits(1)),
            Person.isAuthor: bool(random.getrandbits(1)),
            Person.birthDate: birth_dates[total % len(birth_dates)]
        })
        mock_person.isAuthor = mock_person.isAuthor and not mock_person.isAuthorSuspended
        mock_user = MockUserFactory.create_instance(person=mock_person)
        mock_mo = MockPersonMoFactory.create_instance(person=mock_person)
        db.session.add(mock_person)
        db.session.add(mock_user)
        db.session.add(mock_mo)
        db.session.flush()

    db.session.commit()

    sync_to_new_db.sync_countries()
    sync_to_new_db.sync_institutes()
    sync_to_new_db.sync_people()
    sync_to_new_db.sync_team_leaders()
    sync_to_new_db.sync_affiliations()


@pytest.fixture
def mo_setup_stage(setup_stage):
    # mark all institutes as members
    for inst in db.session.query(Institute).all():
        setattr(inst, Institute.cmsStatus.key, 'YES')
    db.session.flush()

    for mo, person in db.session.query(OldMo, Person).join(Person, OldMo.cmsId == Person.cmsId).all():
        if person.instCode == BRN:
            # let's set everyone from BERN to have free MO in 2018
            setattr(mo, OldMo.freeMo2018.key, 'YES')
            setattr(mo, OldMo.mo2018.key, 'YES')
        elif person.instCode == ZRH:
            # let's set everyone from ZRH to be on the PHD list for 2017, 2016 and 2015
            setattr(mo, OldMo.phdInstCode2015.key, ZRH)
            setattr(mo, OldMo.phdMo2015.key, 'YES')
            setattr(mo, OldMo.phdFa2015.key, FA_CH_2)
            setattr(mo, OldMo.phdInstCode2016.key, ZRH)
            setattr(mo, OldMo.phdMo2016.key, 'YES')
            setattr(mo, OldMo.phdFa2016.key, FA_CH_2)
            setattr(mo, OldMo.phdInstCode2017.key, ZRH)
            setattr(mo, OldMo.phdMo2017.key, 'YES')
            setattr(mo, OldMo.phdFa2017.key, FA_CH_2)
            setattr(mo, OldMo.phdInstCode2018.key, ZRH)
            setattr(mo, OldMo.phdMo2018.key, 'YES')
            setattr(mo, OldMo.phdFa2018.key, FA_CH_2)
        elif person.instCode == GVA:
            # let's set everyone from GVA to have the "wishful" mo enabled but not to be on the PHD list for 2015-17
            setattr(mo, OldMo.mo2015.key, 'YES')
            setattr(mo, OldMo.mo2016.key, 'YES')
            setattr(mo, OldMo.mo2017.key, 'YES')
            setattr(mo, OldMo.mo2018.key, 'YES')
    db.session.flush()
    db.session.commit()

    sync_to_new_db.sync_countries()
    sync_to_new_db.sync_funding_agencies()
    sync_to_new_db.sync_mo()


def test_day_zero_sync(setup_stage):
    # CHECK THE INSTITUTES
    nu_insts = {getattr(r, NuInstitute.code.key): r for r in db.session.query(NuInstitute).all()}
    assert len(nu_insts) == 3
    assert_attr_val(NuInstStatus, {NuInstStatus.code: BRN, NuInstStatus.end_date: None}, {NuInstStatus.status: 'Yes'})
    assert_attr_val(NuInstStatus, {NuInstStatus.code: GVA, NuInstStatus.end_date: None}, {NuInstStatus.status: 'Cooperating'})
    assert_attr_val(NuInstStatus, {NuInstStatus.code: ZRH, NuInstStatus.end_date: None}, {NuInstStatus.status: 'ForReference'})


    # CHECK THE PEOPLE
    nu_ppl = db.session.query(NuPerson).all()
    assert len(nu_ppl) == 15
    assert_attr_val(NuPerson, {NuPerson.cms_id: zrh_cbi_id}, {NuPerson.cms_id: zrh_cbi_id, NuPerson.date_of_birth: None})
    assert_attr_val(NuPerson, {NuPerson.cms_id: gva_cbi_id}, {
                    NuPerson.cms_id: gva_cbi_id, NuPerson.date_of_birth: date(1974, 12, 9)})
    assert_attr_val(NuPerson, {NuPerson.cms_id: brn_cbi_id}, {
                    NuPerson.cms_id: brn_cbi_id, NuPerson.date_of_birth: date(1949, 11, 10)})


    # CHECK THE TEAM LEADERS
    nu_leaders = db.session.query(NuLeader).all()
    assert len(nu_leaders) == 3
    assert_attr_val(NuLeader, {NuLeader.inst_code: ZRH, NuLeader.end_date: None}, {NuLeader.cms_id: zrh_cbi_id})
    assert_attr_val(NuLeader, {NuLeader.inst_code: GVA, NuLeader.end_date: None}, {NuLeader.cms_id: gva_cbi_id})
    assert_attr_val(NuLeader, {NuLeader.inst_code: BRN, NuLeader.end_date: None}, {NuLeader.cms_id: brn_cbi_id})

    # CHECK THE AFFILIATIONS
    nu_affiliations = db.session.query(NuAffiliation).all()
    assert len(nu_affiliations) == 15
    assert_attr_val(NuAffiliation, {NuAffiliation.cms_id: gva_cbi_id, NuAffiliation.end_date: None},
                    {NuAffiliation.inst_code: GVA})
    assert_attr_val(NuAffiliation, {NuAffiliation.cms_id: zrh_cbi_id, NuAffiliation.end_date: None},
                    {NuAffiliation.inst_code: ZRH})
    assert_attr_val(NuAffiliation, {NuAffiliation.cms_id: brn_cbi_id, NuAffiliation.end_date: None},
                    {NuAffiliation.inst_code: BRN})


def test_inst_code_change(setup_stage):
    sb_from_gva = db.session.query(Person).filter(Person.cmsId != gva_cbi_id).filter(Person.instCode == GVA).first()
    sb_from_zrh = db.session.query(Person).filter(Person.cmsId != zrh_cbi_id).filter(Person.instCode == ZRH).first()
    sb_from_brn = db.session.query(Person).filter(Person.cmsId != brn_cbi_id).filter(Person.instCode == BRN).first()

    # FIRST ROUND OF CHANGES
    set_mock_date(SECOND_DAY)
    # the GVA person will change for ZURICH and BRN person will change for GVA
    sb_from_gva.instCode = ZRH
    sb_from_brn.instCode = GVA
    db.session.commit()

    sync_to_new_db.sync_affiliations()

    # the GVA person now should be affiliated with ZRH, but the previous record should be closed properly too
    assert_attr_val(NuAffiliation, {NuAffiliation.cms_id: sb_from_gva.cmsId, NuAffiliation.start_date: SECOND_DAY},
                    {NuAffiliation.end_date: None, NuAffiliation.inst_code: ZRH})
    assert_attr_val(NuAffiliation, {NuAffiliation.cms_id: sb_from_gva.cmsId, NuAffiliation.start_date: FIRST_DAY},
                    {NuAffiliation.end_date: SECOND_DAY, NuAffiliation.inst_code: GVA})

    # likewise, the BRN person should now be linked with GVA...
    assert_attr_val(NuAffiliation, {NuAffiliation.cms_id: sb_from_brn.cmsId, NuAffiliation.start_date: SECOND_DAY},
                    {NuAffiliation.end_date: None, NuAffiliation.inst_code: GVA})
    assert_attr_val(NuAffiliation, {NuAffiliation.cms_id: sb_from_brn.cmsId, NuAffiliation.start_date: FIRST_DAY},
                    {NuAffiliation.end_date: SECOND_DAY, NuAffiliation.inst_code: BRN})

    # and the ZRH person should still have that one affiliation entry, unchanged
    assert_attr_val(NuAffiliation, {NuAffiliation.cms_id: sb_from_zrh.cmsId, NuAffiliation.start_date: FIRST_DAY},
                    {NuAffiliation.end_date: None, NuAffiliation.inst_code: ZRH})

    # ANOTHER ROUND OF CHANGES
    set_mock_date(THIRD_DAY)
    # the GVA person goes back to GVA
    sb_from_gva.instCode = GVA
    db.session.commit()
    sync_to_new_db.sync_affiliations()

    # the GVA person should now be back at GVA, having the previous two affiliations terminated
    assert_attr_val(NuAffiliation, {NuAffiliation.cms_id: sb_from_gva.cmsId, NuAffiliation.start_date: THIRD_DAY},
                    {NuAffiliation.end_date: None, NuAffiliation.inst_code: GVA})
    assert_attr_val(NuAffiliation, {NuAffiliation.cms_id: sb_from_gva.cmsId, NuAffiliation.start_date: FIRST_DAY},
                    {NuAffiliation.end_date: SECOND_DAY, NuAffiliation.inst_code: GVA})
    assert_attr_val(NuAffiliation, {NuAffiliation.cms_id: sb_from_gva.cmsId, NuAffiliation.start_date: SECOND_DAY},
                    {NuAffiliation.end_date: THIRD_DAY, NuAffiliation.inst_code: ZRH})

    # the other two remain where they were last
    assert_attr_val(NuAffiliation, {NuAffiliation.cms_id: sb_from_brn.cmsId, NuAffiliation.start_date: FIRST_DAY},
                    {NuAffiliation.end_date: SECOND_DAY, NuAffiliation.inst_code: BRN})
    assert_attr_val(NuAffiliation, {NuAffiliation.cms_id: sb_from_brn.cmsId, NuAffiliation.start_date: SECOND_DAY},
                    {NuAffiliation.end_date: None, NuAffiliation.inst_code: GVA})
    assert_attr_val(NuAffiliation, {NuAffiliation.cms_id: sb_from_zrh.cmsId, NuAffiliation.start_date: FIRST_DAY},
                    {NuAffiliation.end_date: None, NuAffiliation.inst_code: ZRH})


def test_cbi_change(setup_stage):
    sb_from_gva = db.session.query(Person).filter(Person.cmsId != gva_cbi_id).filter(Person.instCode == GVA).first()
    sb_from_zrh = db.session.query(Person).filter(Person.cmsId != zrh_cbi_id).filter(Person.instCode == ZRH).first()
    sb_from_brn = db.session.query(Person).filter(Person.cmsId != brn_cbi_id).filter(Person.instCode == BRN).first()

    sb_else_from_gva = db.session.query(Person).filter(Person.cmsId != gva_cbi_id).\
        filter(Person.cmsId != sb_from_gva.cmsId).filter(Person.instCode == GVA).first()
    sb_else_from_zrh = db.session.query(Person).filter(Person.cmsId != zrh_cbi_id). \
        filter(Person.cmsId != sb_from_zrh.cmsId).filter(Person.instCode == ZRH).first()
    sb_else_from_brn = db.session.query(Person).filter(Person.cmsId != brn_cbi_id). \
        filter(Person.cmsId != sb_from_brn.cmsId).filter(Person.instCode == BRN).first()

    gva = db.session.query(Institute).filter(Institute.code == GVA).one()
    zrh = db.session.query(Institute).filter(Institute.code == ZRH).one()
    brn = db.session.query(Institute).filter(Institute.code == BRN).one()

    # FIRST ROUND OF CHANGES: CBIs OF GVA AND ZRH CHANGE, CBDs are set
    set_mock_date(SECOND_DAY)
    gva.cbiCmsId = sb_from_gva.cmsId
    zrh.cbiCmsId = sb_from_zrh.cmsId

    gva.cbdCmsId = sb_else_from_gva.cmsId
    zrh.cbdCmsId = sb_else_from_zrh.cmsId
    brn.cbdCmsId = sb_else_from_brn.cmsId

    db.session.commit()
    sync_to_new_db.sync_team_leaders()

    # ensure that GVA and ZRH changed their leaders, BRN retains theirs
    assert_attr_val(NuLeader, {NuLeader.inst_code: GVA, NuLeader.start_date: SECOND_DAY, NuLeader.is_primary: True},
                    {NuLeader.cms_id: sb_from_gva.cmsId, NuLeader.end_date: None})
    assert_attr_val(NuLeader, {NuLeader.inst_code: GVA, NuLeader.start_date: FIRST_DAY, NuLeader.is_primary: True},
                    {NuLeader.cms_id: gva_cbi_id, NuLeader.end_date: SECOND_DAY})

    assert_attr_val(NuLeader, {NuLeader.inst_code: ZRH, NuLeader.start_date: SECOND_DAY, NuLeader.is_primary: True},
                    {NuLeader.cms_id: sb_from_zrh.cmsId, NuLeader.end_date: None})
    assert_attr_val(NuLeader, {NuLeader.inst_code: ZRH, NuLeader.start_date: FIRST_DAY, NuLeader.is_primary: True},
                    {NuLeader.cms_id: zrh_cbi_id, NuLeader.end_date: SECOND_DAY})

    assert_attr_val(NuLeader, {NuLeader.inst_code: BRN, NuLeader.end_date: None, NuLeader.is_primary: True},
                    {NuLeader.cms_id: brn_cbi_id, NuLeader.start_date: FIRST_DAY})

    # check that all the CBDs are synced correctly
    for person, code in [(sb_else_from_gva, GVA), (sb_else_from_brn, BRN), (sb_else_from_zrh, ZRH)]:
        assert_attr_val(NuLeader, {NuLeader.inst_code: code, NuLeader.end_date: None, NuLeader.is_primary: False},
                    {NuLeader.cms_id: person.cmsId, NuLeader.start_date: SECOND_DAY})


    # SECOND ROUND OF CHANGES, FORMER CBI of ZRH RETURNS, BRN CHANGES CBI
    zrh.cbiCmsId = zrh_cbi_id
    brn.cbiCmsId = sb_from_brn.cmsId
    # ALSO, CBD of ZRH will now also be the CBD of GVA while CBD of BRN steps aside
    brn.cbdCmsId = None
    gva.cbd2CmsId = zrh.cbdCmsId
    for mock_date in (THIRD_DAY, FOURTH_DAY, FIFTH_DAY):
        set_mock_date(mock_date)
        db.session.commit()
        if mock_date == FIFTH_DAY:
            # check once with incremental broker - issues have shown up in the past
            brokers.ChangeLogAwareBroker.force_enabled()
        sync_to_new_db.sync_team_leaders()
        # for BRN a new CBI should now be in place
        assert_attr_val(NuLeader, {NuLeader.inst_code: BRN, NuLeader.end_date: None, NuLeader.is_primary: True},
                        {NuLeader.start_date: THIRD_DAY, NuLeader.cms_id: sb_from_brn.cmsId, NuLeader.is_primary: True})
        assert_attr_val(NuLeader, {NuLeader.inst_code: BRN, NuLeader.cms_id: brn_cbi_id, NuLeader.is_primary: True},
                        {NuLeader.start_date: FIRST_DAY, NuLeader.end_date: THIRD_DAY, NuLeader.is_primary: True})
        # ensure that ZRH's original leader returned and that BRN changed theirs
        assert_attr_val(NuLeader, {NuLeader.inst_code: ZRH, NuLeader.end_date: None, NuLeader.is_primary: True},
                        {NuLeader.start_date: THIRD_DAY, NuLeader.cms_id: zrh_cbi_id})
        assert_attr_val(NuLeader, {NuLeader.inst_code: ZRH, NuLeader.start_date: SECOND_DAY, NuLeader.is_primary: True},
                        {NuLeader.cms_id: sb_from_zrh.cmsId, NuLeader.end_date: THIRD_DAY, NuLeader.is_primary: True})
        assert_attr_val(NuLeader, {NuLeader.inst_code: ZRH, NuLeader.start_date: FIRST_DAY, NuLeader.is_primary: True},
                        {NuLeader.cms_id: zrh_cbi_id, NuLeader.end_date: SECOND_DAY, NuLeader.is_primary: True})
        # for GVA there should be no changes
        assert_attr_val(NuLeader, {NuLeader.inst_code: GVA, NuLeader.start_date: SECOND_DAY, NuLeader.is_primary: True},
                        {NuLeader.cms_id: sb_from_gva.cmsId, NuLeader.end_date: None, NuLeader.is_primary: True})
        assert_attr_val(NuLeader, {NuLeader.inst_code: GVA, NuLeader.start_date: FIRST_DAY, NuLeader.is_primary: True},
                        {NuLeader.cms_id: gva_cbi_id, NuLeader.end_date: SECOND_DAY, NuLeader.is_primary: True})

        # Checks for CBDs
        # BRN should now have no active CBD
        assert db.session.query(NuLeader).filter(
            sa.and_(NuLeader.inst_code == BRN, NuLeader.is_primary == False, NuLeader.end_date == None)).count() == 0
        # GVA now has 2 CBDs...
        assert db.session.query(NuLeader).filter(
            sa.and_(NuLeader.inst_code == GVA, NuLeader.end_date == None, NuLeader.is_primary == False)).count() == 2
        # ... because the CBD of Zurich serves there as well
        assert db.session.query(NuLeader).filter(
            sa.and_(NuLeader.cms_id == sb_else_from_zrh.cmsId, NuLeader.is_primary == False,
                    NuLeader.end_date == None)).count() == 2

        # FINAL CHECKS on record counts
        assert db.session.query(NuLeader).filter(sa.and_(NuLeader.inst_code == GVA, NuLeader.is_primary == True)).count() == 2
        assert db.session.query(NuLeader).filter(sa.and_(NuLeader.inst_code == ZRH, NuLeader.is_primary == True)).count() == 3
        assert db.session.query(NuLeader).filter(sa.and_(NuLeader.inst_code == BRN, NuLeader.is_primary == True)).count() == 2

    # re-enable the default broker
    brokers.DefaultBroker.force_enabled()


def test_cbi_and_cbd_changes(setup_stage):
    ppl_from_gva = db.session.query(Person).filter(Person.cmsId != gva_cbi_id).filter(Person.instCode == GVA).all()
    ppl_from_zrh = db.session.query(Person).filter(Person.cmsId != zrh_cbi_id).filter(Person.instCode == ZRH).all()
    ppl_from_brn = db.session.query(Person).filter(Person.cmsId != brn_cbi_id).filter(Person.instCode == BRN).all()

    gva_cbd = ppl_from_gva[1]
    gva_cbd2 = ppl_from_gva[2]
    zrh_cbd = ppl_from_zrh[1]

    gva = db.session.query(Institute).filter(Institute.code == GVA).one()
    zrh = db.session.query(Institute).filter(Institute.code == ZRH).one()
    brn = db.session.query(Institute).filter(Institute.code == BRN).one()

    # FIRST ROUND OF CHANGES:
    # - GVA hires CBDs 1 and 2
    # - CBD 2 of GVA one of which will serve a similar role for BRN
    # - CBI of BRN becomes the CBI of ZURICH too
    # - ZRH gets a CBD that is there to stay

    set_mock_date(SECOND_DAY)
    gva.cbdCmsId = gva_cbd.cmsId
    gva.cbd2CmsId = gva_cbd2.cmsId
    brn.cbdCmsId = gva.cbd2CmsId
    zrh.cbiCmsId = brn_cbi_id
    zrh.cbdCmsId = zrh_cbd.cmsId
    db.session.commit()
    sync_to_new_db.sync_team_leaders()

    # CBD 1 for GVA
    assert_attr_val(NuLeader, {NuLeader.inst_code: GVA, NuLeader.cms_id: gva_cbd.cmsId},
                    {NuLeader.start_date: SECOND_DAY, NuLeader.end_date: None, NuLeader.is_primary: False})
    # CBD 2 for GVA
    assert_attr_val(NuLeader, {NuLeader.inst_code: GVA, NuLeader.cms_id: gva_cbd2.cmsId},
                    {NuLeader.start_date: SECOND_DAY, NuLeader.end_date: None, NuLeader.is_primary: False})
    # GVA's CBD 2 as BRN's CBD
    assert_attr_val(NuLeader, {NuLeader.inst_code: BRN, NuLeader.is_primary: False, NuLeader.end_date: None},
                    {NuLeader.cms_id: gva_cbd2.cmsId, NuLeader.start_date: SECOND_DAY})
    # BRN's CBI is now the ZRH's CBI too
    assert_attr_val(NuLeader, {NuLeader.inst_code: ZRH, NuLeader.is_primary: True, NuLeader.end_date: None},
                    {NuLeader.cms_id: brn_cbi_id, NuLeader.start_date: SECOND_DAY})
    # CBD for ZRH
    assert_attr_val(NuLeader, {NuLeader.inst_code: ZRH, NuLeader.is_primary: False, NuLeader.end_date: None},
                    {NuLeader.cms_id: zrh_cbd.cmsId, NuLeader.start_date: SECOND_DAY})

    # SECOND ROUND OF CHANGES: second of GVA's CBDs becomes a CBI and quits his BRN position as ZRH's original CBI is back
    set_mock_date(THIRD_DAY)
    gva.cbiCmsId = gva.cbd2CmsId
    gva.cbd2CmsId = gva_cbi_id
    brn.cbdCmsId = None
    zrh.cbiCmsId = zrh_cbi_id
    [db.session.add(x) for x in [gva, brn, zrh]]
    db.session.commit()
    sync_to_new_db.sync_team_leaders()

    # GVA's former CBD 2 is now the CBI
    assert_attr_val(NuLeader, {NuLeader.inst_code: GVA, NuLeader.end_date: None, NuLeader.is_primary: True}, {NuLeader.cms_id: gva_cbd2.cmsId, NuLeader.start_date: THIRD_DAY})
    assert_attr_val(NuLeader, {NuLeader.inst_code: GVA, NuLeader.cms_id: gva_cbd2.cmsId, NuLeader.is_primary: False}, {NuLeader.end_date: THIRD_DAY, NuLeader.start_date: SECOND_DAY})

    # GVA's former CBI is now the CBD2
    assert_attr_val(NuLeader, {NuLeader.inst_code: GVA, NuLeader.cms_id: gva_cbi_id, NuLeader.is_primary: True}, {NuLeader.end_date: THIRD_DAY, NuLeader.start_date: FIRST_DAY})
    assert_attr_val(NuLeader, {NuLeader.inst_code: GVA, NuLeader.cms_id: gva_cbi_id, NuLeader.is_primary: False}, {NuLeader.end_date: None, NuLeader.start_date: THIRD_DAY})

    # BRN has no CBD anymore (used to have the CBD2 of GVA as their CBD)
    assert_attr_val(NuLeader, {NuLeader.inst_code: BRN, NuLeader.cms_id: gva_cbd2.cmsId, NuLeader.is_primary: False}, {NuLeader.end_date: THIRD_DAY, NuLeader.start_date: SECOND_DAY})

    # ZRH's original CBI is back
    assert_attr_val(NuLeader, {NuLeader.inst_code: ZRH, NuLeader.end_date: None, NuLeader.is_primary: True}, {NuLeader.cms_id: zrh_cbi_id, NuLeader.start_date: THIRD_DAY})

    # ZRH's CBD remains in place
    assert_attr_val(NuLeader, {NuLeader.inst_code: ZRH, NuLeader.is_primary: False, NuLeader.end_date: None}, {NuLeader.cms_id: zrh_cbd.cmsId, NuLeader.start_date: SECOND_DAY})

    # BRN's CBI remains in place and no longer serves as CBI to ZRH
    assert_attr_val(NuLeader, {NuLeader.inst_code: BRN, NuLeader.is_primary: True, NuLeader.end_date: None}, {NuLeader.cms_id: brn_cbi_id, NuLeader.start_date: FIRST_DAY})
    assert_attr_val(NuLeader, {NuLeader.inst_code: ZRH, NuLeader.is_primary: True, NuLeader.cms_id: brn_cbi_id}, {NuLeader.start_date: SECOND_DAY, NuLeader.end_date: THIRD_DAY})

    # FINAL CHECKS
    assert db.session.query(NuLeader).filter(NuLeader.is_primary == True).filter(NuLeader.end_date == None).count() == 3

def test_assignment_sync(setup_stage):
    db.session.add(Project.from_ia_dict({Project.code: 'ECAL', Project.name: 'ECAL'}))
    db.session.add(Project.from_ia_dict({Project.code: 'HCAL', Project.name: 'HCAL'}))
    db.session.commit()
    launch([AssignmentsSyncAgent])
    assert db.session.query(Assignment).filter(Assignment.end_date != None).count() == 0
    assert db.session.query(Assignment).filter(Assignment.end_date == None).count() > 0
    watchdog = PresentAssignmentWatchdog()
    watchdog.check()
    assert db.session.query(Assignment).count() > 0, "Database should contain target assignment entries by now"
    assert watchdog.has_found_issues() is False, "Expected not to find issues at this stage"

    # "more better" syntax exists but sometimes it would only work in test-case isolation, sometimes not at all:
    # db.session.query(Person).filter(Person.instCode == GVA).update({Person.project: 'HCAL'})
    # sa.update(Person).where(Person.instCode==GVA).values(project='HCAL')
    for p in db.session.query(Person).filter(Person.instCode == GVA):
        p.project = 'HCAL'
        db.session.add(p)
    
    db.session.commit()
    launch([AssignmentsSyncAgent])
    assert db.session.query(Assignment).filter(Assignment.end_date != None).count() > 0
    assert db.session.query(Assignment).filter(Assignment.end_date == None).count() > 0
    watchdog.check()
    assert watchdog.has_found_issues() is False, "Expected not to find issues at this stage"
    another_watchdog_but_of_a_different_type = DuplicateRowsWatchdog()
    another_watchdog_but_of_a_different_type.check()
    assert another_watchdog_but_of_a_different_type.has_found_issues() is False

def test_country_sync(setup_stage):
    sync_to_new_db.sync_countries()
    assert_attr_val(NuCountry, {NuCountry.code: 'PL'}, {NuCountry.name: 'Poland'})
    assert_attr_val(NuCountry, {NuCountry.code: 'PE'}, {NuCountry.name: 'Peru'})
    assert_attr_val(NuCountry, {NuCountry.code: 'BH'}, {NuCountry.name: 'Bahrain'})


def test_fa_sync(setup_stage):

    MockInstFactory.set_overrides(Institute, {Institute.cmsStatus: 'Yes'})

    for mock_data in [
        ('LDN', 'United Kingdom', 'London', 'United Kingdom'),
        ('ANT', 'Belgium', 'Antwerp', 'Belgium-XYZ'),
        ('NYC', 'USA', 'New York', 'USA-TEST-TEST'),
        ('PSI', 'Switzerland', 'PSI', 'Switzerland PSI'),
        ('MSC', 'Russia', 'Moscow', 'FA-Russia-MAIN'),
        ('DBN', 'Ireland', 'Dublin', 'EurFundAgent'),
        ('KNU', 'Korea', 'Kyungpook National University', 'Korea-1')
    ]:
        mock = MockInstFactory.create_instance({Institute.code: mock_data[0], Institute.country: mock_data[1], Institute.name: mock_data[2], Institute.fundingAgency: mock_data[3]})
        db.session.add(mock)
    db.session.commit()

    # let's assure that some extra FA names are also exported if them only appear in the MO table (RL-inspired)
    for _fa in ['USA-TEST-NP', 'Belgium-ABC', 'Korea-1B', 'FA-Russia-OTHER', 'Belgium-XYZ']:
        _p = MockPersonFactory.create_instance()
        _u = MockUserFactory.create_instance(person=_p)
        _mo_col = random.choice([OldMo.phdFa2017, OldMo.phdFa2019, OldMo.phdFa2018])
        _mo = MockPersonMoFactory.create_instance(person=_p, instance_overrides={_mo_col: _fa})
        _ = [db.session.add(_o) for _o in (_p, _u, _mo)]
    db.session.commit()

    ForwardSyncManager.launch_sync(agent_classes_override=[CountriesSyncAgent, FundingAgenciesSyncAgent, InstitutesSyncAgent, PeopleSyncAgent, FinalMoSyncAgent])

    for fa_name, fa_cc in {
        'United Kingdom': 'GB', 'Belgium-XYZ': 'BE', 'USA-TEST-TEST': 'US', 'Switzerland PSI': 'CH',
        'FA-Russia-MAIN': 'RU', 'EurFundAgent': None, 'Korea-1': 'KR', 'USA-TEST-NP': 'US', 'Belgium-ABC': 'BE',
        'Korea-1B': 'KR', 'FA-Russia-OTHER': 'RU'
    }.items():
        assert_attr_val(FA, {FA.name: fa_name}, {FA.country_code: fa_cc})


def test_missing_or_changed_gender_gets_synced_correctly(setup_stage):
    # pick some people, 2 females and 2 males

    males = db.session.query(User).filter(User.sex == 'M').limit(2).all()
    females = db.session.query(User).filter(User.sex == 'F').limit(2).all()

    set_mock_date(SECOND_DAY)
    males[0].set(User.sex, None)
    males[1].set(User.sex, 'F')

    females[0].set(User.sex, None)
    females[1].set(User.sex, 'M')

    for container in (males, females):
        for elem in container:
            db.session.add(elem)
    db.session.commit()

    sync_to_new_db.sync_people()

    assert_attr_val(NuPerson, {NuPerson.cms_id: males[0].cmsId}, {NuPerson.gender: None})
    assert_attr_val(NuPerson, {NuPerson.cms_id: females[0].cmsId}, {NuPerson.gender: None})

    assert_attr_val(NuPerson, {NuPerson.cms_id: males[1].cmsId}, {NuPerson.gender: common.GenderValues.FEMALE})
    assert_attr_val(NuPerson, {NuPerson.cms_id: females[1].cmsId}, {NuPerson.gender: common.GenderValues.MALE})

    # SOME OTHER DAY, SOME NEW CHANGE
    set_mock_date(THIRD_DAY)
    # set the gender back to be correct
    for e in males:
        e.set(User.sex, 'M')
        db.session.add(e)
    for e in females:
        e.set(User.sex, 'F')
        db.session.add(e)
    db.session.commit()
    sync_to_new_db.sync_people()

    for e in males:
        assert_attr_val(NuPerson, {NuPerson.cms_id: e.cmsId}, {NuPerson.gender: common.GenderValues.MALE})

    for e in females:
        assert_attr_val(NuPerson, {NuPerson.cms_id: e.cmsId}, {NuPerson.gender: common.GenderValues.FEMALE})


def test_newcomers_sync(setup_stage):
    """
    We rewind the clock and randomly add some people to some institutes.
    Then we'll check if that information has been propagated correctly.
    """
    for the_day in [SECOND_DAY, THIRD_DAY, FOURTH_DAY, FIFTH_DAY]:
        set_mock_date(the_day)

        newcomers_by_inst = {code: [] for code in inst_codes}
        newcomers_count = random.randint(0, 15)

        for _ in range(0, newcomers_count):
            newcomer = MockPersonFactory.create_instance(instance_overrides={Person.instCode: random.choice(inst_codes)})
            db.session.add(newcomer)
            db.session.add(MockUserFactory.create_instance(person=newcomer))
            db.session.add(MockPersonMoFactory.create_instance(person=newcomer))
            db.session.flush()
            # let's see where we added them
            newcomers_by_inst[newcomer.instCode].append(newcomer.cmsId)
        db.session.commit()

        sync_to_new_db.sync_people()
        sync_to_new_db.sync_affiliations()

        checked_newcomers_count = 0
        for ic in inst_codes:
            for cms_id in newcomers_by_inst.get(ic, []):
                checked_newcomers_count += 1
                # that basically just ensures the newly added person has been synchronised into the new db
                assert_attr_val(NuPerson, {NuPerson.cms_id: cms_id}, {NuPerson.cms_id: cms_id})
                # that makes sure that a new affiliation has been established and that it remains active
                assert_attr_val(NuAffiliation, {NuAffiliation.cms_id: cms_id}, {NuAffiliation.inst_code: ic, NuAffiliation.end_date: None, NuAffiliation.start_date: the_day})

        assert checked_newcomers_count == newcomers_count


def test_date_cms_in_gets_synced_correctly(setup_stage):
    assert_attr_val(NuInstitute, {NuInstitute.code: GVA}, {NuInstitute.official_joining_date: date(2010, 1, 1)})
    assert_attr_val(NuInstitute, {NuInstitute.code: ZRH}, {NuInstitute.official_joining_date: None})
    assert_attr_val(NuInstitute, {NuInstitute.code: BRN}, {NuInstitute.official_joining_date: None})

    set_mock_date(SECOND_DAY)

    gva = db.session.query(Institute).filter(Institute.code == GVA).one()
    zrh = db.session.query(Institute).filter(Institute.code == ZRH).one()
    brn = db.session.query(Institute).filter(Institute.code == BRN).one()

    zrh.set(Institute.dateCmsIn, date(2011, 11, 11))
    gva.set(Institute.dateCmsIn, date(2012, 12, 12))
    _ = [db.session.add(x) for x in (zrh, gva)]
    db.session.commit()
    sync_to_new_db.sync_institutes()

    assert_attr_val(NuInstitute, {NuInstitute.code: ZRH}, {NuInstitute.official_joining_date: date(2011, 11, 11)})
    assert_attr_val(NuInstitute, {NuInstitute.code: GVA}, {NuInstitute.official_joining_date: date(2012, 12, 12)})
    assert_attr_val(NuInstitute, {NuInstitute.code: BRN}, {NuInstitute.official_joining_date: None})

    set_mock_date(THIRD_DAY)

    gva.set(Institute.dateCmsIn, None)
    db.session.add(gva)
    db.session.commit()
    sync_to_new_db.sync_institutes()

    assert_attr_val(NuInstitute, {NuInstitute.code: ZRH}, {NuInstitute.official_joining_date: date(2011, 11, 11)})
    assert_attr_val(NuInstitute, {NuInstitute.code: GVA}, {NuInstitute.official_joining_date: None})
    assert_attr_val(NuInstitute, {NuInstitute.code: BRN}, {NuInstitute.official_joining_date: None})


def test_authorship_flag_syncs_fine(setup_stage):
    cms_ids = [x[0] for x in db.session.query(Person.cmsId).all()]
    # ----------------------------------- remove everubody's signing rights
    set_mock_date(SECOND_DAY)
    for p in db.session.query(Person).all():
        p.set(Person.isAuthor, False)
        db.session.add(p)
    db.session.commit()
    sync_to_new_db.sync_people()

    for cms_id in cms_ids:
        assert_attr_val(NuPersonStatus, {NuPersonStatus.cms_id: cms_id, NuPersonStatus.end_date: None},
                        {NuPersonStatus.is_author: False})

    # ------------------------------------ some people will get sigining rights
    set_mock_date(THIRD_DAY)
    random.shuffle(cms_ids)
    authors_to_be = cms_ids[0:7]

    for p in db.session.query(Person).filter(Person.cmsId.in_(authors_to_be)):
        p.set(Person.isAuthor, True)
        db.session.add(p)
    db.session.commit()
    sync_to_new_db.sync_people()
    for cms_id in cms_ids:
        assert_attr_val(NuPersonStatus, {NuPersonStatus.cms_id: cms_id, NuPersonStatus.end_date: None}, {NuPersonStatus.is_author: True if cms_id in authors_to_be else False})

    # ------------------------------------ two authors will lose signing rights, 2 non-authors will acquire them
    set_mock_date(FOURTH_DAY)
    authors_no_more = authors_to_be[-2:]
    newest_authors = cms_ids[-2:]

    for p in db.session.query(Person).filter(Person.cmsId.in_(authors_no_more)):
        p.set(Person.isAuthor, False)
        db.session.add(p)

    for p in db.session.query(Person).filter(Person.cmsId.in_(newest_authors)):
        p.set(Person.isAuthor, True)
        db.session.add(p)

    db.session.commit()
    sync_to_new_db.sync_people()

    for cms_id in cms_ids:
        should_sign = cms_id in newest_authors or cms_id in authors_to_be and cms_id not in authors_no_more
        assert_attr_val(NuPersonStatus, {NuPersonStatus.cms_id: cms_id, NuPersonStatus.end_date: None}, {NuPersonStatus.is_author: should_sign})


def test_activity_changes_sync_well(setup_stage):
    ppl = {p.get(Person.cmsId): p for p in db.session.query(Person).all()}
    cms_ids = ppl.keys()
    ppl_keys = list(ppl.keys())
    random.shuffle(ppl_keys)

    # --------------------- set everyone's activity to None
    set_mock_date(SECOND_DAY)
    for p in ppl.values():
        p.set(Person.activityId, None)
    db.session.commit()
    sync_to_new_db.sync_people()

    for cms_id in ppl_keys:
        assert_attr_val(NuPersonStatus, {NuPersonStatus.cms_id: cms_id, NuPersonStatus.end_date: None}, {NuPersonStatus.activity: None})

    # --------------------- set some people to PHD students, some to non-PHD, some to Physycists
    set_mock_date(THIRD_DAY)
    to_be_phd = []
    to_be_non_phd = []
    to_be_physicists = []
    for cms_id in cms_ids:
        random.choice([to_be_non_phd, to_be_phd, to_be_physicists]).append(cms_id)

    assert len(to_be_non_phd) + len(to_be_phd) + len(to_be_physicists) == len(cms_ids)

    for person in db.session.query(Person).all():
        value = None
        if person.get(Person.cmsId) in to_be_physicists:
            value = 6
        elif person.get(Person.cmsId) in to_be_non_phd:
            value = 13
        elif person.get(Person.cmsId) in to_be_phd:
            value = 9
        person.set(Person.activityId, value)
        db.session.add(person)
    db.session.commit()
    sync_to_new_db.sync_people()

    for cms_id in cms_ids:
        expected_activity = None
        if cms_id in to_be_phd:
            expected_activity = common.CmsActivityValues.PHD_STUDENT
        elif cms_id in to_be_non_phd:
            expected_activity = common.CmsActivityValues.NON_PHD_STUDENT
        elif cms_id in to_be_physicists:
            expected_activity = common.CmsActivityValues.PHYSICIST
        assert_attr_val(NuPersonStatus, {NuPersonStatus.cms_id: cms_id, NuPersonStatus.end_date: None}, {NuPersonStatus.activity: expected_activity})

    # --------------------- promote some students (to PHD students or to Physicists)
    # Well, not for now at least
