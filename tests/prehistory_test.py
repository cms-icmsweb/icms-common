#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

from scripts.prescript_setup import db
from icmsutils import prehistory as ph
import pytest
from datetime import date
from icmsutils.icmstest.mock import make_mock_person
from icms_orm.cmspeople.people import Person, MemberActivity
from icmsutils.icmstest.ddl import recreate_people_db
from icmsutils.icmstest.mock import MockActivityFactory
from icms_orm.cmspeople import PersonHistory
from icms_orm.common import PrehistoryTimeline
from icmsutils.prehistory import PrehistoryModel


@pytest.fixture(scope='module')
def prehistory_test_setup():
    recreate_people_db()
    while True:
        activity = MockActivityFactory.create_instance()
        if activity is not None:
            db.session.add(activity)
        else:
            db.session.commit()
            break

hist_a =u"""
iCMSfor2951_22/07/2014-StatusCMS:toEXMEMBERfromCMS
iCMSfor10100_19/06/2013-fromPH:CERNstatus=USERand-StatusCMS=CMSandLocalPercent=30
iCMSfor9217_18/02/2013-fromPH:CERNstatus=USERand-StatusCMS=CMSandLocalPercent=30
iCMSfor9217_18/02/2013-fromPH:CERNstatus=USERand-StatusCMS=CMSandLocalPercent=30
tbawej_01/04/2013-Activity:13
albelova_04/02/2013-InstCodeOther:
0: 17/01/2013 13:58IRegistration sent to CBI , 1: 17/01/2013 13:58ISubmitted by Mr. John Rocksmith (usermail@no.no) to CBI Prof. Some CBI (SOMEINST)[9999] as Physicist in project GEN in institute SOMEINST , 2: 17/01/2013 14:13ICBI approved, Activity = Non-Doctoral Student, M&O = , Project = GEN , 3: 17/01/2013 14:13ItoSecr , waitForSecrApproval , 4: 04/02/2013 12:44ISECR approved , 5: 04/02/2013 12:44IDONE , approvedBySecr
"""
person_a = make_mock_person(cms_id=1234, inst_code='SOMEINST', activity_name='Doctoral Student', status_cms='EXMEMBER')

hist_b = u"""
iCMSfor11862_02/02/2017-SWcleaningP3: AuthorUnBlock
iCMSfor11862_02/02/2017-SWcleaningP3: becomes Author
tdelaere_31/01/2017-FREEMO2017:NO
tdelaere_31/01/2017-MO2017:NO
tdelaere_31/01/2017-Remarks:
iCMSfor13_27/01/2017-Activity:to17from15
iCMSfor10537_05/05/2014-fromPH:CERNstatus=USERand-StatusCMS=CMSandLocalPercent=10
0: 17/01/2013 11:44IRegistration sent to CBI   , 1: 17/01/2013 11:44ISubmitted by Sven Svensson (svenhasnoem@il) to CBI Dr. Anna Smith (BEST_INST)[123] as Technician in project L1TRG in institute BEST_INST   , 2: 17/01/2013 11:46ICBI approved, Activity = Technician, M&O = , Project = L1TRG   , 3: 17/01/2013 11:46ItoSecr , waitForSecrApproval   , 4: 21/01/2013 18:05ISECR approved   , 5: 21/01/2013 18:05IDONE , approvedBySecr
"""
person_b = make_mock_person(cms_id=9998, activity_name='Engineer Electronics', status_cms='CMS', inst_code='BEST_INST')


hist_c = u"""
iCMSfor793_13/02/2015-StatusCMS:toEXMEMBERfromCMS
0: 22/01/2013 06:33IRegistration sent to CBI   , 1: 22/01/2013 06:33ISubmitted by Mr. Hernan Hector (nomail@no.no) to CBI Prof. Marc M. Surname (ANOTHER-INST)[123] as Doctoral Student in project HC in institute ANOTHER-INST   , 2: 22/01/2013 22:33ICBI approved, Activity = Doctoral Student, M&O = , Project = HC   , 3: 22/01/2013 22:33ItoSecr , waitForSecrApproval   , 4: 23/01/2013 19:23ISECR approved   , 5: 23/01/2013 19:23IDONE , approvedBySecr
"""
person_c = make_mock_person(cms_id=9999, activity_name='Doctoral Student', status_cms='EXMEMBER', inst_code='ANOTHER-INST')


hist_d = u"""
iCMSfor9981_08/06/2018-AuthorBlock:
tbawej_08/06/2018-Remarks:free MO2008 change of activity
iCMSfor12628_16/05/2018-fromPH:CERNstatus=USERand-StatusCMS=CMSandLocalPercent=100
iCMSfor1440_25/08/2015-StatusCMS:toEXMEMBERfromCMS
iCMSfor1440_25/08/2015-MO2016:NO
morozova_14/08/2008-Activity:3
morozova_13/08/2008-InstCode:FORMERLAB
morozova_23/08/2007-LocalPercent:0
0: 04/06/2007 10:18ISubmitted by Secretariat : Dr. Er Er (erer@mail.net) as Engineer Software in institute FORMERLAB   , 1: 05/06/2007 08:51ISECR approved   , 2: 05/06/2007 08:51IDONE , approvedBySecr
"""
"""

should yield the following timelines:

2018/05/06  ongoing     CMS         Physicist             NEWLAB
2015/08/25  2018/05/06  EXMEMBER    Physicist             NEWLAB
14/08/2008  2015/08/25  CMS         Physicist             NEWLAB
13/08/2008  14/08/2008  CMS         Engineer Software     NEWLAB
04/06/2007  13/08/2008  CMS         Engineer Software     FORMERLAB

"""

person_d = make_mock_person(cms_id=8888, activity_name='Physicist', status_cms='CMS', inst_code='NEWLAB')


hist_e = u"""
iCMSfor12628_20/06/2018-fromPH:CERNstatus=EXMPand-StatusCMS=CMSandLocalPercent=0
iCMSfor5271_09/04/2018-InstCode:toNORTHWESTERNfromCORNELL
0: 17/10/2015 06:22IRegistration sent to CBI   , 1: 17/10/2015 06:22ISubmitted by Ms. Liu Yihan (yl569@cornell.edu) to CBI Prof. James Alexander (CORNELL)[5271] as Non-Doctoral Student in project UPGRADE in institute CORNELL   , 2: 17/10/2015 16:08ICBI approved, Activity = Non-Doctoral Student, M&O = , Project = UPGRADE   , 3: 17/10/2015 16:08ItoSecr , waitForSecrApproval   , 4: 22/10/2015 14:59ISECR approved   , 5: 22/10/2015 14:59IDONE , approvedBySecr
"""

person_e = make_mock_person(cms_id=9999, activity_name='Non-Doctoral Student', status_cms='CMS', inst_code='CORNELL')


hist_f = u"""
asustina_07/09/2011-MO2012:YES
iCMSfor168_05/09/2011-MO2012:YES
morozova_16/06/2010-CERNdateEnd:
morozova_16/06/2010-LocalPercent:60
morozova_16/06/2010-CERNPhone:71567
morozova_16/06/2010-CERNOffice:40-4-B20
morozova_19/03/2009-InstCode:TBILISI-IHEPI
morozova_11/12/2007-StatusCMS:EXMEMBER
morozova_11/12/2007-InstCode:ANTWERPEN
morozova_11/12/2007-EmailProblem:DONOTSEND
toteva_1/18/2007-EmailProblem:
toteva_12/12/2006-DateEndSign:12/31/2007
toteva_08/09/2006-DateEndSign:
toteva_06/12/2005-CERNPhone:71547
toteva_06/12/2005-CERNOffice:40-2-B32
toteva_18/07/2005-InstCode:TBILISI-IPAS
toteva_18/07/2005-MOstatus:notMO
"""

person_f = make_mock_person(cms_id=12321, activity_name='Physicist', status_cms='CMS', inst_code='TBILISI-TSU')

person_h = make_mock_person(cms_id=32123, activity_name='Physicist', status_cms='CMS', inst_code='CORNELL')
hist_h = """
iCMSfor9899_06/02/2019-fromPH:CERNstatus=USERand-StatusCMS=CMSandLocalPercent=60
iCMSfor9899_06/02/2019-fromPH:CERNstatus=USERand-StatusCMS=CMSandLocalPercent=50
iCMSfor9981_01/01/2019-Author:True
iCMSfor7583_25/07/2018-MO2019:NO
iCMSfor10630_19/08/2016-MO2017:NO
iCMSfor912_27/02/2015-SWcleaning: AuthorUnBlocked
dolya_17/01/2014-MO2014:YES
iCMSfor9389_11/09/2013-PhysicsAccess:to1from0
iCMSfor8065_02/09/2013-MO2014:YES
albelova_15/10/2012-EmailProblem:DONOTSEND
albelova_15/10/2012-Remarks:
albelova_15/10/2012-StatusCMS:EXMEMBER
iCMSfor8065_10/09/2012-StatusCMS:toEXMEMBERfromCMS
iCMSfor9133_08/08/2012-fromPH:CERNstatus=USERand-StatusCMS=CMSandLocalPercent=10
iCMSfor8065_21/05/2012-MO2013:NO
albelova_14/05/2012-InstCodeOther:
"""

from_ph_lines = [
    (u'iCMSfor10100_19/06/2013-fromPH:CERNstatus=USERand-StatusCMS=CMSandLocalPercent=30', date(2013, 6, 19), 'CMS'),
    (u'iCMSfor9217_18/02/2013-fromPH:CERNstatus=USERand-StatusCMS=CMSandLocalPercent=30', date(2013, 2, 18), 'CMS'),
    (u'iCMSfor9217_18/02/2013-fromPH:CERNstatus=USERand-StatusCMS=CMSandLocalPercent=30', date(2013, 2, 18), 'CMS'),
    (u'iCMSfor12628_28/11/2017-fromPH:CERNstatus=EXMPand-StatusCMS=EXMEMBERandLocalPercent=0', date(2017, 11, 28), 'EXMEMBER')
]

change_lines = [
    (u'albelova_04/02/2013-InstCodeOther:', date(2013, 2, 4), None, None, None),
    (u'iCMSfor2951_22/07/2014-StatusCMS:toEXMEMBERfromCMS', date(2014, 7, 22), ph.PrehistoryEntry.Attr.CMS_STATUS, 'CMS', None),
    (u'tbawej_01/04/2013-Activity:13', date(2013, 4, 1), ph.PrehistoryEntry.Attr.CMS_ACTIVITY, 'Non-Doctoral Student', None),
    (u'iCMSfor13_27/01/2017-Activity:to17from15', date(2017, 1, 27), ph.PrehistoryEntry.Attr.CMS_ACTIVITY, 'Technician', None),
    (u'tdelaere_31/01/2017-FREEMO2017:NO', date(2017, 1, 31), None, None, None),
    (u'tdelaere_31/01/2017-MO2017:NO', date(2017, 1, 31), None, None, None),
    (u'tdelaere_31/01/2017-Remarks:', date(2017, 1, 31), None, None, None),
    (u'iCMSfor11862_02/02/2017-SWcleaningP3: AuthorUnBlock', date(2017, 2, 2), None, None, None),
    (u'iCMSfor11862_02/02/2017-SWcleaningP3: becomes Author', date(2017, 2, 2), None, None, None),
    (u'iCMSfor528_20/07/2016-Activity:to6from', date(2016, 7, 20), ph.PrehistoryEntry.Attr.CMS_ACTIVITY, None, None),
    (u'www-jpp-jpp_13/11/2005-Activity:6;MOstatus:notMO', date(2005, 11, 13), ph.PrehistoryEntry.Attr.CMS_ACTIVITY, 'Physicist', None),
    (u'iCMSfor1440_25/08/2015-StatusCMS:toEXMEMBERfromCMS', date(2015, 8, 25), ph.PrehistoryEntry.Attr.CMS_STATUS, 'CMS', 'EXMEMBER'),
    (u'morozova_7/24/2007-StatusCMS:CMS', date(2007, 7, 24), ph.PrehistoryEntry.Attr.CMS_STATUS, 'CMS', None)
]

reg_lines = [
    # first one is nice because the activity in the submitter form differs from the one finally assigned
    (u'0: 17/01/2013 13:58IRegistration sent to CBI , 1: 17/01/2013 13:58ISubmitted by Mr. John Rocksmith (usermail@no.no) to CBI Prof. Some CBI (SOMEINST)[9999] as Physicist in project GEN in institute SOMEINST , 2: 17/01/2013 14:13ICBI approved, Activity = Non-Doctoral Student, M&O = , Project = GEN , 3: 17/01/2013 14:13ItoSecr , waitForSecrApproval , 4: 04/02/2013 12:44ISECR approved , 5: 04/02/2013 12:44IDONE , approvedBySecr',
     'Non-Doctoral Student', 'GEN', 'SOMEINST'),
    (u'0: 17/01/2013 11:44IRegistration sent to CBI   , 1: 17/01/2013 11:44ISubmitted by Sven Svensson (svenhasnoem@il) to CBI Dr. Anna Smith (BEST_INST)[123] as Technician in project L1TRG in institute BEST_INST   , 2: 17/01/2013 11:46ICBI approved, Activity = Technician, M&O = , Project = L1TRG   , 3: 17/01/2013 11:46ItoSecr , waitForSecrApproval   , 4: 21/01/2013 18:05ISECR approved   , 5: 21/01/2013 18:05IDONE , approvedBySecr',
     'Technician', 'L1TRG', 'BEST_INST'),
    (u'0: 22/01/2013 06:33IRegistration sent to CBI   , 1: 22/01/2013 06:33ISubmitted by Mr. Hernan Hector (nomail@no.no) to CBI Prof. Marc M. Surname (ANOTHER-INST)[123] as Doctoral Student in project HC in institute ANOTHER-INST   , 2: 22/01/2013 22:33ICBI approved, Activity = Doctoral Student, M&O = , Project = HC   , 3: 22/01/2013 22:33ItoSecr , waitForSecrApproval   , 4: 23/01/2013 19:23ISECR approved   , 5: 23/01/2013 19:23IDONE , approvedBySecr',
     'Doctoral Student', 'HC', 'ANOTHER-INST'),
    # the trick here is that there is no value for project
    (u'0: 22/03/2016 10:41IRegistration sent to CBI   , 1: 22/03/2016 10:41ISubmitted by Ms. Carol Caroline (keuferpc@phys.ethz.ch) to CBI Prof. Hermann Dermann (A_N_O)[0000] as Administrative in project  in institute A_N_O   , 2: 22/03/2016 15:11ICBI approved, Activity = Administrative, M&O = , Project = EC   , 3: 22/03/2016 15:11ItoSecr , waitForSecrApproval   , 4: 23/03/2016 10:19ISECR approved   , 5: 23/03/2016 10:19IDONE , approvedBySecr',
     'Administrative', 'EC', 'A_N_O'),
    # and no "in Project" part altogether
    (u'0: 11/10/2011 08:35IRegistration sent to CBI   , 1: 11/10/2011 08:35ISubmitted by Ms. Jane Jade (no-reply@gmail.com) to CBI Prof. Ram Ram Ram (INDIA_INST)[000] as Doctoral Student in institute INDIA_INST   , 2: 14/04/2013 11:01ICBI approved, Activity = Doctoral Student, M&O = , Project = HC   , 3: 14/04/2013 11:01ItoSecr , waitForSecrApproval   , 4: 15/04/2013 16:18ISECR approved   , 5: 15/04/2013 16:18IDONE , approvedBySecr',
     'Doctoral Student', 'HC', 'INDIA_INST'),
    (u'0: 21/08/2015 12:25IRegistration sent to CBI   , 1: 21/08/2015 12:25ISubmitted by Dr. Kuznetsov Sergei (no-reply@gmail.com) to CBI Dr. Vladimir Ivanov (RUS_INST)[000] as Engineer Software in project OFFCOMP in institute RUS_INST   , 2: 21/08/2015 13:42ICBI approved, Activity = Engineer Software, M&O = , Project = OFFCOMP   , 3: 21/08/2015 13:42ItoSecr , waitForSecrApproval   , 4: 24/08/2015 10:28ISECR approved   , 5: 24/08/2015 10:28IDONE , ',
     'Engineer Software', 'OFFCOMP', 'RUS_INST'),
    (u'0: 30/04/2018 11:47ISubmitted by Secretariat : Mr. Meide Dario (123456@cern.ch) as Physicist in project CT-PPS in institute CERN-based   , 1: 30/04/2018 11:47ISECR approved   , 2: 30/04/2018 11:47IDONE , approvedBySecr',
     'Physicist', 'CT-PPS', 'CERN-based'),
    (u'0: 08/02/2007 20:11IRegistration sent to CBI   , 1: 08/02/2007 20:11ISubmitted by Dr. Ferrari Francesco (doctor@exmpl.it) to CBI Prof. Albert Einstein (PRINCETON)[307] as Physicist in institute PRINCETON   , 2: 09/02/2007 10:40ICBI approved, Activity = Engineer, M&O =    , 3: 09/02/2007 10:40ItoSecr , waitForSecrApproval   , 4: 14/02/2007 10:04ISECR approved   , 5: 14/02/2007 10:04IDONE , approvedBySecr', 'Engineer', None, 'PRINCETON'),
    (u'0: 06/02/2018 10:05IRegistration sent to CBI   , 1: 06/02/2018 10:05ISubmitted by Mr. Puerta Francesco (francesco.puerta@cern.ch) to CBI Livio Di Livio` (VERONA)[2738] as Physicist in project UPGRADE in institute VERONA   , 2: 06/02/2018 10:31ICBI approved, Activity = Physicist, M&O = , Project = Tracker   , 3: 06/02/2018 10:31ItoSecr , waitForSecrApproval   , 4: 06/02/2018 10:32ICBI approved, Activity = Non-Doctoral Student, M&O = , Project = Tracker   , 5: 06/02/2018 10:32ItoSecr , waitForSecrApproval   , 6: 06/02/2018 12:18ISECR approved   , 7: 06/02/2018 12:18IDONE , approvedBySecr', 'Non-Doctoral Student', 'Tracker', 'VERONA'),
    (u'0: 17/04/2007 16:29IRegistration sent to CBI   , 1: 17/04/2007 16:29ISubmitted by Mr. Thomas Daniele (thomas@to.infn.it) to CBI Dr. Amedeo Smith (TORINO)[1501] as Undergraduate Student in institute TORINO   , 2: 17/04/2007 16:31IRegistration sent to CBI   , 3: 17/04/2007 16:31ISubmitted by Mr. Thomas Daniele (thomas@to.infn.it) to CBI Dr. Amedeo Smith (TORINO)[1501] as Undergraduate Student in institute TORINO   , 4: 17/04/2007 16:31IRegistration sent to CBI   , 5: 17/04/2007 16:31ISubmitted by Mr. Thomas Daniele (thomas@to.infn.it) to CBI Dr. Amedeo Smith (TORINO)[1501] as Undergraduate Student in institute TORINO   , 6: 17/04/2007 16:39IRegistration sent to CBI   , 7: 17/04/2007 16:39ISubmitted by Mr. Thomas Daniele (thomas@to.infn.it) to CBI Dr. Amedeo Smith (TORINO)[1501] as Undergraduate Student in institute TORINO   , 8: 17/04/2007 16:51IRegistration sent to CBI   , 9: 17/04/2007 16:51ISubmitted by Mr. Thomas Daniele (thomas@to.infn.it) to CBI Dr. Amedeo Smith (TORINO)[1501] as Undergraduate Student in institute TORINO   , 10: 17/04/2007 16:56IRegistration sent to CBI   , 11: 17/04/2007 16:56ISubmitted by Mr. Thomas Daniele (thomas@to.infn.it) to CBI Dr. Amedeo Smith (TORINO)[1501] as Undergraduate Student in institute TORINO   , 12: 17/04/2007 17:06IRegistration sent to CBI   , 13: 17/04/2007 17:06ISubmitted by Mr. Thomas Daniele (thomas@to.infn.it) to CBI Dr. Amedeo Smith (TORINO)[1501] as Undergraduate Student in institute TORINO   , 14: 17/04/2007 17:09IRegistration sent to CBI   , 15: 17/04/2007 17:09ISubmitted by Mr. Thomas Daniele (thomas@to.infn.it) to CBI Dr. Amedeo Smith (TORINO)[1501] as Undergraduate Student in institute TORINO   , 16: 18/04/2007 11:07ISubmitted by Secretariat : Wayne Adrianne (awayne@psl.wisc.edu) as Engineer Mechanical in institute WISCONSIN   , 17: 18/04/2007 11:59ISECR approved   , 18: 18/04/2007 11:59IDONE , approvedBySecr', 'Engineer Mechanical', None, 'WISCONSIN')
]

short_reg_lines = [
    (u'0: 07/12/2015 20:14ISubmitted by Secretariat : Dr. Hidden Hidden (joe.doe@big.edu) as Physicist in project BRIL in institute ANTARCTICA   , 1: 15/12/2015 12:20ISECR approved   , 2: 15/12/2015 12:20IDONE , approvedBySecr   ', 'Physicist', 'BRIL', 'ANTARCTICA'),
    (u'0: 18/07/2011 14:31ISubmitted by Secretariat : Mr. Andersen Hans (no-reply@mail.com) as Non-Doctoral Student in institute EGYPT-UNIV   , 1: 18/07/2011 14:31ISECR approved   , 2: 18/07/2011 14:31IDONE , approvedBySecr   ', 'Non-Doctoral Student', None, 'EGYPT-UNIV'),
    (u'0: 15/01/2018 10:56ISubmitted by Secretariat : Ms. Lee Jieun (LeeJieun@gmail.com) as Non-Doctoral Student in project L1TRG in institute KYUNGPOOK   , 1: 15/01/2018 11:32ISECR approved   , 2: 15/01/2018 11:32IDONE , approvedBySecr', 'Non-Doctoral Student', 'L1TRG', 'KYUNGPOOK'),
    (u'0: 28/04/2017 14:25ISubmitted by Secretariat : Mr. Bialy Tomas (tomasz.bialy@gmail.com) as Non-Doctoral Student in project GEN in institute CERN-based   , 1: 28/04/2017 16:32ISECR approved   , 2: 28/04/2017 16:32IDONE , approvedBySecr', 'Non-Doctoral Student', 'GEN', 'CERN-based')
]


@pytest.mark.parametrize('line, entry_date, cms_status', from_ph_lines)
def test_regex_on_from_ph_lines(line, entry_date, cms_status, prehistory_test_setup):
    assert ph.PrehistoryRegistration.from_line(line) is None, 'From-PH line qualified as registration line'
    assert ph.PrehistoryChange.from_line(line) is None, 'From PH line qualified as change line'
    assert ph.PrehistoryRegistrationShort.from_line(line) is None
    entry = ph.PrehistoryFromPh.from_line(line)
    assert entry is not None, 'From PH line not recognized properly'
    assert entry.new_values[ph.PrehistoryEntry.Attr.CMS_STATUS] == cms_status
    assert entry.date == entry_date


@pytest.mark.parametrize('line, change_date, attribute, value, _', change_lines)
def test_regex_on_change_lines(line, change_date, attribute, value, _, prehistory_test_setup):
    assert ph.PrehistoryRegistration.from_line(line) is None
    assert ph.PrehistoryFromPh.from_line(line) is None
    assert ph.PrehistoryRegistrationShort.from_line(line) is None
    entry = ph.PrehistoryChange.from_line(line)
    assert entry is not None, 'A change line %s not recognized' % (line)
    assert entry.date == change_date
    if attribute is not None:
        assert entry.old_values[attribute] == value


@pytest.mark.parametrize('line, activity, project, inst', reg_lines)
def test_regex_on_reg_lines(line, activity, project, inst, prehistory_test_setup):
    assert ph.PrehistoryChange.from_line(line) is None, 'Reg line qualified as change line'
    assert ph.PrehistoryFromPh.from_line(line) is None, 'Reg line qualified as from ph line'
    entry = ph.PrehistoryRegistration.from_line(line)
    assert entry is not None, 'Reg line not recognized'
    assert entry.new_values[ph.PrehistoryEntry.Attr.CMS_ACTIVITY] == activity
    assert entry.new_values[ph.PrehistoryEntry.Attr.PROJECT] == project
    assert entry.new_values[ph.PrehistoryEntry.Attr.INST_CODE] == inst


@pytest.mark.parametrize('line, activity, project, inst', short_reg_lines)
def test_regex_on_short_reg_lines(line, activity, project, inst, prehistory_test_setup):
    assert ph.PrehistoryChange.from_line(line) is None, 'Reg line qualified as change line'
    assert ph.PrehistoryFromPh.from_line(line) is None, 'Reg line qualified as from ph line'
    entry = ph.PrehistoryRegistrationShort.from_line(line)
    assert entry is not None
    assert entry.new_values[ph.PrehistoryEntry.Attr.INST_CODE] == inst
    assert entry.new_values[ph.PrehistoryEntry.Attr.CMS_ACTIVITY] == activity
    assert entry.new_values[ph.PrehistoryEntry.Attr.PROJECT] == project


@pytest.mark.parametrize('line, date, status_cms', from_ph_lines)
def test_create_from_ph_objects(line, date, status_cms, prehistory_test_setup):
    fph = ph.PrehistoryFromPh.from_line(line)
    assert fph.date == date
    assert fph.new_values[ph.PrehistoryEntry.Attr.CMS_STATUS] == status_cms


@pytest.mark.parametrize('line, date, param_name, old_value, new_value', change_lines)
def test_create_change_objects(line, date, param_name, old_value, new_value, prehistory_test_setup):
    change = ph.PrehistoryChange.from_line(line)
    assert isinstance(change, ph.PrehistoryChange)
    assert change.date == date
    if param_name:
        assert param_name in change.old_values
        assert change.old_values[param_name] == old_value
    if new_value:
        assert change.new_values.get(param_name) == new_value


def test_attribute_name_resolver():
    for q in ['cmsstatus', 'status_cms', 'CMSstatus', 'StatusCMS']:
        assert ph.PrehistoryEntry.Attr.resolve(q) == ph.PrehistoryEntry.Attr.CMS_STATUS
    for q in ['cmsactivity', 'ActivityCMS', 'CMS Activity', 'Activity']:
        assert ph.PrehistoryEntry.Attr.resolve(q) == ph.PrehistoryEntry.Attr.CMS_ACTIVITY
    for q in ['inst code', 'InstCOde', 'inst_code']:
        assert ph.PrehistoryEntry.Attr.resolve(q) == ph.PrehistoryEntry.Attr.INST_CODE

    for q in ['inst code sign', 'instcodesign', 'InstCodeSign']:
        assert ph.PrehistoryEntry.Attr.resolve(q) == q


def test_prehistory_parser_finds_events():
    pp = ph.PrehistoryParser(hist_a, person_obj=person_a)
    assert len(pp.events) == 6
    for _class, _count in ((ph.PrehistoryChange, 3), (ph.PrehistoryRegistration, 1), (ph.PrehistoryFromPh, 2)):
        assert len([e for e in pp.events if e.__class__ == _class]) == _count


def test_prehistory_parser_generate_timelines():
    pp = ph.PrehistoryParser(hist_a, person_obj=person_a)
    lines = pp.generate_timelines()
    assert len(lines) == 3
    assert lines[0].start_date == date(2014, 7, 22)
    assert lines[0].status_cms == 'EXMEMBER'
    assert lines[0].activity_cms == 'Doctoral Student'

    assert lines[1].start_date == date(2013, 4, 1)
    assert lines[1].status_cms == 'CMS'
    assert lines[1].activity_cms == 'Doctoral Student'

    assert lines[2].start_date == date(2013, 2, 4)
    assert lines[2].activity_cms == 'Non-Doctoral Student'
    assert lines[2].status_cms == 'CMS'


def test_prehistory_parser_on_returning_cms_member():
    pp = ph.PrehistoryParser(hist_d, person_obj=person_d)
    lines = pp.generate_timelines()
    assert len(lines) == 5
    assert lines[0].status_cms == 'CMS'
    assert lines[1].status_cms == 'EXMEMBER'
    assert lines[2].status_cms == 'CMS'


def test_prehistory_parser_for_person_e():
    pp = ph.PrehistoryParser(hist_e, person_e)
    lines = pp.generate_timelines()
    assert len(lines) == 2


def test_prehistory_parser_generate_timelines_b():
    pass


def test_prehistory_parser_generate_timelines_c():
    pp = ph.PrehistoryParser(hist_c, person_c)
    lines = pp.generate_timelines()

    assert len(lines) == 2
    assert lines[0].start_date == date(2015, 2, 13)
    assert lines[0].status_cms == 'EXMEMBER'
    assert lines[0].activity_cms == 'Doctoral Student'

    assert lines[1].start_date == date(2013, 1, 23)
    assert lines[1].status_cms == 'CMS'
    assert lines[1].activity_cms == 'Doctoral Student'


def test_prehistory_parser_generate_timelines_h():
    pp = ph.PrehistoryParser(hist_h, person_h)
    lines = pp.generate_timelines()

    assert len(lines) > 0
    assert lines[0].status_cms == 'CMS'
    assert lines[0].start_date == date(2019, 2, 6)
    assert lines[1].status_cms == 'EXMEMBER'
    assert lines[2].status_cms == 'EXMEMBER'


def test_parsing_history_f_and_make_sure_there_are_no_missing_values():
    history = PersonHistory.from_ia_dict({PersonHistory.cmsId: person_f.cmsId, PersonHistory.history: hist_f})
    m = PrehistoryModel(cms_id=person_f.cmsId, person=person_f, history=history)
    assert len(m.get_prehistory_time_lines()) > 0
    for l in m.get_prehistory_time_lines()[0:-1]:
        for attr in [PrehistoryTimeline.inst_code, PrehistoryTimeline.status_cms, PrehistoryTimeline.activity_cms]:
            assert l.get(attr) is not None, '%s is apparently None for the time line starting on %s' % (
            attr.key, l.get(PrehistoryTimeline.start_date))


