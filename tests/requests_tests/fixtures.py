from datetime import timedelta

import pytest
from icms_orm.common import Person

from icmsutils.datatypes import ActorData
from .mock_processors import *
from .mock_executors import *
from icmsutils.businesslogic.requests import RequestsService
from icmsutils.icmstest.ddl import recreate_common_db
from icmsutils.icmstest.mock import MockNewPersonFactory as MockPersonFactory
from icmsutils.icmstest.mock import MockInstituteFactory, MockAffiliationFactory, MockInstituteLeaderFactory

def clean_slate() :
    recreate_common_db()
    MockPersonFactory.reset_static_counters()
    for cms_id in range(1, 14):
        _mock = MockPersonFactory.create_instance({Person.cms_id: cms_id})
        Request.session().add(_mock)
    Request.session().commit()

@pytest.fixture(scope='function', name='clean_slate')
def clean_slate_fixture():
    return clean_slate()


@pytest.fixture(scope='class')
def setup_for_request_processing():
    clean_slate()
    if 'BOTCHED_REQUEST' not in RequestsService.get_registered_request_types():
        # adding a botched request type (with failing processor)
        RequestsService.register_request_type_name_with_processor_and_executor(
            'BOTCHED_REQUEST', processor_class=LazyRequestProcessor, executor_class=IneptRequestExecutor
        )
    if 'CREATABLE_BY_13_REQUEST' not in RequestsService.get_registered_request_types():
        # adding a simple request that, sadly, cannot be created (except by cms_id=13)
        RequestsService.register_request_type_name_with_processor_and_executor(
            'CREATABLE_BY_13_REQUEST', processor_class=RequestProcessorWithSelectiveCreation,
            executor_class=LazyRequestExecutor
        )

    if 'MANAGEABLE_BY_13_REQUEST' not in RequestsService.get_registered_request_types():
        # adding a simple request that, just as sadly, cannot be acted upon (except by cms_id=13)
        RequestsService.register_request_type_name_with_processor_and_executor(
            'MANAGEABLE_BY_13_REQUEST', processor_class=RequestProcessorWithSelectiveAction,
            executor_class=LazyRequestExecutor
        )

    if 'NEW_INST_REQUEST' not in RequestsService.get_registered_request_types():
        # adding a more elaborate request type with a refined processor requiring certain privileges etc.
        RequestsService.register_request_type_name_with_processor_and_executor(
            'NEW_INST_REQUEST', processor_class=RequestProcessorWithDynamicActPermission,
            executor_class=InstituteCreatingRequestExecutor
        )

    if 'BROKEN_REQUEST' not in RequestsService.get_registered_request_types():
        RequestsService.register_request_type_name_with_processor_and_executor('BROKEN_REQUEST',
                                                                               processor_class=BrokenProcessor,
                                                                               executor_class=LazyRequestExecutor)

    if 'STILL_BROKEN_REQUEST' not in RequestsService.get_registered_request_types():
        RequestsService.register_request_type_name_with_processor_and_executor('STILL_BROKEN_REQUEST',
                                                                               processor_class=StillBrokenProcessor,
                                                                               executor_class=LazyRequestExecutor)

    if 'TRANSFER_REQUEST' not in RequestsService.get_registered_request_types():
        RequestsService.register_request_type_name_with_processor_and_executor('TRANSFER_REQUEST',
            processor_class=TransferRequestProcessor, executor_class=TransferRequestExecutor)


@pytest.fixture(scope='class')
def submit_inst_create_request(setup_for_request_processing, request):
    _data = request.param
    _request = RequestsService.create_request(request_type_name='NEW_INST_REQUEST',
                                   actor_data=ActorData(cms_id=_data.get('actor_cms_id')),
                                   remarks=_data.get('remarks', ''),
                                   name=_data.get('name'),
                                   code=_data.get('code'),
                                   decision_maker_id = _data.get('decision_maker_id'));
    return _request.get(Request.id)


@pytest.fixture(scope='class')
def bulk_submit_requests(setup_for_request_processing, request):
    """
    :param setup_for_request_processing: fixture assuring that the request types are registered for testing
    :param request: parameter for indirect parametrization, this time a list of request details
    :return:
    """
    # todo: running here clean slate as the scope doesn't seem to work - perhaps due to the parameter passed? (should be identical anyway, but it's an array)
    clean_slate()
    print( 'Running the bulk submit fixture' )
    for e in request.param:
        RequestsService.create_request(
            request_type_name=e.get('request_type', 'NEW_INST_REQUEST'),
            actor_data=ActorData(cms_id=e.get('cms_id', 1)),
            remarks = e.get('remarks'),
            **e
        )


@pytest.fixture(scope='class')
def setup_for_transfer_requests():
    recreate_common_db()
    # create institutes
    _ssn = Request.session()
    rio1 = MockInstituteFactory.create_instance({Institute.name: 'RIO-1', Institute.code: 'R1'})
    rio2 = MockInstituteFactory.create_instance({Institute.name: 'RIO-2', Institute.code: 'R2'})
    bern = MockInstituteFactory.create_instance({Institute.name: 'BERN', Institute.code: 'BN'})
    _ = [_ssn.add(x) for x in [rio1, rio2, bern]]
    _ssn.flush()

    # create people and affiliations
    _ppl = []
    _affs = []
    for i in range(0, 15):
        p = MockPersonFactory.create_instance()
        _ppl.append(p)
        _ssn.add(p)
        _ssn.flush()
        af = MockAffiliationFactory.create_instance({
            Affiliation.cms_id: p.get(Person.cms_id),
            Affiliation.inst_code: ([rio1, rio2, bern][int(i/5)]).get(Institute.code),
            Affiliation.is_primary: True
        })
        _affs.append(af)
        _ssn.add(af)
    _ssn.commit()

    # create team leaders
    _bosses = []
    for _ppl_idx, _inst in [(0, rio1), (0, rio2), (14, bern)]:
        _il = MockInstituteLeaderFactory.create_instance({
            InstituteLeader.cms_id: _ppl[_ppl_idx].get(Person.cms_id),
            InstituteLeader.inst_code: _inst.get(Institute.code),
            InstituteLeader.is_primary: True
        })
        _bosses.append(_il)
        _ssn.add(_il)
    _ssn.commit()
    return {'people': {_p.get(Person.cms_id): _p for _p in _ppl},
            'bosses': {_b.get(InstituteLeader.inst_code): _b for _b in _bosses},
            'affiliations': {_a.get(Affiliation.cms_id): _a.get(Affiliation.inst_code) for _a in _affs}
            }


@pytest.fixture(scope='class')
def setup_transfer_requests(setup_for_transfer_requests):

    _people = setup_for_transfer_requests['people']
    _bosses = setup_for_transfer_requests['bosses']
    _insts = setup_for_transfer_requests['affiliations']
    _requests = []

    if 'TRANSFER_REQUEST' not in RequestsService.get_registered_request_types():
        RequestsService.register_request_type_name_with_processor_and_executor('TRANSFER_REQUEST',
            processor_class=TransferRequestProcessor, executor_class=TransferRequestExecutor)

    for i, p in enumerate(_people.values()):
        if i%5 != 2:
            continue
        _cms_id = int(p.get(Person.cms_id))
        _req = RequestsService.create_request('TRANSFER_REQUEST', actor_data=ActorData(cms_id=_cms_id), cms_id=_cms_id,
                                              remarks='from %s' % _insts.get(_cms_id),
                                              inst_code_from=_insts.get(_cms_id),
                                              inst_code_to=_insts.get( _people[(i + 5) % len(_people)].cms_id ))
        _requests.append(_req)
    return {'bosses': _bosses, 'requests': _requests}


@pytest.fixture(scope='class')
def accept_transfer_requests(setup_transfer_requests):
    _requests = setup_transfer_requests['requests']
    _bosses = setup_transfer_requests['bosses']

    for _r in _requests:
        _boss_from_id = _bosses[_r.get(Request.processing_data)['inst_code_from']].get(InstituteLeader.cms_id)
        _boss_to_id = _bosses[_r.get(Request.processing_data)['inst_code_to']].get(InstituteLeader.cms_id)

        RequestsService.approve_step(
            request_id=_r.get(Request.id),
            actor_data=ActorData(cms_id=_boss_from_id),
            step_id=_r.get(Request.steps)[0].get(RequestStep.id)
        )

        RequestsService.approve_step(
            request_id=_r.get(Request.id),
            actor_data=ActorData(cms_id=_boss_to_id),
            step_id=_r.get(Request.steps)[1].get(RequestStep.id)
        )

    return {'requests': _requests}


@pytest.fixture(scope='class')
def submit_delayed_request(setup_for_request_processing):
    _r = RequestsService.create_request(request_type_name='BOTCHED_REQUEST', actor_data=ActorData(cms_id=2),
          remarks='Botched request has a failing executor but for a delayed request that is not an immediate problem',
          execution_date=date.today() + timedelta(minutes=17))
    return _r.get(Request.id)
