from icms_orm.common import Request, RequestStatusValues, RequestStep, RequestStepStatusValues, InstituteLeader
from sqlalchemy import and_

from icmsutils.businesslogic.requests import BaseRequestProcessor


class LazyRequestProcessor(BaseRequestProcessor):
    def can_create(self, actor_data):
        return True

    def get_cms_ids_allowed_to_act(self, action=None, step=None):
        return range(1, 20000)

    def create_next_steps(self, actor_data, action=None):
        return []


class RequestProcessorWithSteps(LazyRequestProcessor):
    def create_next_steps(self, actor_data, action=None):
        _steps = []
        if self.request.get(Request.status) == RequestStatusValues.PENDING_APPROVAL:
            _steps_so_far = self.request.get(Request.steps)
            _remarks = 'Just another step'
            if 'decision_maker_id' in self.request.get(Request.processing_data):
                _remarks = 'Only CMS ID %d can make the call!' % self.request.get(Request.processing_data)['decision_maker_id']
            if not (_steps_so_far):
                _steps += [RequestStep.from_ia_dict({
                    # todo: test somewhere that the dates get initialized correctly
                    RequestStep.request_id: self.request.id,
                    RequestStep.status: RequestStepStatusValues.PENDING,
                    RequestStep.remarks: _remarks,
                })]
        return _steps


class RequestProcessorWithSelectiveCreation(RequestProcessorWithSteps):
    def can_create(self, actor_data):
        return actor_data.cms_id == 13


class RequestProcessorWithSelectiveAction(RequestProcessorWithSteps):

    def get_cms_ids_allowed_to_act(self, action=None, step=None):
        return [13]


class RequestProcessorWithDynamicActPermission(RequestProcessorWithSteps):
    """
    This one will require the request to store explicit CMS_IDs of people allowed to handle it
    """

    def get_cms_ids_allowed_to_act(self, action=None, step=None):
        return [self.request.get(Request.processing_data).get('decision_maker_id', None)]


class BrokenProcessor(BaseRequestProcessor):
    pass


class StillBrokenProcessor(BrokenProcessor):
    def can_create(self, actor_data):
        return True


class TransferRequestProcessor(BaseRequestProcessor):
    @property
    def cms_id(self):
        return self.request.get(Request.processing_data)['cms_id']

    @property
    def inst_code_from(self):
        return self.request.get(Request.processing_data)['inst_code_from']

    @property
    def inst_code_to(self):
        return self.request.get(Request.processing_data)['inst_code_to']

    def can_create(self, actor_data):
        # can only file the request on one's own behalf
        return actor_data.cms_id == self.cms_id

    def get_cms_ids_allowed_to_act(self, action=None, step=None):
        _ssn = InstituteLeader.session()
        _ans = set()
        if self.request.get(Request.status) != RequestStatusValues.PENDING_APPROVAL:
            return _ans
        _bosses = InstituteLeader.session().query(InstituteLeader).filter(and_(
            InstituteLeader.end_date == None, InstituteLeader.inst_code.in_([self.inst_code_from, self.inst_code_to])
        )).all()

        _steps = self.request.get(Request.steps)
        for _i, _s in enumerate(_steps):
            if step and step.get(RequestStep.id) != _s.get(RequestStep.id):
                # Means that the step has been specifically indicated and it does not match current step
                continue
            elif _s.get(RequestStep.status) == RequestStepStatusValues.PENDING:
                # first step will by convention correspond to the present institute, second step to the next one
                _ = [_ans.add(_boss.get(InstituteLeader.cms_id)) for _boss in _bosses if _boss.get(InstituteLeader.inst_code) == [self.inst_code_from, self.inst_code_to][_i]]
        return _ans

    def create_next_steps(self, actor_data, action=None):
        if self.request.get(Request.status) == RequestStatusValues.PENDING_APPROVAL and len(self.request.get(Request.steps)) == 0:
            return [
                RequestStep.from_ia_dict({
                    RequestStep.status: RequestStepStatusValues.PENDING,
                    RequestStep.request: self.request,
                    RequestStep.creator_cms_id: self.cms_id
                }),
                RequestStep.from_ia_dict({
                    RequestStep.status: RequestStepStatusValues.PENDING,
                    RequestStep.request: self.request,
                    RequestStep.creator_cms_id: self.cms_id
                })
            ]
        return []