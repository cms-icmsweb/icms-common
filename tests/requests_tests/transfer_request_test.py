from .fixtures import *
from icmsutils.businesslogic.requests import RequestProcessingException
from icmsutils.icmstest.assertables import assert_attr_val


class TestTransferRequest(object):
    @pytest.mark.parametrize('code', ['R1', 'R2', 'BN'])
    def test_that_each_cbi_only_sees_requests_relevant_for_them(self, setup_transfer_requests, code):
        # we extract a CMS ID of 'code' institute's boss
        _boss_id = setup_transfer_requests['bosses'][code].get(InstituteLeader.cms_id)
        # we find ALL the institutes led by this person
        _managed_codes = {_k for _k, _v in setup_transfer_requests['bosses'].items() if _v.get(InstituteLeader.cms_id) == _boss_id}
        # we find the steps pending this boss' action
        _steps = RequestsService.get_request_steps_pending_action(ActorData(cms_id=_boss_id))
        for _s in _steps:
            # ensure that ANY of the institutes led by this boss shows up in the request!
            _code_from, _code_to = (_s.get(RequestStep.request).get(Request.processing_data).get(k) for k in ('inst_code_from', 'inst_code_to'))
            assert _code_from in _managed_codes or _code_to in _managed_codes

    @pytest.mark.parametrize('inst_from', ['R2'])
    def test_that_multi_cbi_needs_to_specify_the_step_id(self, setup_transfer_requests, inst_from):
        # we extract a CMS ID of 'code' institute's boss
        _boss_id = setup_transfer_requests['bosses'][inst_from].get(InstituteLeader.cms_id)
        _request = RequestsService.get_requests_by_remarks('from %s' % inst_from)[0]
        with pytest.raises(RequestProcessingException):
            RequestsService.approve_step(request_id=_request.get(Request.id), actor_data=ActorData(cms_id=_boss_id))
        with pytest.raises(RequestProcessingException):
            RequestsService.reject_step(request_id=_request.get(Request.id), actor_data=ActorData(cms_id=_boss_id))

    @pytest.mark.parametrize('inst_from', ['R1', 'R2', 'BN'])
    def test_that_uninvolved_cbi_cannot_approve_request(self, setup_transfer_requests, inst_from):
        # extract a non-boss cms_id
        _non_boss_id = setup_transfer_requests['bosses']['R' in inst_from and 'BN' or 'BN']
        _request = RequestsService.get_requests_by_remarks('from %s' % inst_from)[0]
        with pytest.raises(RequestProcessingException):
            RequestsService.approve_step(request_id=_request.get(Request.id), actor_data=ActorData(cms_id=_non_boss_id))
        with pytest.raises(RequestProcessingException):
            RequestsService.reject_step(request_id=_request.get(Request.id), actor_data=ActorData(cms_id=_non_boss_id))

    @pytest.mark.parametrize('inst_from', ['R1', 'R2', 'BN'])
    def test_that_acting_on_wrong_step_id_raises_exception(self, setup_transfer_requests, inst_from):
        # extract a non-boss cms_id
        _boss_id = setup_transfer_requests['bosses'][inst_from].get(InstituteLeader.cms_id)
        _req_id = RequestsService.get_requests_by_remarks('from %s' % inst_from)[0].get(Request.id)
        _wrong_step_id = Request.session().query(RequestStep.id).filter(Request.id != _req_id).first()
        with pytest.raises(RequestProcessingException):
            RequestsService.approve_step(request_id=_req_id, actor_data=ActorData(cms_id=_boss_id), step_id=_wrong_step_id)
        with pytest.raises(RequestProcessingException):
            RequestsService.reject_step(request_id=_req_id, actor_data=ActorData(cms_id=_boss_id), step_id=_wrong_step_id)

    def test_that_affiliation_has_been_changed(self, accept_transfer_requests):
        _requests = accept_transfer_requests['requests']
        for _request in _requests:
            _request = Request.session().query(Request).filter(Request.id == _request.get(Request.id)).one()
            assert _request.get(Request.status) == RequestStatusValues.EXECUTED
            _ic_from = _request.get(Request.processing_data)['inst_code_from']
            _ic_to = _request.get(Request.processing_data)['inst_code_to']
            _cms_id = _request.get(Request.creator_cms_id)
            assert_attr_val(Affiliation, {Affiliation.cms_id: _cms_id, Affiliation.inst_code: _ic_from}, {Affiliation.end_date: date.today()})
            assert_attr_val(Affiliation, {Affiliation.cms_id: _cms_id, Affiliation.inst_code: _ic_to}, {Affiliation.end_date: None})

    @pytest.mark.parametrize('inst_from', ['R1', 'R2', 'BN'])
    def test_that_approval_does_not_create_extra_steps(self, accept_transfer_requests, inst_from):
        _request = RequestsService.get_requests_by_remarks('from %s' % inst_from)[0]
        assert 2 == len(_request.get(Request.steps))
