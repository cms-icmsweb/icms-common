from datetime import datetime as dt


class TestScenario(object):

    def __init__(self, db):
        self.db = db

    """
    Introduces a common interface to
    """
    def run(self):
        for year in range(2015, 2020):
            yield dt(year, 1, 1)

